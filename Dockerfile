# Dockerfile to build component for compiling YARR software
# Includes FELIX build so we can build against NetIO

ARG CI_DIR

# This basically has just the FELIX software image
FROM gitlab-registry.cern.ch/ci-tools/ci-worker:cc7

# Clean all means docker only captures the installed version, not the YUM cache
RUN yum install -y centos-release-scl-rh devtoolset-7 make cmake3 git gnuplot gcc-c++ python snappy-devel && yum clean all && python3 -m pip install --upgrade pip setuptools wheel && pip install gcovr
