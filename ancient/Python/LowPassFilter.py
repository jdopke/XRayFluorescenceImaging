import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import cv2
import sys

from read_function import read_file

#creates a circular point spread function of diameter d
def defocus_kernel(d, sz=30):
    kern = np.zeros((sz, sz), np.uint8)
    cv2.circle(kern, (sz, sz), d, 255, -1, cv2.LINE_AA, shift=1)
    kern = np.float32(kern) / 255.0
    return kern

#reading known good files for signal and background
d1, nBins1 = read_file(r"C:\XFCT\Alex\data\TEST3SIGNAL.hxt")
d2, nBins2 = read_file(r"C:\XFCT\Alex\data\TEST3BACKGROUND.hxt")

#scalefactors for signal and background in acceptance window
events_to_scale_signal = np.sum(d1[40:65, 20:45, 320:324])
events_to_scale_background = np.sum(d2[40:65, 20:45, 320:324])
scaleMe = events_to_scale_signal/events_to_scale_background

#signal and background slices    
m1 = np.zeros( (80,80) );
m2 = np.zeros( (80,80) );

#filling slices with lead k lines
for j in range(0, 80):
    for k in range (0, 80):
        m1[j][k] += np.sum(d1[j, k, 290:294])+np.sum(d1[j, k, 298:302])+np.sum(d1[j, k, 338:342])
        m2[j][k] += np.sum(d2[j, k, 290:294])+np.sum(d2[j, k, 298:302])+np.sum(d2[j, k, 338:342])

#background subtraction
m1-=m2*scaleMe

#storing name for deconvolution window
win = 'filtered'

#generating cv2 compatible image from input
img = np.float32(m1)/np.float32(np.amax(m1))

height, width = img.shape[:2]
#showing input data
cv2.namedWindow('input', 0)
cv2.resizeWindow('input', 600,600)
blurrr = cv2.GaussianBlur(img,(3,3),0)
cv2.imshow('input', blurrr)
cv2.namedWindow('psf', 0)
cv2.resizeWindow('psf', 600,600)
cv2.imshow('psf', img)
cv2.namedWindow('FFT', 0)
cv2.resizeWindow('FFT', 600,600)
IMG = cv2.dft(img, flags=cv2.DFT_COMPLEX_OUTPUT)
dft_shift = np.fft.fftshift(IMG)
magnitude_spectrum = 20*np.log(cv2.magnitude(dft_shift[:,:,0],dft_shift[:,:,1]))
cv2.imshow('FFT', magnitude_spectrum)

"""
#callback function for updating deconvolution parameters
def update(_):
    #getting updating parameters from trackbars
    d = cv2.getTrackbarPos('d', win)
    psf = defocus_kernel(d)
    cv2.imshow('psf', psf)
    psf /= psf.sum()
    psf_pad = np.zeros_like(img)
    kh, kw = psf.shape
    psf_pad[:kh, :kw] = psf
    PSF = cv2.dft(psf_pad, flags=cv2.DFT_COMPLEX_OUTPUT, nonzeroRows = kh)
    PSF2 = (PSF**2).sum(-1)
    iPSF = PSF / (PSF2)[...,np.newaxis]
    RES = cv2.mulSpectrums(IMG, iPSF, 0)
    res = cv2.idft(RES, flags=cv2.DFT_SCALE | cv2.DFT_REAL_OUTPUT )
    res = np.roll(res, -kh//2, 0)
    res = np.roll(res, -kw//2, 1)
    cv2.imshow(win, res)
"""
#generating output windows with trackbars
cv2.namedWindow(win, cv2.WINDOW_NORMAL)
cv2.resizeWindow(win, 600,600)
#cv2.createTrackbar('d', win, int(7), 25, update)
#update(None)

#required to keep the program running
while True:
    ch = cv2.waitKey()
    if ch == ' ':
        break


