import concurrent.futures
import logging
import queue
import random
import threading
import time

import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import tkinter as tk
from tkinter import filedialog
from readframe import readframe, fastorder, apply_to_files_and_save, readRawFrames
import scipy
from scipy import ndimage

loglevel_const = 1000

idx = np.empty( 6400 )

def frameConsumer(frameQueue, clusterQueue, event):
    logging.info("Frameconsumer started")
    framecount = 1
    thresh = fastorder(np.loadtxt(r"D:\\XF-CT\\Prep\\500V_28C_Thresh.txt"))
    grads = fastorder(np.loadtxt(r"D:\\XF-CT\\Prep\\500V_28C_Grads.txt"))
    inters = fastorder(np.loadtxt(r"D:\\XF-CT\\Prep\\500V_28C_Inters.txt"))
    kernel = np.ones((3,3))
    logging.info("Frameconsumer made it past init, queue size is: %d", frameQueue.qsize())
    while not event.is_set() or not frameQueue.empty():
#        logging.info("Frameconsumer is retrieving a frame from queue (size %d)", frameQueue.qsize())
        frame = fastorder(frameQueue.get(block=True, timeout=1))
        if (framecount % loglevel_const == 0):
            logging.info("%d frames processed (size=%d)", framecount, frameQueue.qsize())
        occupation_frame = np.greater(frame, thresh)
        labeled_frame, number_of_hits = ndimage.label(occupation_frame,kernel)
        regions = ndimage.find_objects(labeled_frame)
        cluster = np.asarray([np.sum(occupation_frame[r]) for r in regions])
        clusters_hist, _ = np.histogram(cluster, range(0,16))
        calibrated_frame = (frame * grads) + inters
        cluster_energies np.asarray([np.sum(calibrated_frame[r]) for r in regions])
        clusterQueue.put(clusters_hist);
        framecount += 1
        
    if (event.is_set()):
        logging.info("Consumer received event. Exiting")
    else:
        logging.info("Empty frameQueue. Exiting")

def clusterConsumer(clusterQueue, event, totalclusters_hist):
    logging.info("Clusterconsumer started, queue size is %d", clusterQueue.qsize())
    framecount = 1
    while not event.is_set() or not clusterQueue.empty():
        clusters = clusterQueue.get(block=True, timeout=1)
        if (framecount % loglevel_const == 0):
            logging.info("%d Clusters retrieved, cluster queue size: %d",framecount, clusterQueue.qsize())
        framecount += 1
        totalclusters_hist = totalclusters_hist + clusters
        if (clusterQueue.empty()):
            time.sleep(0.1)
    if (event.is_set()):
        logging.info("clusterConsumer received event. Exiting")
    else:
        logging.info("Empty clusterQueue. Exiting")
    

def frameProducer(frame1Queue, frame2Queue, frame3Queue, event, fp):
    """Pretend we're getting a number from the network."""
    logging.info("Frameproducer started")
    framecount = 1
#    read, frame = readframe(fp)
    nFrames = 6
    read, frames = readRawFrames(fp, nFrames)
    while not event.is_set() and read:
        if ( (framecount % loglevel_const) < ((framecount - nFrames) % loglevel_const) ):
            logging.info("Read %d frames", nFrames)
        frame1Queue.put(frames[0:6400])
        frame2Queue.put(frames[6400:12800])
        frame3Queue.put(frames[12800:19200])
        frame1Queue.put(frames[19200:25600])
        frame2Queue.put(frames[25600:32000])
        frame3Queue.put(frames[32000:38400])
        if ( (framecount % loglevel_const) < ((framecount - nFrames) % loglevel_const) ):
            logging.info("Read %d frames (queue sizes %d/%d/%d)", framecount, frame1Queue.qsize(), frame2Queue.qsize(), frame3Queue.qsize())
        framecount += nFrames
        read, frame = readframe(fp)

    if (event.is_set()):
        logging.info("Producer received event. Producer exiting")
    else:
        logging.info("End of file reached. Producer exiting")


if __name__ == "__main__":
    file_dir=filedialog.askopenfilename()
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")

    frame1Pipeline = queue.Queue(maxsize=1000)
    frame2Pipeline = queue.Queue(maxsize=1000)
    frame3Pipeline = queue.Queue(maxsize=1000)
    clusterPipeline = queue.Queue(maxsize=100)
    for i in range(6400):
        idx[i] = ((i*4)%6400) + ((int) (i/6400))
    event = threading.Event()
    total_clusters_hist =np.zeros(15)
    with open(file_dir, mode="rb", buffering=(6400*2*10000)) as fp:
        with concurrent.futures.ThreadPoolExecutor(max_workers=20) as executor:
            executor.submit(frameProducer, frame1Pipeline, frame2Pipeline, frame3Pipeline, event, fp)
            time.sleep(0.1)
            executor.submit(frameConsumer, frame1Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame2Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame3Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame1Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame2Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame3Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame1Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame2Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame3Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame1Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame2Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame3Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame1Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame2Pipeline, clusterPipeline, event)
            executor.submit(frameConsumer, frame3Pipeline, clusterPipeline, event)
            time.sleep(0.1)
            executor.submit(clusterConsumer, clusterPipeline, event, total_clusters_hist)
    
            time.sleep(10)
            logging.info("Main: about to set event")
            event.set()
            logging.info("Queue sizes:\n FrameQueue 1: %d\n FrameQueue 2: %d\n FrameQueue 3: %d\n ClusterQueue: %d", frame1Pipeline.qsize(), frame2Pipeline.qsize(), frame3Pipeline.qsize(), clusterPipeline.qsize(), )
    logging.info("Queue after Threads ended:\n FrameQueue 1: %d\n FrameQueue 2: %d\n FrameQueue 3: %d\n ClusterQueue: %d", frame1Pipeline.qsize(), frame2Pipeline.qsize(), frame3Pipeline.qsize(), clusterPipeline.qsize(), )
    print(total_clusters_hist)
