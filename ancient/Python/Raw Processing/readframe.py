import numpy as np
import struct
import tkinter as tk
from tkinter import filedialog
import glob
import os


def detector_index(b, a):
    return a*80 + 20*(b%4) + int(b/4)

def fastorder(data):
    frame3d = np.reshape(data, (80, 20, 4))
    frame = np.empty( (80,80) )
    frame[:,0:20] = data[0::4]
    frame[:,20:40] = data[1::4]
    frame[:,40:60] = data[2::4]
    frame[:,60:80] = data[3::4]
#    frame[:,20:40] = frame3d[:,:,1]
#    frame[:,40:60] = frame3d[:,:,2]
#    frame[:,60:80] = frame3d[:,:,3]
    return frame



def readframe(fp):
    frame=np.zeros((80,80))
    x=fp.read(80 * 80 * 2)
    if x:
        data = np.asarray(struct.unpack("H" * 80 * 80, x)) # read 80*80*2 bytes
        frame=fastorder(data)
        return True,frame
    else:
        return False,None

def loadCalib(filename):
    calib = numpy.loadtxt(filename)
    return calib

def apply_to_files_and_save(path,extension,function,target):
    for file in glob.glob(path+ '/*.'+extension):
        print(file)
        function(file,target)







