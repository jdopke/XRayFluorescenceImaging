import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import tkinter as tk
from tkinter import filedialog
from readframe import *
import scipy
from scipy import ndimage
import msvcrt
from typing import Any
import math
from scipy import ndimage, signal


class Data_Frame:

    def __init__(self, raw_data,frame_number , function_dictionary={}): #pass the functions for analyzing the data as dictionary
        self.frames = {"raw_frame": raw_data}
        self.function_dictionary = function_dictionary
        self.frame_number=frame_number

    def get(self, name, func=None):
        if name not in self.frames:                       #only evaluate the frame if it was not evaluated before
            if name in self.function_dictionary:
                self.frames[name] = self.function_dictionary[name](self)
            elif func is not None:
                self.frames[name] = func(self)
        return self.frames[name]


def energy_map(slopes, intercepts):
    def energy_frame(data_frame):
        return data_frame.get("raw_frame")* slopes + intercepts
    return energy_frame


def occupancy_map(thresh):
    def occupancy_frame(data_frame):
        return np.greater(data_frame.get("raw_frame"), thresh)
    return occupancy_frame


def labeled_frame(data):
    kernel = np.ones((3,3))
    labeled_frame, _ = ndimage.label(data.get("occupancy_frame"), kernel)

    return labeled_frame

def single_cluster_occupancy_frame(frame):
    occupancy = frame.get("occupancy_frame")
    regions = ndimage.find_objects(frame.get("labeled_frame"))
    single_cluster_occupancy=np.zeros((80,80))

    for region in regions:
        size = np.sum(occupancy[region])
        if size == 1:
            single_cluster_occupancy[region] = 1

    return single_cluster_occupancy

def single_clusters_energy(frame):
    result=[]
    occupancy = frame.get("single_cluster_occupancy_frame")
    energy = frame.get("energy_frame")
    energy[occupancy == False] = 0
    regions = ndimage.find_objects(frame.get("labeled_frame"))
    for region in regions:
        size = np.sum(occupancy[region])
        if size == 1:
            result.append(np.sum(energy[region]))
    return np.array(result)

def single_clusters_energy_frame(frame):
    result=[]
    occupancy = frame.get("single_cluster_occupancy_frame")
    energy = frame.get("energy_frame")
    energy[occupancy == False] = 0
    return energy




def fast_order(data):
    number_of_frames = int(data.size/6400)
    frames3d = np.reshape(data, (number_of_frames, 80, 20, 4))
    frames = np.empty((number_of_frames,80,80) )
    frames[:, :, 0:20] = frames3d[:, :, :, 0]
    frames[:, :, 20:40] = frames3d[:, :, :, 1]
    frames[:, :, 40:60] = frames3d[:, :, :, 2]
    frames[:, :, 60:80] = frames3d[:, :, :, 3]
    if number_of_frames==1:
        return frames[0]
    return frames








def read_frames(fp, number_of_frames=1):

    frames = np.zeros((number_of_frames,80,80))
    x = fp.read(80 * 80 * 2 * number_of_frames)
    if x:
        data = np.asarray(struct.unpack("H" * 80 * 80 * number_of_frames, x)) # read 80*80*2 bytes
        frames=fast_order(data)
        return frames
    else:
        return False


class Experiment:

    def __init__(self, file_dir, thresh, slopes, intercepts):
        self.number_of_frames = math.floor(os.path.getsize(file_dir) / 6400 / 2)
        self.file_dir= file_dir
        self.basic_frames = {
            "energy_frame": energy_map(slopes, intercepts)
            , "occupancy_frame": occupancy_map(thresh)
            , "labeled_frame": labeled_frame
            , "single_cluster_occupancy_frame": single_cluster_occupancy_frame
            , "single_clusters_energy": single_clusters_energy
            , "single_clusters_energy_frame": single_clusters_energy_frame
        }
    def frames_generator(self):
        frames_left = self.number_of_frames
        with open(self.file_dir, "rb") as fp:
            frame_count = 0
            while frames_left > 0:
                if frames_left > 10000:
                    buffer_size = 10000
                elif frames_left > 0:
                    buffer_size = frames_left

                frames_left -= buffer_size
                frame_buffer = read_frames(fp, buffer_size)

                for frame in frame_buffer:
                    frame_count += 1
                    if frame_count % 1000 == 0:
                        print("Frame count is", frame_count)
                    yield Data_Frame(frame,frame_count,self.basic_frames)
    def make(self,plot):

        plot.set_number_of_frames(self.number_of_frames)
        for frame in self.frames_generator():
                plot.get_frame(frame)



class Plot:  #plot class, will be called from the experiment object
    def __init__(self, function, function_name):
        self.function = function
        self.function_name=function_name
        self.current_frame=0

    def get_frame(self, data_frame): #take frames from the experiment class, make lists of result
        self.current_data_point = data_frame.get(self.function_name, self.function)
    def set_number_of_frames(self,number_of_frames):
        self.number_of_frames=number_of_frames


class LinearPlot(Plot):
    def __init__(self,function,function_name,number_of_points):
        self.number_of_points=number_of_points
        super().__init__(function,function_name)
        self.y=[]
        self.x=[]
        self.current_data_point=0

    def set_number_of_frames(self,number_of_frames):
        self.number_of_frames = number_of_frames
        self.step = int(number_of_frames/self.number_of_points)

    def get_frame(self,data_frame):
        self.current_frame += 1
        self.current_data_point += data_frame.get(self.function_name, self.function)
        if self.current_frame%self.step==0:
            self.y.append(self.current_data_point/self.step)
            self.x.append(self.current_frame)
            self.current_data_point=0
    def plot(self):
        plt.plot(self.x, self.y, "r", label=self.function_name )
        print(self.x)
        plt.show()

class Histogram(Plot):
    def __init__(self, function, function_name, part, bins):
        self.part=part
        self.bins = bins
        super().__init__(function,function_name)
        self.y = np.zeros(bins)
        self.number_of_points=0
        self.step= (part[1] - part[0]) / bins
        self.results_list=[]
        self.x= np.linspace(part[0],part[1],bins)
        self.hist=np.zeros(bins)


    def get_frame(self,data_frame):
        results=data_frame.get(self.function_name,self.function)

        if data_frame.frame_number%1000==0 or data_frame.frame_number==self.number_of_frames:
            self.hist, _ = np.histogram(self.results_list,self.bins, range=self.part )
            self.y += self.hist
            self.results_list=[]
        else:
            self.results_list.extend(results)
    def plot(self,fun= lambda x:x):
        normalised_y = self.y[:]
        plt.bar(self.x[0:normalised_y.size], normalised_y, width = 1.01*(self.part[1] - self.part[0])/self.bins)
        plt.xlabel(self.function_name)
        print(self.x.size, "x size", self.y.size, "y size")
        print(self.x,"x")
        print(normalised_y,"y")
        plt.show()
        plt.ylabel("n")

class Map2D(Plot):
    def __init__(self, function, function_name, part, bins):
        self.part = part
        self.bins = bins
        super().__init__(function, function_name)
        self.y = np.zeros((80,80,bins))
        self.number_of_points = 0
        self.step = (part[1] - part[0]) / bins
        self.results_list = np.zeros((80,80,10000))
        self.results_list_size=np.zeros((80,80)).astype("int64")
        self.hist = np.zeros((80, 80, bins))
        self.x = np.linspace(part[0], part[1]-self.step, bins)
    def reduce_frames(self,frames_to_reduce):
        self.bins = int(self.bins/frames_to_reduce)
        self.y=self.y.reshape((80,80,self.bins,frames_to_reduce)).sum(axis=3)
        self.x=self.x[frames_to_reduce-1::frames_to_reduce]
        print(self.bins,self.y.shape,self.x.shape)

    def get_frame(self,data_frame):
        results=data_frame.get(self.function_name,self.function)

        if data_frame.frame_number%10000==0 or data_frame.frame_number==self.number_of_frames:
            print(self.y.size,self.hist.size)

            for i in range(0,80): #add histograms every 10000 frames because it's slow
                for j in range(0,80):

                    self.hist[i,j], _ = np.histogram(self.results_list[i, j, 0:self.results_list_size[i,j]], self.bins, range=self.part )
                    self.y[i,j] += self.hist[i,j]

            self.results_list = np.zeros((80, 80, 10000))
            self.results_list_size = np.zeros((80, 80)).astype("int64")
        else:
            indeces= np.nonzero(results)
            self.results_list[indeces+(self.results_list_size[indeces],)] = results[indeces]
            self.results_list_size[indeces] += 1

    def apply(self, function):
        self.heatmap= np.zeros((80,80))
        for x in range(0,80):
            for y in range(0,80):
                self.heatmap[x, y] = function(self.y[x, y, :], self.x)


def calculate_pixel_response(experiment):
    histogram = np.zeros((105, 11))
    number_of_elements = np.zeros(105)
    frame_buffer = []
    for frame in experiment.frames_generator():
        frame_buffer.append(frame)

        if len(frame_buffer) == 11:

            hit_sum = np.zeros((80, 80))
            hits_to_take = np.zeros((80, 80))
            kernel = [[1, 0, 1], [0, 0, 0], [1, 0, 1]]

            for buffered_frame in frame_buffer:
                hit_sum += buffered_frame.get("single_cluster_occupancy_frame")

            energy_frame = frame_buffer[5].get("energy_frame")
            hit_pixels = np.logical_and(frame_buffer[5].get("single_cluster_occupancy_frame"), hit_sum == 2)
            hits_boundary = signal.convolve2d(hit_pixels, kernel, mode="same") == 1  # excluding multiple boundary

            '''
            boundary_energy = np.copy(energy_frame)
            boundary_energy[np.logical_not(hit_pixels)] = 0
            boundary_energy = signal.convolve2d(boundary_energy, kernel, mode="same")'''

            hits_to_take = hit_pixels
            hit_energies = energy_frame[hit_pixels]

            for i in range(0, 11):
                energies_to_add = frame_buffer[i].get("energy_frame")[hits_to_take]
                for j in range(0, hit_energies.size - 1):
                    if int(hit_energies[j]) < 105:
                        histogram[int(hit_energies[j]), i] += energies_to_add[j]
                    else:
                        print("hit with energy over threshold")

            for j in range(0, hit_energies.size - 1):
                if int(hit_energies[j]) < 105:
                    number_of_elements[int(hit_energies[j])] += 1

            del frame_buffer[0]  #to future self : stop deleting this u moron


    return histogram, number_of_elements







