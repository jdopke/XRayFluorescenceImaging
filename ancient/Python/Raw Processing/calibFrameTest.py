import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
from scipy import ndimage, signal, optimize,special
from numpy import vstack


from readframe import readframe,fastorder #orderframe

def find_cluster_sizes(occupation_frame):       #   returns list of clusters and number of them from the occupation frame

    kernel = np.array([[0,1,0],[1,1,1],[0,1,0]])
    labeled_frame, number_of_hits = ndimage.label(occupation_frame,kernel)
    regions = ndimage.find_objects(labeled_frame)
    cluster_list = np.asarray([np.sum(occupation_frame[region]) for region in regions])
    #print(cluster_list)
    #print(number_of_hits)
    return cluster_list , number_of_hits, regions

#slopes = fastorder(np.loadtxt(r"C:\Users\liamg\OneDrive\Documents\Project\Python\Raw Processing\500V_28C_Grads.txt"))
#intercepts = fastorder(np.loadtxt(r"C:\Users\liamg\OneDrive\Documents\Project\Python\Raw Processing\500V_28C_Inters.txt"))
#thresh = fastorder(np.loadtxt(r"C:\Users\liamg\OneDrive\Documents\Project\Python\Raw Processing\500V_28C_Thresh.txt"))

#filename = r"C:\Users\liamg\OneDrive\Documents\Project\Data\Alpha_Square_105keV_400um_PH_35mm_170mm_7_2s_170714_101833.bin";


slopes = fastorder(np.loadtxt(r"C:\Projects\XRFCPP\config\500V_28C_Grads.txt"))
intercepts = fastorder(np.loadtxt(r"C:\Projects\XRFCPP\config\500V_28C_Inters.txt"))
thresh = fastorder(np.loadtxt(r"C:\Projects\XRFCPP\config\500V_28C_Thresh.txt"))

filename = r"C:\Projects\XRFCPP\data\Alpha.bin";

#x = []
#y = []
x = np.empty([0])
y = np.empty([0])
with open(filename, "rb") as fp:
    frameCount = 1;
#    M1 = np.zeros((80,80))
    M2 = np.zeros((80,80))
    histo = np.zeros( 8192 )
    histoCalib = np.zeros(10000)
    while True:
        buf = fp.read(80*80*2)
        if buf:
            frame = np.asarray(struct.unpack("H"*80*80, buf))
            ordered = fastorder(frame)
            occupation_frame = np.greater(ordered, thresh)
            clusters,b,regions = find_cluster_sizes(occupation_frame)
            ordered *= slopes
            ordered += intercepts
            for region in regions:
                cluster_size = np.sum(occupation_frame[region])
                if (cluster_size != 2):
                    continue
                tempcluster = ordered[region].flatten()
                x = np.append(x, tempcluster[0])
                y = np.append(y, tempcluster[1])
                #print(x.size)
                #print(x.shape)
                #x = (tempcluster[0])
                #y = (tempcluster[1])
                #x += tempcluster[0]
                #y += tempcluster[1]
                #print(x)
                #plt.figure(9)
                #plt.scatter(x,y)
                #plt.show()
                #print(tempcluster[0],"\t",tempcluster[1])
                #print(x, "\t", y)
            M2 += ordered;
            histo[frame//8] += 1
            for i in range (0, 6400):
                if (ordered[i//80,i%80] > 0) and (ordered[i//80,i%80] < 100000):
                    histoCalib[np.uint(ordered[i//80,i%80]*10)] += 1
        else:
            break;
        frameCount += 1;
        if (frameCount % 100 == 0):
            print ("Frame count is", frameCount)
#        if (frameCount == 2000):
#            break



print ("Final frame count is", frameCount)
#print(tempcluster[0],"\t",tempcluster[1])
#print(T)
#print(a)
print(tempcluster)
print(x)
plt.figure(1)
plt.hist2d(x, y, bins=200)
plt.show()

plt.figure(2)
plt.matshow(M2, origin='lower', fignum=1);
plt.show(block = False)

#plt.figure(2)
#plt.semilogy(np.linspace(0, 8192, 8192), histo[0:8192], 'r', label = "Spectrum");
#plt.plot(np.linspace(0, nBins2/8, nBins2/2), histo2, 'b', label = "Background");
#plt.plot(np.linspace(0, nBins3/8, nBins3/2), histo3, 'g', label = "Gold");
#plt.xlabel("Bin")
#plt.ylabel("Relative intensity")
#plt.legend(loc = "upper right")
#plt.title("Full Data")
#plt.show(block = False)

#plt.figure(3)
#plt.semilogy(np.linspace(50000, 10000, 500), histoCalib[500:1000], 'r', label = "Spectrum");
#plt.plot(np.linspace(0, nBins2/8, nBins2/2), histo2, 'b', label = "Background");
#plt.plot(np.linspace(0, nBins3/8, nBins3/2), histo3, 'g', label = "Gold");
#plt.xlabel("Bin")
#plt.ylabel("Relative intensity")
#plt.legend(loc = "upper right")
#plt.title("Full Data")
#plt.show(block = False)

M3=M2[42]
#print(M3)
plt.figure(4)
plt.bar(range(len(M3)), M3)
plt.show()

#plt.figure(5)
#plt.plot(M3)
#plt.show()

raise SystemExit
