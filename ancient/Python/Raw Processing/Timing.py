import timeit
import concurrent.futures
import logging
import queue
import random
import threading
import time

import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import tkinter as tk
from tkinter import filedialog
from readframe import readframe, fastorder, apply_to_files_and_save, readRawFrames
import scipy
from scipy import ndimage


if __name__ == "__main__":
    print(timeit.timeit("fastorder(data)", setup="from readframe import fastorder; import numpy as np; data = np.empty((6400));", number=100000)/100000)
