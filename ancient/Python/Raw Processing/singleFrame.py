import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2
import tkinter as tk
from tkinter import filedialog
from readframe import *
import scipy
from scipy import ndimage, signal, optimize,special
import msvcrt
from typing import Any
from frame_class import *
import sys
import random
import math



def find_cluster_sizes(occupation_frame):       #   returns list of clusters and number of them from the occupation frame

    kernel = np.array([[0,1,0],[1,1,1],[0,1,0]])
    labeled_frame, number_of_hits = ndimage.label(occupation_frame,kernel)
    regions = ndimage.find_objects(labeled_frame)
    cluster_list = np.asarray([np.sum(occupation_frame[region]) for region in regions])

    return cluster_list , number_of_hits

#lots of repetition of code here, this will have it's own frame
def histogram_cluster_occupancy(occupancy_frame, bins):
    kernel = np.array([[0,1,0],[1,1,1],[0,1,0]])
    labeled_frame, number_of_hits = ndimage.label(occupancy_frame, kernel)
    regions = ndimage.find_objects(labeled_frame)
    histogram=np.zeros((bins,80,80))
    for region in regions:
        region_size = np.sum(occupancy_frame[region])
        if region_size<bins:
            histogram[region_size][region] += occupancy_frame[region] > 0
    return histogram
def histogram_cluster_energy(energy_frame,occupancy_frame,bins):
    kernel = np.array([[0, 1, 0], [1, 1, 1], [0, 1, 0]])
    labeled_frame, number_of_hits = ndimage.label(occupancy_frame, kernel)
    regions = ndimage.find_objects(labeled_frame)
    histogram = np.zeros((7,bins))
    for region in regions:
        cluster_size = np.sum(occupancy_frame[region])
        cluster_energy=np.sum(energy_frame[region])
        if cluster_size<7 and cluster_energy<105:
            histogram[cluster_size,int(cluster_energy)] += 1
    return histogram




def directory(target_file,filename):

    return lambda a : (target_file + "\\" + a + filename.split("\\")[-1]).split(".")[0]

def read_key():
    if msvcrt.kbhit():
        a=msvcrt.getch()
        return(str(a).split("'")[-2])
    return False

def make_plots_and_save(file_dir,target_file):

    hits=np.zeros((1,7))
    total_clusters = np.empty(256)
    cluster_number = 0
    hits_avg = np.zeros(7)
    display_plots=False


    predicted_frames=os.path.getsize(file_dir)/6400/2
    step=int(predicted_frames/256)
    print("predicted frames", predicted_frames,"step ",step)




    cluster_occupancy=np.zeros((15,80,80))

    with open(file_dir, "rb") as fp:
        total_clusters_hist =np.zeros((15))
        frameCount = 0;
        total_count = np.zeros((80, 80))
        cluster_energy_hist = np.zeros((7, 105))

        read, frame = read_frames(fp)
        while read:  # while file is not empty

            total_count += frame
            occupancy_frame = np.greater(frame, thresh)
            energy_frame = frame
            energy_frame[energy_frame < thresh]=0
            energy_frame = energy_frame * slopes
            energy_frame += intercepts


            clusters, cluster_number = find_cluster_sizes(occupancy_frame)
            clusters_hist, _ = np.histogram(clusters, range(0,16))
            total_clusters_hist = total_clusters_hist + clusters_hist

            cluster_energy_hist += histogram_cluster_energy(energy_frame,occupancy_frame,105)

            key = read_key()
            '''
            if key:
                print(key)
            if key=="\\t":

                display_plots = not display_plots
            '''

            if display_plots:
                plt.figure(figsize = (25,20))
                plt.imshow(cluster_occupancy[1],aspect='auto')
                plt.colorbar()

                while not plt.waitforbuttonpress():pass
                plt.close()
                print("closed")


            if frameCount % 1000 == 0:
                print("Frame count is", frameCount, " clusters histogram ", clusters_hist )
            frameCount += 1

            cluster_occupancy+=histogram_cluster_occupancy(occupancy_frame,15)

            if frameCount % step == 0:

                hits_avg = np.vstack((hits_avg, hits/6400/step))
                hits=np.zeros(7)
            else:
                hits += clusters_hist[0:7]


            read, frame = read_frames(fp)  # read next frame

    #    plt.matshow(M1);


    print("Final frame count is ", frameCount)
    print(total_clusters_hist)
    print(file_dir)
    give_directory = directory(target_file, file_dir)



    plt.bar(np.arange(np.size(total_clusters_hist)), total_clusters_hist)
    plt.xlabel("cluster size")
    plt.ylabel("number of clusters")
    plt.savefig(give_directory("cluster_size_histogram_"), bbox_inches='tight')
    plt.close()

    x = np.linspace(0,np.shape(hits_avg)[0]-1,dtype="int")
    color=['r','g','b','c','m','y','k']
    for i in range(0,7):
        y=hits_avg[:,i]
        y=[y[a] for a in x]
        plt.plot(x, y, color[i], label="hit number of cluster size "+str(i), )






    '''
    plt.xlabel("Bin")
    plt.ylabel("Relative intensity")
    plt.legend(loc = "upper right")
    plt.title("Full Data")
    plt.show(block = True)
    '''
    plt.xlabel("time")
    plt.ylabel("hit fraction")
    plt.legend(loc="upper right")

    plt.title(file_dir)
    plt.savefig(give_directory("occupation_graph_"), bbox_inches='tight')
    plt.close()

    for i in range(0, 15):

        plt.figure(figsize=(25, 20))
        plt.imshow(cluster_occupancy[i], aspect='auto')
        plt.colorbar()
        plt.savefig(give_directory("cluster_size_"+str(i)+ "_hist"), bbox_inches='tight')
        plt.close()

    for i in range(0,7):

        print("plotting histo ",str(i))
        plt.bar(np.arange(np.shape(cluster_energy_hist)[1]), cluster_energy_hist[i,:])
        plt.xlabel("energy")
        plt.ylabel("number of clusters")
        plt.savefig(give_directory("energy_histogram_size"+str(i)), bbox_inches='tight')
        plt.close()






#filename = r"C:\XFCT\ScrollSample_30deg88keVMono_80mm200mm_400umPH_60mins_170716_155317.bin";

root = tk.Tk()   #open data
root.withdraw()

#file_dir=filedialog.askopenfilename()

#file_dir = filedialog.askdirectory().replace("/","\\")
#target_file=filedialog.askdirectory().replace("/","\\")

target_file="C:\\Users\\Maks\\Desktop\\results\crap"

#gives a file dir from filename







frame_buffer = []
'''
histo, number_of_elements= calculate_pixel_response(experiment)
for i in range(0,105):
    if number_of_elements[i] != 0:
        histo[i]=histo[i]/number_of_elements[i]
    plt.bar(np.arange(11), histo[i])
    plt.show()
    print(number_of_elements[i],i, "keV")'''


def smallest_difference(a,array):
    difference=a
    for number in array:
        if abs(a-number)<difference:
            difference=abs(a-number)
    return difference


def squares_difference(a,b):
    squares=0
    for number in b:
        squares+=smallest_difference(number,a)
    return squares

def optimisation(peaks_energy, reference_peaks):
    def fun(gradient):
        scaled_energy = np.copy(peaks_energy)
        scaled_energy *= gradient[1]
        scaled_energy += gradient[0]
        return squares_difference(scaled_energy, reference_peaks)

    return fun
def make_calibration_map(experiment, reference_peaks ,bins=500):
    histogram = Map2D(single_clusters_energy_frame, "single_clusters_energy_frame", (0, 105), bins)
    experiment.make(histogram)
    better_gradients=np.zeros((80,80,2))
    plot_x= np.linspace(0,105-105/bins, bins)
    finish_var=np.zeros((80,80))



    for y in range(0,80):
        for x in range(0,80):
            gradient = [0, 1]
            peaks, _ = signal.find_peaks(histogram.y[int(y), int(x)], prominence=np.sum(histogram.y[int(y), int(x)]) / bins)
            peaks_energy = plot_x[peaks]
            finish_var[y,x] = optimisation(peaks_energy,reference_peaks)(gradient)

            if np.sum(histogram.y[int(y), int(x)]) > bins*2 and peaks.size >= 5:
                gradient_optimisation = optimisation(peaks_energy, reference_peaks)
                better_gradients[y,x,:] = scipy.optimize.minimize(gradient_optimisation, gradient, bounds=((-0.2, 0.2), (0.98, 1.02)))["x"]
                finish_var[y, x] = optimisation(peaks_energy, reference_peaks)(better_gradients[y,x])
            else:
                finish_var[y,x]=3
                better_gradients[y,x,:]=gradient







    return better_gradients,finish_var

def find_compton_peak(counts, energies):
    peak_index = counts.argmax()

    return energies[peak_index]


def find_compton_counts(counts, energies):
    peak_index = counts.argmax()
    return counts[peak_index]

def set_thresh_frame(energy, slopes, intercept, calibration_histogram):
    energy_frame= np.ones((80,80))*energy
    for x in range(0,80):
        for y in range(0,80):
            if np.sum(calibration_histogram.y[x,y])>0.01*calibration_histogram.number_of_frames:
                energy_frame[x,y]=5
    thresh=(energy_frame- intercept)/slopes
    return thresh #return 3eV treshold everywhere except for hot pixels


def dodgy_calibration(thresh,slopes,intercepts): #re calibrate frames using gold and lead
    gold_peaks=[9.713,11.442,68.803,77.984]
    lead_peaks=[10.551,12.613,72.804,74.969]

    file_dir="E:\Diamond 2017\Day 3\PSF\\PSF_10um_Lead_Foil_105keV_400um_PH_35mm_170mm_60s_170714_142628.bin"
    experiment = Experiment(file_dir, thresh, slopes, intercepts)
    calibration_map, lead_variation = make_calibration_map(experiment, lead_peaks, 200)
    slopes_lead = slopes*calibration_map[:,:,1]
    intercepts_lead = intercepts*calibration_map[:,:,1] + calibration_map[:,:,0]


    file_dir="E:\\Diamond 2017\\Day 5\\30IshDeg88keVMono400umPH\\GoldGrid_30deg88keVMono_80mm200mm_400umPH_5mins_170716_153557.bin"
    experiment = Experiment(file_dir, thresh, slopes, intercepts)
    calibration_map, gold_variation = make_calibration_map(experiment, gold_peaks, 200)
    slopes_gold = slopes*calibration_map[:,:,1]
    intercepts_gold = intercepts*calibration_map[:,:,1] + calibration_map[:,:,0]

    intercepts=np.where(np.less(gold_variation,lead_variation),intercepts_gold,intercepts_lead)
    slopes=np.where(np.less(gold_variation,lead_variation),slopes_gold,slopes_lead)

    corrected_thresh= np.floor((3 * np.ones((80, 80)) - intercepts) / slopes).astype(int)
    corrected_thresh = np.where(corrected_thresh > thresh, corrected_thresh, thresh)
    np.savetxt("calibrated_slopes.txt",slopes)
    np.savetxt("calibrated_intercepts.txt",intercepts)
    np.savetxt("calibrated_threshold.txt", corrected_thresh)

    return True
def log(x):

    return np.log(x)

def flog(f):
    def fun(*x):
        return log(f(*x))
    return fun

def gaussian(x,amplitude, peak_energy, variance ):
    gaussian= amplitude * np.exp(-(x-peak_energy) ** 2 / (2. * variance ** 2))
    return gaussian


def compton_fit(x,peak_energy, amplitude, variance, f_g, f_a, f_b, gamma_a, gamma_b ):
    delta_E=(x - peak_energy)
    gaussian= amplitude * np.exp(-(1/2)*((delta_E / (f_g * variance))**2))
    T_a = amplitude* np.exp(delta_E/(gamma_a*variance)+1/(2*gamma_a**2))*scipy.special.erfc(delta_E/(math.sqrt(2)*variance)+1/(math.sqrt(2)*gamma_a))
    T_b = amplitude* np.exp(-delta_E/(gamma_b*variance)+1/(2*gamma_b**2))*scipy.special.erfc(-delta_E/(math.sqrt(2)*variance)+1/(math.sqrt(2)*gamma_b))

    return gaussian+f_a*T_a+f_b*T_b
def line_fit(x, line_a,line_b,line_start=65):
    y=np.where(x>line_start,line_b+line_a*(x-line_start),0)
    y[y<0]=0
    return y

def compton_fit_with_background(x,peak_energy, amplitude, variance, f_g, f_a, f_b, gamma_a, gamma_b,  line_a,line_b , line_start=68):
    delta_E=(x - peak_energy)
    gaussian= amplitude * np.exp(-(1/2)*((delta_E / (f_g * variance))**2))
    T_a = amplitude* np.exp(delta_E/(gamma_a*variance)+1/(2*gamma_a**2))*scipy.special.erfc(delta_E/(math.sqrt(2)*variance)+1/(math.sqrt(2)*gamma_a))
    T_b = amplitude* np.exp(-delta_E/(gamma_b*variance)+1/(2*gamma_b**2))*scipy.special.erfc(-delta_E/(math.sqrt(2)*variance)+1/(math.sqrt(2)*gamma_b))
    return gaussian+f_a*T_a+f_b*T_b


def big_compton_fit(x,peak_energy_a,amp, peak_energy, amplitude, variance, f_g, f_a, f_b, gamma_a, gamma_b):
    delta_E = (x - peak_energy)
    gaussian = amplitude * np.exp(-(1 / 2) * ((delta_E / (f_g * variance)) ** 2))/variance
    T_a = amplitude * np.exp(delta_E / (gamma_a * variance) + 1 / (2 * gamma_a ** 2)) * scipy.special.erfc(
        delta_E / (math.sqrt(2) * variance) + 1 / (math.sqrt(2) * gamma_a))/variance/gamma_a
    T_b = amplitude * np.exp(-delta_E / (gamma_b * variance) + 1 / (2 * gamma_b ** 2)) * scipy.special.erfc(
        -delta_E / (math.sqrt(2) * variance) + 1 / (math.sqrt(2) * gamma_b))/variance/gamma_b
    peak_a = 6/10*amp * np.exp(-(1 / 2) * ((x-peak_energy_a) / (variance)) ** 2)/variance
    peak_b = amp * np.exp(-(1 / 2) * ((x-peak_energy_a-2.165) / (variance)) ** 2)/variance
    peak_c = 12/100*amp * np.exp(-(1 / 2) * ((x - peak_energy_a - 11.646) / (variance)) ** 2) / variance
    peak_d = 23/100*amp * np.exp(-(1 / 2) * ((x - peak_energy_a - 12.132) / (variance)) ** 2) / variance

    return gaussian + f_a * T_a + f_b * T_b+peak_a+peak_b+peak_c+peak_d

def peak_fit(x,peak_energy_a,amp,variance):
    peak_a = 6/10*amp * np.exp(-(1 / 2) * ((x-peak_energy_a) / (variance)) ** 2)/variance
    peak_b = amp * np.exp(-(1 / 2) * ((x-peak_energy_a-2.165) / (variance)) ** 2)/variance
    peak_c = 12/100*amp * np.exp(-(1 / 2) * ((x - peak_energy_a - 11.646) / (variance)) ** 2) / variance
    peak_d = 23/100*amp * np.exp(-(1 / 2) * ((x - peak_energy_a - 12.132) / (variance)) ** 2) / variance
    return peak_a+peak_b+peak_c+peak_d

def high_peak_fit(x,peak_energy_a,amp,variance):
    peak_a = 6/10*amp * np.exp(-(1 / 2) * ((x-peak_energy_a) / (variance)) ** 2)/variance
    peak_b = amp * np.exp(-(1 / 2) * ((x-peak_energy_a-2.165) / (variance)) ** 2)/variance
    return peak_a+peak_b

def align(f_a,f_b):
    def lol(a, b):
        return a*f_a+b*f_b
    return lol


def two_lead_peak(histogram,y,x): #return energy under peak in histogram at x,y




    flat_indeces = np.nonzero(np.logical_and(histogram.x > 65, histogram.x <= 70))
    flat_x= histogram.x[flat_indeces]
    flat_y= histogram.y[y,x][flat_indeces]
    lin_max=np.amax( histogram.y[y,x][flat_indeces] )
    if lin_max < 1:
        lin_max = 1


    lin_fit_parameters, _ = scipy.optimize.curve_fit(line_fit, flat_x, flat_y,
    p0=[-5,lin_max/2],
    bounds=((-100,0),(-1,lin_max)))

    corrected_histo = np.copy(histogram.y[y, x])
    region_indeces = np.nonzero(np.logical_and(histogram.x > 70, histogram.x < 90))
    region_x = histogram.x[region_indeces]

    corrected_histo[region_indeces]= corrected_histo[region_indeces] - line_fit(region_x, *lin_fit_parameters)


    compton_indeces = np.nonzero(np.logical_and(histogram.x > 77, histogram.x < 100))
    compton_y = corrected_histo[compton_indeces]
    compton_x = histogram.x[compton_indeces]
    compton_energy = histogram.x[np.argmax(corrected_histo)]
    peak_indeces = np.nonzero(np.logical_and(histogram.x > 70, histogram.x < 77))

    peak_x = histogram.x[peak_indeces]


    if np.sum(compton_y)>500:
        try:
            fit_parameters, _ = scipy.optimize.curve_fit(compton_fit, compton_x, compton_y,
            p0=[compton_energy,500,0.69, 1.07, 0.42, 0.28,4.90,2.88],
            bounds=((86, 0.01, 0.2, 1, 0.01, 0.01, 0.01, 0.01),(91, 2000, 1.2, 50, 15, 15, 100, 100)))
            print(x,y)


        except:
            print("fail 1st fit")
            fit_parameters = [1, 1, 1, 1, 1, 1, 1, 1]
            return 0, 0, np.concatenate((np.zeros(3), fit_parameters, np.zeros(2)),axis=None)
    else:
        fit_parameters = [1, 1, 1, 1, 1, 1, 1, 1]
    '''
    try:
        scaled_params, _ =scipy.optimize.curve_fit(scaled_params(compton_fit(np.append(flat_x,compton_x),*fit_parameters),line_fit( np.append(flat_x,compton_x),*lin_fit_parameters))
        , np.append(flat_x,compton_x), np.append(flat_y,compton_y), p0=[1,1],bounds=((0,0),(2,2)))
    except:
        scaled_params = [1,1]
    '''

    corrected_histo = np.copy(histogram.y[y, x])
    corrected_histo[region_indeces] = corrected_histo[region_indeces]-compton_fit(region_x,*fit_parameters)-line_fit(region_x,*lin_fit_parameters)


    region_x = histogram.x[region_indeces]
    peak_x = histogram.x[peak_indeces]
    peak_y = histogram.y[y,x][peak_indeces]
    flat_x = histogram.x[flat_indeces]
    background_avg= np.mean(histogram.y[y,x][flat_indeces])



    region_y = corrected_histo[region_indeces]

    try:
        fit_parameters2, _ = scipy.optimize.curve_fit(peak_fit, region_x, region_y , p0=[73,0,0.4],bounds=((71.5,0,0.2),(75.5,500,0.6)))
    except:
        fit_parameters2=[72,0,0.5]
        print("fail 2nd fit")
    area= np.sum(peak_fit(region_x, *fit_parameters2))
    if fit_parameters2[1]<10:
        area=0
    return 0, area, np.concatenate((fit_parameters2,fit_parameters,lin_fit_parameters),axis= None)





def new_find_lead_peak(histogram,y,x): #return energy under peak in histogram at x,y



    fit_indeces = np.nonzero(np.logical_and(histogram.x > 71, histogram.x < 100))

    fit_x = histogram.x[fit_indeces]
    fit_y =histogram.y[y,x][fit_indeces]


    peak_indeces= np.nonzero(np.logical_and(fit_x > 73.5, fit_x < 77))
    compton_indeces= np.nonzero(np.logical_and(fit_x > 77, fit_x < 97))

    fit_y[compton_indeces] = scipy.signal.savgol_filter(fit_y[compton_indeces], 13 , 3)
    fit_y[fit_y < 0] = 0

    peak_y= fit_y[peak_indeces]
    peak_x= fit_x[peak_indeces]

    compton_y = fit_y[compton_indeces]
    compton_x = fit_x[compton_indeces]

    compton_energy= compton_x[np.argmax(compton_y)]
    Pb_line_energy = peak_x[np.argmax(peak_y)] - 2.165
    if Pb_line_energy < 71.7:
        Pb_line_energy=72.804
    print(compton_energy,Pb_line_energy)



    if np.sum(fit_y)>1000:
        try:
            fit_parameters, _ = scipy.optimize.curve_fit(big_compton_fit, fit_x, fit_y,
            p0=[Pb_line_energy,10, compton_energy,500,0.556, 1.46, 0.33, 0.22,7.11,3.86],
            bounds=((71.8,0, 86, 0, 0.15, 1, 0.01, 0.01, 0.01, 0.01 ),(74.7,100,91, 1000, 1.2, 5, 15, 15, 100, 100)))

        except:
            fit_parameters=[Pb_line_energy,10, compton_energy,300,0.4, 4.08, 3.92, 1.86,20.05,4.15]
            print("failll")



        area= fit_parameters[1]
        corrected_area=area / np.sum(fit_y)
        print(fit_parameters)
    else:

        area=0
        fit_parameters = [Pb_line_energy,10, compton_energy,30,0.4, 4.08, 3.92, 1.86,20.05,4.15]

        corrected_area=area / np.sum(fit_y)


    return corrected_area, area, fit_parameters

def find_lone_peak_area(energy, histogram,y,x): #return energy under peak in histogram at x,y

    peak_indeces= np.nonzero(np.logical_and(histogram.x > energy-2,histogram.x < energy+5))
    peak_y= histogram.y[y,x][peak_indeces]
    area= np.sum(peak_y)
    return area
'''
plt.figure(figsize = (25,20))
plt.imshow(thresh*slopes+intercepts, aspect='auto')
print("energy threshold", thresh*slopes+intercepts)
plt.colorbar()
plt.show()
plt.close()'''

slopes = fast_order(np.loadtxt(r"500V_28C_Grads.txt"))
intercepts = fast_order(np.loadtxt(r"500V_28C_Inters.txt"))
thresh = fast_order(np.loadtxt(r"500V_28C_Thresh.txt"))

#dodgy_calibration(thresh, slopes, intercepts)
corrected_slopes = np.loadtxt("calibrated_slopes.txt")
corrected_intercepts = np.loadtxt("calibrated_intercepts.txt")

corrected_thresh= np.floor((3 * np.ones((80, 80)) - corrected_intercepts) / corrected_slopes).astype(int)
corrected_thresh = np.where(corrected_thresh > thresh, corrected_thresh, thresh)
'''
change_map = np.greater(np.absolute(corrected_slopes-slopes)/slopes,0.005)
plt.imshow(change_map)
plt.show()
plt.close()'''

file_dir="E:\\Diamond 2017\\Day 5\\30IshDeg88keVMono400umPH\\HERCmultiSample_30IshDeg88keVMono400umPH_60_170716_134603.bin"
experiment=Experiment(file_dir, corrected_thresh, corrected_slopes, corrected_intercepts)


histogram= Map2D(single_clusters_energy_frame, "single_clusters_energy_frame",(0,105),2100)

'''
np.save("HERC_400bin_y", histogram.y)
np.save("HERC_400bin_x", histogram.x)
'''

histogram.x=np.load("ScrollOvernight_2100bin_x.npy")
histogram.y=np.load("ScrollOvernight_2100bin_y.npy")
histogram.reduce_frames(5)


Histo1D = Histogram(single_clusters_energy, "single_clusters_energy", (0,105),2100)
Histo1D.x = np.load("ScrollOvernight_2100bin_x.npy")
Histo1D.y = np.sum(np.load("ScrollOvernight_2100bin_y.npy"),axis=(0,1))
print(Histo1D.y)
Histo1D.plot()


area=np.zeros((80,80))
corrected_area=np.zeros((80,80))
peak_parameters=np.zeros((80,80,13))
'''
kernel=np.ones((2,2))
for i in range(0,histogram.y.shape[-1]):
    histogram.y[:,:,i]=scipy.signal.convolve2d(histogram.y[:,:,i],kernel,mode="same")
'''


for y in range(20,60):
    for x in range(20,60):

        corrected_area[y,x],area[y,x], peak_parameters[y,x] = two_lead_peak(histogram, y, x)

'''
chamsko=np.zeros((80,80))
for x in range(20,60):
    for y in range(20,60):
        if np.sum(histogram.y[x,y])>2000:
            chamsko[x,y] = find_lone_peak_area(73,histogram, x, y)/np.sum(histogram.y[x,y])
plt.figure(figsize=(25, 20))
plt.title("area")
plt.imshow(chamsko, aspect='auto')
plt.colorbar()
plt.show()
'''

names= ("lead peak_energy", "lead amplitude", "lead variance","compton peak_energy", "compton amplitude", "variance", "f_g", "f_a", "f_b", "gamma_a", "gamma_b","line start","line b" , "line a" )

plt.figure(figsize=(25, 20))
plt.title("area")
plt.imshow(area, aspect='auto')
plt.colorbar()
plt.show()

plt.figure(figsize=(25, 20))
plt.title("corrected area")
plt.imshow(corrected_area, aspect='auto')
plt.colorbar()
plt.show()

kernel= np.ones((2,2))
convolved_area=signal.convolve2d(area,kernel,mode="same")
convolved_corrected_area=signal.convolve2d(corrected_area,kernel,mode="same")

plt.figure(figsize=(25, 20))
plt.title("convolved area")
plt.imshow(convolved_area, aspect='auto')
plt.colorbar()
plt.show()

plt.figure(figsize=(25, 20))
plt.title("convolved corrected area")
plt.imshow(convolved_corrected_area, aspect='auto')
plt.colorbar()
plt.show()


for i in range(0,10):
    plt.figure(figsize=(25, 20))
    plt.title(names[i])
    plt.imshow(peak_parameters[:,:,i], aspect='auto')
    plt.colorbar()
    plt.show()

while True:
    plt.figure(figsize=(25, 20))
    plt.title("area")
    plt.imshow(area, aspect='auto')
    plt.colorbar()
    plt.show()


    x = input()
    y = input()
    x=int(x)
    y=int(y)

    area_indeces = np.logical_and(histogram.x > 58, histogram.x < 99)
    area_x = histogram.x[area_indeces]
    area_y = histogram.y[y, x][area_indeces]
    fit_y=np.zeros(area_x.size)
    fit_y = compton_fit(area_x,*peak_parameters[y,x][3:11])+peak_fit(area_x,*peak_parameters[y,x][0:3])+line_fit(area_x,*peak_parameters[y,x][11:])

    corrected = area_y - fit_y


    params = dict(zip(names, peak_parameters[y,x]))
    print("coords",y ,x )
    print("area ",area[y,x])
    print("params",params)
    plt.plot(area_x, area_y, "r")
    plt.plot(area_x, fit_y, "b")
    plt.plot(area_x, corrected, "g")
    plt.show()
    plt.close()


    if x=="exit":
        break



plt.figure(figsize=(25, 20))
plt.imshow(peak_energies, aspect='auto')
plt.colorbar()
plt.show()





histogram.apply(find_compton_peak)
x=input()
while True:

    plt.figure(figsize = (25,20))
    plt.imshow(histogram.heatmap, aspect='auto')
    plt.colorbar()
    plt.show()

    calibration=np.zeros((80,80))
    plt.figure(figsize=(25, 20))
    plt.imshow(np.sum(calibration_histogram.y,axis=2), aspect='auto')
    plt.colorbar()
    plt.show()

    x=input("x coordinate")
    y=input("y coordinate")

    plt.bar(histogram.x, histogram.y[int(x), int(y)], width=1.01 * (105) /200)
    plt.show()
    plt.close()







histogram.apply(find_compton_counts)
plt.figure(figsize = (25,20))
plt.imshow(histogram.heatmap, aspect='auto')
plt.colorbar()
plt.show()



"""
while True:
    plot_x= np.linspace(0,105,500)

    peaks, _ = signal.find_peaks(histogram.y[int(x),int(y)], prominence= np.sum(histogram.y[int(x),int(y)])/500)
    peaks_energy = plot_x[peaks]
    plt.plot(plot_x[peaks], histogram.y[int(x),int(y),peaks], "x")
    plt.bar(plot_x, histogram.y[int(x),int(y)], width=1.01 * (105) / 500)

    gradient_optimisation= optimisation(peaks_energy, gold_peaks)
    gradient = [0,1]
    better_gradients=scipy.optimize.minimize(gradient_optimisation, gradient,bounds=((-1,1),(0.98,1.02)))["x"]
    print(better_gradients, "cokurwa")

    print(squares_difference(peaks_energy, gold_peaks), "difference squared")
    print(peaks_energy, "found peaks")
    print(gold_peaks, "expected" , "\n")

    corrected_energy = peaks_energy * better_gradients[1] + better_gradients[0]
    print(squares_difference(corrected_energy, gold_peaks), "difference squared corrected")
    print(corrected_energy, "found peaks corrected")
    print(gold_peaks, "expected")


    plt.show()
    plt.close()

    x=input()
    y=input()



histogram.plot()
"""














#apply_to_files_and_save(file_dir,"bin", make_plots_and_save , target_file)
#make_plots_and_save(file_dir,target_file)

raise SystemExit
