import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import cv2
import sys

from read_function import read_file

#creates a circular point spread function of diameter d
def defocus_kernel(d, sz=30):
    kern = np.zeros((sz, sz), np.uint8)
    cv2.circle(kern, (sz, sz), d, 255, -1, cv2.LINE_AA, shift=1)
    kern = np.float32(kern) / 255.0
    return kern

#reading known good files for signal and background
d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\PSF_10um_Lead_Foil_105keV_400um_PH_35mm_170mm_600s_170714_142747.hxt")

#d1, nBins1 = read_file(r"C:\XFCT\Data\Medium_Eps_33mmPHtoSample_400umPH_105keV_60s_Pb_Flight_Tube_W_Exit_Tube_170713_113737.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Alex\data\TEST3SIGNAL.hxt")
#d2, nBins2 = read_file(r"C:\XFCT\Alex\data\TEST3BACKGROUND.hxt")

#scalefactors for signal and background in acceptance window
events_to_scale_signal = np.sum(d1[40:65, 20:45, 320:324])
#events_to_scale_background = np.sum(d2[40:65, 20:45, 320:324])
#scaleMe = events_to_scale_signal/events_to_scale_background

#signal and background slices    
m1 = np.zeros( (80,80) );
#m2 = np.zeros( (80,80) );

#filling slices with lead k lines
for j in range(0, 80):
    for k in range (0, 80):
        m1[j][k] += np.sum(d1[j, k, 290:294])+np.sum(d1[j, k, 298:302])#+np.sum(d1[j, k, 338:342])
#        m2[j][k] += np.sum(d2[j, k, 290:294])+np.sum(d2[j, k, 298:302])#+np.sum(d2[j, k, 338:342])

#background subtraction
#m1-=m2*scaleMe

#storing name for deconvolution window
win = 'deconvolution'

#generating cv2 compatible image from input
img = np.float32(m1)/np.float32(np.amax(m1))

height, width = img.shape[:2]
#showing input data
cv2.namedWindow('input', 0)
cv2.startWindowThread()
cv2.resizeWindow('input', 600,600)
cv2.imshow('input', img)
cv2.namedWindow('psf', 0)
cv2.resizeWindow('psf', 600,600)

plt.ion()

fig = plt.figure()
ax = fig.add_subplot(1,1,1)

#initial deconvolution values
d = 7
noise = 10**(-0.1*1)

#generating initial deconvolution
psf = defocus_kernel(d)
cv2.imshow('psf', psf)
psf /= psf.sum()
psf_pad = np.zeros_like(img)
kh, kw = psf.shape
psf_pad[:kh, :kw] = psf
PSF = cv2.dft(psf_pad, flags=cv2.DFT_COMPLEX_OUTPUT, nonzeroRows = kh)
PSF2 = (PSF**2).sum(-1)
iPSF = PSF / (PSF2 + noise)[...,np.newaxis]
IMG = cv2.dft(img, flags=cv2.DFT_COMPLEX_OUTPUT)
RES = cv2.mulSpectrums(IMG, iPSF, 0)
res = cv2.idft(RES, flags=cv2.DFT_SCALE | cv2.DFT_REAL_OUTPUT )
res = np.roll(res, -kh//2, 0)
res = np.roll(res, -kw//2, 1)
#showing slice histogram(x) through acceptance region
histo_res = res[55, :]
histo_img = img[55, :]
line1, = ax.plot(np.linspace(0, 80, 80), histo_res, 'r')
line2, = ax.plot(np.linspace(0, 80, 80), histo_img, 'b')
plt.show(block = False)

#callback function for updating deconvolution parameters
def update(_):
    #getting updating parameters from trackbars
    d = cv2.getTrackbarPos('d', win)
    noise = 10**(-0.1*cv2.getTrackbarPos('SNR (db)', win))

    psf = defocus_kernel(d)
    cv2.imshow('psf', psf)

    psf /= psf.sum()
    psf_pad = np.zeros_like(img)
    kh, kw = psf.shape
    psf_pad[:kh, :kw] = psf
    PSF = cv2.dft(psf_pad, flags=cv2.DFT_COMPLEX_OUTPUT, nonzeroRows = kh)
    PSF2 = (PSF**2).sum(-1)
    iPSF = PSF / (PSF2 + noise)[...,np.newaxis]
    RES = cv2.mulSpectrums(IMG, iPSF, 0)
    res = cv2.idft(RES, flags=cv2.DFT_SCALE | cv2.DFT_REAL_OUTPUT )
    res = np.roll(res, -kh//2, 0)
    res = np.roll(res, -kw//2, 1)
    res = np.float32(res)/np.float32(np.amax(res))
    histo_res = res[55, :]
    histo_img = img[55, :]
    line1.set_ydata(histo_res)
    line2.set_ydata(histo_img)
    fig.canvas.draw()
    cv2.imshow(win, res)
#generating output windows with trackbars
cv2.namedWindow(win, cv2.WINDOW_NORMAL)
cv2.resizeWindow(win, 600,600)
cv2.createTrackbar('d', win, int(7), 25, update)
cv2.createTrackbar('SNR (db)', win, int(1), 50, update)
update(None)

#required to keep the program running
while True:
    ch = cv2.waitKey(0) & 0xFF
    if ch == 32 or ch == 27:
        break

cv2.destroyAllWindows()
raise SystemExit


