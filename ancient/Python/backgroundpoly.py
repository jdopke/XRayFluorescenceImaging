import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2

from scipy.optimize import curve_fit
from scipy import asarray as ar,exp

from read_function import read_file

#creates a circular point spread function of diameter d
def defocus_kernel(d, sz=30):
    kern = np.zeros((sz, sz), np.uint8)
    cv2.circle(kern, (sz, sz), d, 255, -1, cv2.LINE_AA, shift=1)
    kern = np.float32(kern) / 255.0
    return kern



#d1, nBins1 = read_file(r"C:\XFCT\Data\Medium_Eps_33mmPHtoSample_400umPH_105keV_60s_Pb_Flight_Tube_W_Exit_Tube_170713_113737.hxt")
d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Scroll_105keV_400um_PH_35mm_170mm_8hrs_170714_002257.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\PSF_10um_Lead_Foil_105keV_400um_PH_35mm_170mm_600s_170714_142747.hxt")

divider = 1

m1 = np.zeros( (80/divider+1,80/divider+1) );
m2 = np.zeros( (80/divider+1,80/divider+1) );
m3 = np.zeros( (80/divider+1,80/divider+1) );

start_bin = 60
end_bin = 87

for j in range(divider, 80, divider):
    for k in range (divider, 80, divider):
        histo1 = np.zeros( d1.shape[2] )
        for i in range(0, d1.shape[2]):
            histo1[i] = 0.01+np.sum(d1[j:j+divider, k:k+divider,i])#/events_to_scale_signal*10.24
        x_fit = np.concatenate((np.linspace(start_bin, 72, (72-start_bin)*4), np.linspace(77, end_bin, (end_bin-77)*4)))
        y_fit = np.log(np.concatenate((histo1[start_bin*4:288], histo1[308:end_bin*4])))
        fit = np.polyfit(x_fit,y_fit,3)
        x_eval = np.linspace(72,77, 20)
        y_eval = np.exp(np.polynomial.polynomial.polyval(x_eval,np.flip(fit,0)))

        m1[j/divider][k/divider] = np.sum(histo1[288:308]-y_eval)#+np.sum(d1[j:j+divider, k:k+divider, 338:354])#lead
        m2[j/divider][k/divider] = np.sum(histo1[288:308])#(2*np.sum(d1[j:j+divider, k:k+divider, 276:288])-np.sum(d1[j:j+divider, k:k+divider, 264:276]))
        m3[j/divider][k/divider] = np.sum(y_eval)#(2*np.sum(d1[j:j+divider, k:k+divider, 304:312])-np.sum(d1[j:j+divider, k:k+divider, 312:320]))
        #m1[j/divider][k/divider] -= (m2[j/divider][k/divider]+m3[j/divider][k/divider])*3/5
        #if (m1[j/divider][k/divider] < 0):
        #    m1[j/divider][k/divider] = 0
        
        #m1[j][k] += np.sum(d1[j, k, 266: 270])+np.sum(d1[j, k, 274:278])+np.sum(d1[j, k, 310:314])#gold
        #m1[j][k] += np.sum(d1[j, k, 350:354])#scatter

#to plot figure with colourbar

print (np.sum(m1))
#print (np.sum(m2))

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(m1, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\Clean_Signal.png")  
plt.close()

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(m2, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\Signal.png")  
plt.close()

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(m3, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\Background.png")  
plt.close()

raise SystemExit
