import numpy as np
import struct

#function to read hexitec data
# first return value is numpy ndarray with dimensions 80,80,800
# second return value is number of bins in energy
# could return all dimensions
# could return status
def read_file(file_address):
    mssX = 0;
    mssY = 0;
    mssZ = 0;
    mssRot = 0;
    GalX = 0;
    GalY = 0;
    GalZ = 0;
    GalRot = 0;
    GalRot2 = 0;
    nCharFPreFix = 0;
    filePreFix = "";
    dummy = "";
    timeStamp = "";
    nRows = 0;
    nCols = 0;
    nBins = 0;
    bins = 0.0;
    d = 0.0;
    with open(file_address, "rb") as f:
        f.read(8)
        version = struct.unpack("Q", f.read(8))[0]
        if version != 3:
            #ignore everything here
            M = 0
            bins = 0
        else :
            mssX = struct.unpack("I", f.read(4))[0];
            mssY = struct.unpack("I", f.read(4))[0];
            mssZ = struct.unpack("I", f.read(4))[0];
            mssRot = struct.unpack("I", f.read(4))[0];
            GalX = struct.unpack("I", f.read(4))[0];
            GalY = struct.unpack("I", f.read(4))[0];
            GalZ = struct.unpack("I", f.read(4))[0];
            GalRot = struct.unpack("I", f.read(4))[0];
            GalRot2 = struct.unpack("I", f.read(4))[0];
            nCharFPreFix = struct.unpack("i", f.read(4))[0];
            filePreFix = (f.read(nCharFPreFix)).decode("utf-8");
            dummy = (f.read(100-nCharFPreFix)).decode("utf-8");
            timeStamp = (f.read(16)).decode("utf-8");
            nRows = struct.unpack("I", f.read(4))[0];
            nCols = struct.unpack("I", f.read(4))[0];
            nBins = struct.unpack("I", f.read(4))[0];
            bins = struct.unpack("d" * nBins, f.read(nBins*8));
            d= np.reshape(np.asarray(struct.unpack("d"*nBins*nRows*nCols, f.read(nBins*nRows*nCols*8))), (80, 80, nBins))
        return(d,nBins)

