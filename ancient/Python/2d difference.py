import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2

from scipy.optimize import curve_fit
from scipy import asarray as ar,exp

from read_function import read_file

#creates a circular point spread function of diameter d
def defocus_kernel(d, sz=30):
    kern = np.zeros((sz, sz), np.uint8)
    cv2.circle(kern, (sz, sz), d, 255, -1, cv2.LINE_AA, shift=1)
    kern = np.float32(kern) / 255.0
    return kern



d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Recreation_Sample_Lambda_Fresh_Lead_Plot_88keV_600s_170715_110242.hxt")
d2, nBins2 = read_file(r"C:\XFCT\Herculaneum2017\Recreation_Sample_Lambda_Fresh_Lead_Plot_87keV_600s_170715_111617.hxt")

divider = 1

m1 = np.zeros( (80/divider+1,80/divider+1) );
m2 = np.zeros( (80/divider+1,80/divider+1) );
m3 = np.zeros( (80/divider+1,80/divider+1) );

for j in range(divider, 80, divider):
    for k in range (divider, 80, divider):
        #m1[j/divider][k/divider] = np.sum(d1[j:j+divider, k:k+divider, 338:344])#np.sum(d1[j:j+divider, k:k+divider, 288:298])+np.sum(d1[j:j+divider, k:k+divider, 298:304])+np.sum(d1[j:j+divider, k:k+divider, 338:344])#lead
        #m2[j/divider][k/divider] = np.sum(d2[j:j+divider, k:k+divider, 338:344])#np.sum(d2[j:j+divider, k:k+divider, 288:298])+np.sum(d2[j:j+divider, k:k+divider, 298:304])+np.sum(d2[j:j+divider, k:k+divider, 338:344])#lead
        m1[j/divider][k/divider] = np.sum(d1[j:j+divider, k:k+divider, 288:298])+np.sum(d1[j:j+divider, k:k+divider, 298:304])+np.sum(d1[j:j+divider, k:k+divider, 338:344])/np.amax(d1[j:j+divider, k:k+divider, :])#lead
        m2[j/divider][k/divider] = np.sum(d2[j:j+divider, k:k+divider, 288:298])+np.sum(d2[j:j+divider, k:k+divider, 298:304])+np.sum(d2[j:j+divider, k:k+divider, 338:344])/np.amax(d1[j:j+divider, k:k+divider, :])#lead


histo1 = np.zeros( nBins1 );
histo2 = np.zeros( nBins2 );

for i in range(0, nBins1):
    histo1[i] = np.sum(d1[:,:,i])#/events_to_scale_signal*10.24
    histo2[i] = np.sum(d2[:,:,i])#/events_to_scale_signal*10.24

plt.plot(np.linspace(60, 100, 160), histo1[240:400], 'r', label = "Papyrus");
plt.plot(np.linspace(60, 100, 160), histo2[240:400], 'g', label = "Background");
plt.xlabel("Energy[keV]")
plt.ylabel("Relative intensity")
plt.legend(loc = "upper right")
plt.title("Full Area Spectrum")
figure = plt.gcf() # get current figure
figure.set_size_inches(8, 6)
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum.png", dpi = 300)
plt.close()


#to plot figure with colourbar

print (np.sum(m1))
print (np.sum(m2))

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(m1, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\Signal.png")  
plt.close()

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(m2, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\Background.png")  
plt.close()

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow((m1-m2), cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\Difference.png")  
plt.close()

raise SystemExit
