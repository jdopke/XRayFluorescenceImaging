import numpy as np
import struct
import os
import matplotlib.pyplot as plt

from read_function import read_file

#get all file names in directory    
names = os.listdir(r"C:\Users\ajm18458\Documents\data")

#start bin and end bin for fitting
start_bin = 71
end_bin = 82

#looping over all files
for i in names:
    #only picking files with the correct extension
    if ".hxt" in i:
        #loading current file
        d1, nBins1 = read_file(r"C:\Users\ajm18458\Documents\data\\"+i)
        #histogramming spectrum
        histo1 = np.zeros( nBins1/2 );
        for m in range(0, nBins1/2):
            histo1[m] = np.sum(d1[:,:,m])
                    
        #generating x coordinates for fit, excluding lead k alpha
        x_fit = np.concatenate((np.linspace(start_bin, 72, (72-start_bin)*4), np.linspace(77, end_bin, (end_bin-77)*4)))
        #selecting histogram data without lead k alpha
        y_fit = np.concatenate((histo1[start_bin*4:288], histo1[308:end_bin*4]))
        #fitting 3rd order polynomial
        fit = np.polyfit(x_fit,y_fit,3)
        #x range from start bin to end bin
        x_eval = np.linspace(start_bin,end_bin,(end_bin-start_bin)*4)
        #filling y values with fitted polynomial
        y_eval = np.polynomial.polynomial.polyval(x_eval,np.flip(fit,0))
        #subtracting polynomial from spectrum
        y_diff = histo1[start_bin*4:end_bin*4]-y_eval

        #plotting background subtracted spectrum
        plt.plot(x_eval, y_diff, "b")
        plt.xlabel("Energy[keV]")
        #removing extension from input file name and saving figure
        name2 = i[:-4]
        plt.savefig(r"C:\Users\ajm18458\Documents\Output\\" +name2+ "_diff.png")
        plt.close() 
