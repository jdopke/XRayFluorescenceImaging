import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import cv2

#plt.xkcd()

from read_function import read_file
    
#reading known good data file for signal/background/gold
d1, nBins1 = read_file(r"C:\XFCT\Alex\data\TEST3SIGNAL.hxt")
d2, nBins2 = read_file(r"C:\XFCT\Alex\data\TEST3BACKGROUND.hxt")
d3, nBins3 = read_file(r"C:\XFCT\Alex\data\TEST3GOLD.hxt")

if nBins1!=nBins2:
    print("error")

#getting total event count for signal/background/gold
events_to_scale_signal = np.sum(d1)
events_to_scale_background = np.sum(d2)
events_to_scale_gold = np.sum(d3)

#scale factor to scale background to signal
#i.e. background*scaleMe ~ signal
scaleMe = events_to_scale_signal/events_to_scale_background

#signal and background slices    
m_signal = np.zeros( (80,80) );
m_background = np.zeros( (80,80) );

#signal and background histograms
histo1 = np.zeros( nBins1/2 );
histo2 = np.zeros( nBins2/2 );

for i in range(0, nBins1/2):
    histo1[i] = np.sum(d1[40:65,20:45,i])
    histo2[i] = np.sum(d2[40:65,20:45,i])

#plotting full histograms signal/background
plt.plot(np.linspace(0, nBins1/8, nBins1/2), histo1, 'r');
plt.plot(np.linspace(0, nBins2/8, nBins2/2), histo2, 'b');
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum.png")  
plt.close()

#plotting difference histogram
#scalefactor taken from scatter peak at 87-89 keV
scalefactor2 = np.sum(histo1[348:356])/np.sum(histo2[348:356])
histo3 = histo1-(histo2*scalefactor2)
plt.plot(np.linspace(0, nBins2/8, nBins2/2), histo3, 'b');
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum_diff.png")  
plt.close()

#plotting zoomed spectra
plt.plot(np.linspace(74, 77, 12), histo1[296:308], 'r');
plt.plot(np.linspace(74, 77, 12), histo2[296:308], 'b');
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum_zoom.png")  
plt.close()

#plotting zoomed difference histogram
plt.plot(np.linspace(72, 77, 20), histo3[288:308], 'b');
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum_diff_zoom.png")  
plt.close()

#filling the slices
for j in range(0, 80):
    for k in range (0, 80):
        m_signal[j][k] += np.sum(d1[j, k, 290:294])+np.sum(d1[j, k, 298:302])#+np.sum(d1[j, k, 338:342])#lead
        m_background[j][k] += np.sum(d2[j, k, 290:294])+np.sum(d2[j, k, 298:302])#+np.sum(d2[j, k, 338:342])#d2[j, k, i]
        #m_signal[j][k] += np.sum(d1[j, k, 266:270])+np.sum(d1[j, k, 274:278])+np.sum(d1[j, k, 310:314])#gold
        #m_background[j][k] += np.sum(d2[j, k, 266:270])+np.sum(d2[j, k, 274:278])+np.sum(d2[j, k, 310:314])
        #m_signal[j][k] += np.sum(d1[j, k, 350:354])#scatter
        #m_background[j][k] += np.sum(d2[j, k, 350:354])
        #m_signal[j][k] += np.sum(d1[j, k, 300:304])
        #m_background[j][k] += np.sum(d2[j, k, 300:304])

#projection histograms
histoY_signal = np.zeros( 80 );
histoX_signal = np.zeros( 80 );
histoY_background = np.zeros( 80 );
histoX_background = np.zeros( 80 );

for i in range(0, 80):
    histoX_signal[i] = np.sum(m_signal[:,i])
    histoY_signal[i] = np.sum(m_signal[i,:])
    #printing out signal bins with high content
    if histoX_signal[i]>6000:
        print(i)
    if histoY_signal[i]>6000:
        print(i)
    histoX_background[i] = np.sum(m_background[:,i])*scaleMe
    histoY_background[i] = np.sum(m_background[i,:])*scaleMe

#plotting projection histograms signal/background
plt.plot(np.linspace(0, 80, 80), histoY_signal, 'b');
plt.plot(np.linspace(0, 80, 80), histoY_background, 'r');
plt.savefig(r"C:\XFCT\Alex\Output\histoY.png")
plt.close()
plt.plot(np.linspace(0, 80, 80), histoX_signal, 'b');
plt.plot(np.linspace(0, 80, 80), histoX_background, 'r');
plt.savefig(r"C:\XFCT\Alex\Output\histoX.png")
plt.close()

#finding and printing peak energies in signal histogram
for i in range(0, nBins1/2-4):
    if histo1[i]<histo1[i+1] and histo1[i+1]<histo1[i+2] and histo1[i+2]>histo1[i+3] and histo1[i+3]>histo1[i+4]:
        print (i/4.)

#calculating 2d difference
m_diff = m_signal-(m_background*scaleMe)

#plotting 2d difference map
fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(m_diff, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\LeadSliceDifference.png")

#2x2 summed difference (reduces image size by 2 in both axes)
small = cv2.resize(m_diff, (0,0), fx=0.5, fy=0.5) 
fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(small, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\LeadSliceDifferenceSmall.png")

#raw signal map
fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(m_signal, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\LeadSliceSignal.png")

#raw background map
fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
im = ax.imshow(m_background, cmap='bone')
fig.colorbar(im, cax=cax, orientation='vertical')
plt.savefig(r"C:\XFCT\Alex\Output\LeadSliceBackground.png")
