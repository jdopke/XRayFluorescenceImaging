import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import cv2

from read_function import read_file

plt.xkcd()

#reading known good files(test3)
#d1, nBins1 = read_file(r"C:\XFCT\Data\Medium_Eps_33mmPHtoSample_400umPH_105keV_60s_Pb_Flight_Tube_W_Exit_Tube_170713_113737.hxt")
#d2, nBins2 = read_file(r"C:\Users\ajm18458\Documents\data\TEST3BACKGROUND.hxt")
#d3, nBins3 = read_file(r"C:\Users\ajm18458\Documents\data\TEST3GOLD.hxt")
d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Scroll_105keV_400um_PH_35mm_170mm_8hrs_170714_002257.hxt")

#if nBins1!=nBins2:
#    print("error")

#inside square - [40:65,20:45]
events_to_scale_signal = np.sum(d1)#[40:65,20:45,:])
#events_to_scale_background = np.sum(d2)#[40:65,20:45,:])
#events_to_scale_gold = np.sum(d3)#[40:65,20:45,:])

#spectral histograms for signal/background/gold
histo1 = np.zeros( nBins1 );
#histo2 = np.zeros( nBins2/2 );
#histo3 = np.zeros( nBins3/2 );

for i in range(0, nBins1):
    histo1[i] = np.sum(d1[40:44,45:49,i])#/events_to_scale_signal*10.24
#    histo2[i] = np.sum(d2[40:65,20:45,i])/events_to_scale_background*10.24
#    histo3[i] = np.sum(d3[40:65,20:45,i])/events_to_scale_gold*10.24

#plotting spectra with legend
plt.plot(np.linspace(0, nBins1/4, nBins1), histo1, 'r', label = "Papyrus");
#plt.plot(np.linspace(0, nBins2/8, nBins2/2), histo2, 'b', label = "Background");
#plt.plot(np.linspace(0, nBins3/8, nBins3/2), histo3, 'g', label = "Gold");
plt.xlabel("Energy[keV]")
plt.ylabel("Relative intensity")
plt.legend(loc = "upper right")
plt.title("Inside sensitive area")
    
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum_on_square.png")
plt.close()


start_bin = 60
end_bin = 87


x_fit = np.concatenate((np.linspace(start_bin, 72, (72-start_bin)*4), np.linspace(77, end_bin, (end_bin-77)*4)))
#selecting histogram data without lead k alpha
y_fit = np.concatenate((histo1[start_bin*4:288], histo1[308:end_bin*4]))
#fitting 3rd order polynomial
fit = np.polyfit(x_fit,y_fit,3)
#x range from start bin to end bin
x_eval = np.linspace(start_bin,end_bin,(end_bin-start_bin)*4)
#filling y values with fitted polynomial
y_eval = np.polynomial.polynomial.polyval(x_eval,np.flip(fit,0))
#subtracting polynomial from spectrum
#y_diff = histo1[start_bin*4:end_bin*4]-y_eval
plt.xkcd()

#plotting background subtracted spectrum
plt.plot(x_eval, histo1[start_bin*4:end_bin*4], "b", label = "Spectrum")
plt.plot(x_eval, y_eval, "r", label = "3rd order Poly")
fit = np.polyfit(x_fit,y_fit,1)
y_eval = np.polynomial.polynomial.polyval(x_eval,np.flip(fit,0))
plt.plot(x_eval, y_eval, "g", label = "Linear")

plt.xlabel("Energy[keV]")
#removing extension from input file name and saving figure
plt.legend(loc = "upper left")
plt.title("Inside sensitive area")
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum_diff.png")
plt.close()

plt.plot(np.linspace(70, 90, 80), histo1[280:360], 'r', label = "Papyrus");
#plt.plot(np.linspace(0, nBins2/8, nBins2/2), histo2, 'b', label = "Background");
#plt.plot(np.linspace(0, nBins3/8, nBins3/2), histo3, 'g', label = "Gold");
plt.xlabel("Energy[keV]")
plt.ylabel("Relative intensity")
plt.legend(loc = "upper right")
plt.title("Inside sensitive area")
    
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum_on_square_zoom.png")
plt.close()



#outside square - [5:30,5:30]
events_to_scale_signal1 = np.sum(d1)#[40:65,20:45,:])
#events_to_scale_background1 = np.sum(d2)#[40:65,20:45,:])
#events_to_scale_gold1 = np.sum(d3)#[40:65,20:45,:])

histo1_outside = np.zeros( nBins1/2 );
#histo2_outside = np.zeros( nBins2/2 );
#histo3_outside = np.zeros( nBins3/2 );

for i in range(0, nBins1/2):
    histo1_outside[i] = np.sum(d1[40:50,45:55,i])/events_to_scale_signal1*10.24
#    histo2_outside[i] = np.sum(d2[5:30,5:30,i])/events_to_scale_background1*10.24
#    histo3_outside[i] = np.sum(d3[5:30,5:30,i])/events_to_scale_gold1*10.24

plt.plot(np.linspace(0, nBins1/8, nBins1/2), histo1_outside, 'r', label = "Papyrus");
#plt.plot(np.linspace(0, nBins2/8, nBins2/2), histo2_outside, 'b', label = "Background");
#plt.plot(np.linspace(0, nBins3/8, nBins3/2), histo3_outside, 'g', label = "Gold");
plt.xlabel("Energy[keV]")
plt.ylabel("Relative intensity")
plt.legend(loc = "upper right")
plt.title("Outside sensitive area")

plt.savefig(r"C:\XFCT\Alex\Output\Spectrum_outside_square.png")
plt.close()

x_fit = np.concatenate((np.linspace(start_bin, 72, (72-start_bin)*4), np.linspace(77, end_bin, (end_bin-77)*4)))
#selecting histogram data without lead k alpha
y_fit = np.concatenate((histo1_outside[start_bin*4:288], histo1_outside[308:end_bin*4]))
#fitting 3rd order polynomial
fit = np.polyfit(x_fit,y_fit,3)
#x range from start bin to end bin
x_eval = np.linspace(start_bin,end_bin,(end_bin-start_bin)*4)
#filling y values with fitted polynomial
y_eval = np.polynomial.polynomial.polyval(x_eval,np.flip(fit,0))
#subtracting polynomial from spectrum
#y_diff = histo1[start_bin*4:end_bin*4]-y_eval

#plotting background subtracted spectrum
plt.plot(x_eval, histo1_outside[start_bin*4:end_bin*4], "b", label = "Spectrum")
plt.plot(x_eval, y_eval, "b", label = "3rd order Poly")
plt.xlabel("Energy[keV]")
#removing extension from input file name and saving figure
plt.legend(loc = "upper left")
plt.title("Outside sensitive area")
plt.savefig(r"C:\XFCT\Alex\Output\Spectrum_diff_background.png")
plt.close()
