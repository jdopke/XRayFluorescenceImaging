import matplotlib as plt
import numpy as np

hc = 12.4
compton = 0.0243

def energy_f(energy, angle):
    return hc/((hc/energy+((1-np.cos(angle))*compton)))

input_energy = 88.5

for i in range(0, 180, 5):
    print (energy_f(input_energy, np.deg2rad(i)), i)
