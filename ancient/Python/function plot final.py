import matplotlib.pyplot as plt
import numpy as np

#plotting angular efficiency relative to maximum through pinhole
# pinhole radius 0.5 mm, pinhole depth 2 mm
x = np.linspace(-26.56,26.56,5312)
y = np.arccos(np.absolute(np.tan(np.deg2rad(x))/0.5))
plt.plot(x, np.absolute(y-np.cos(y)*np.sin(y))/np.pi)

plt.savefig(r"C:\Users\ajm18458\Documents\Output\efficiency.png")
plt.close()
