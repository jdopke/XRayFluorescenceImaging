import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2

from scipy.optimize import curve_fit
from scipy import asarray as ar,exp


from read_function import read_file
#d1, nBins1 = read_file(r"C:\XFCT\Data\Medium_Eps_33mmPHtoSample_400umPH_105keV_60s_Pb_Flight_Tube_W_Exit_Tube_170713_113737.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Scroll_105keV_400um_PH_35mm_170mm_8hrs_170714_002257.hxt")
d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\PSF_10um_Lead_Foil_105keV_400um_PH_35mm_170mm_600s_170714_142747.hxt")

divider = 1

m1 = np.zeros( (80/divider+1,80/divider+1) );
m2 = np.zeros( (80/divider+1,80/divider+1) );

for j in range(divider, 80, divider):
    for k in range (divider, 80, divider):
        m1[j/divider][k/divider] = np.sum(d1[j:j+divider, k:k+divider, 290:294])+np.sum(d1[j:j+divider, k:k+divider, 298:302])#+np.sum(d1[j:j+divider, k:k+divider, 338:354])#lead
        m2[j/divider][k/divider] = np.sum(d1[j:j+divider, k:k+divider, 40:44])+np.sum(d1[j:j+divider, k:k+divider, 46:50])#+np.sum(d1[j:j+divider, k:k+divider, 338:354])#lead L-lines
        #m1[j][k] += np.sum(d1[j, k, 266:270])+np.sum(d1[j, k, 274:278])+np.sum(d1[j, k, 310:314])#gold
        #m1[j][k] += np.sum(d1[j, k, 350:354])#scatter

#to plot figure with colourbar

histo1 = np.zeros( 20 );

gradient = 27./34.


'''
Stuff simon added within dashes. Clips 
'''
subimage=m1[20:30,0:10]
mean=np.mean(subimage)
m1=m1.clip(max=mean)
#end of stuff simon added
'''
'''
for i in range(20, 54):
    offset_f = gradient*(i-20)
    offset_i = np.int(offset_f) # offset for index
    offset_r = offset_f-offset_i;
    histo1 += ((1-offset_r)*m1[i, 6+offset_i:26+offset_i] + (offset_r)*m1[i, 7+offset_i:27+offset_i])

histo2 = np.zeros( 19 );
for i in range(0, 19):
    histo2[i] = histo1[i] - histo1[i+1]

x_fit = np.linspace(0, 19, 19)
n = len(x_fit)                          #the number of data
mean = np.sum(x_fit*histo2)/np.sum(histo2)                   #note this correction
sigma = sum(histo2*(x_fit-mean)**2)/np.sum(histo2)        #note this correction

print (mean, "   ", sigma, "   ", n)

def gaus(x,a,x0,sigma):
    return a*exp(-(x-x0)**2/(2*sigma**2))

popt,pcov = curve_fit(gaus,x_fit,histo2,p0=[1,mean,sigma])
print(popt)
gaussian = gaus(x_fit, popt[0], popt[1], popt[2])

#fit = np.gaussfit(x_fit,histo2)

plt.plot(np.linspace(0, 20, 20), histo1, 'r', label = "top");
plt.plot(np.linspace(0, 19, 19), histo2, 'b', label = "diff");
plt.plot(np.linspace(0, 19, 19), gaussian, 'y', label = "fit");
#plt.plot(np.linspace(0, nBins3/8, nBins3/2), histo3, 'g', label = "Gold");
plt.xlabel("Channel")
plt.ylabel("Relative intensity")
plt.legend(loc = "upper right")
plt.title("Inside sensitive area")
    
plt.savefig(r"C:\XFCT\Alex\Output\EdgeHisto.png")
plt.close()

print (np.sum(m1))
print (np.sum(m2))

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])

im = ax.imshow(m1, cmap='bone')

fig.colorbar(im, cax=cax, orientation='vertical')

#to add contents to figure, see block comment below
"""
fig2 = plt.figure()
ax2 = fig2.add_subplot(111, aspect='equal')
im = ax2.imshow(m1)
ax2.add_patch(
    patches.Rectangle(
        (20, 40),
        25,
        25,
        fill=False, # remove background
        edgecolor="red"
    )
)

ax2.add_patch(
    patches.Rectangle(
        (5, 5),
        25,
        25,
        fill=False, # remove background
        edgecolor="green"
    )
)
"""
#simple plot of matrix data
#plt.matshow(m1)
plt.savefig(r"C:\XFCT\Alex\Output\signal_fragment.png")  
plt.close()

raise SystemExit

