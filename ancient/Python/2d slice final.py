import numpy as np
import struct
import os
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import cv2

from scipy.optimize import curve_fit
from scipy import asarray as ar,exp

from read_function import read_file

#creates a circular point spread function of diameter d
def defocus_kernel(d, sz=30):
    kern = np.zeros((sz, sz), np.uint8)
    cv2.circle(kern, (sz, sz), d, 255, -1, cv2.LINE_AA, shift=1)
    kern = np.float32(kern) / 255.0
    return kern



#d1, nBins1 = read_file(r"C:\XFCT\Data\Medium_Eps_33mmPHtoSample_400umPH_105keV_60s_Pb_Flight_Tube_W_Exit_Tube_170713_113737.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Scroll_105keV_400um_PH_35mm_170mm_8hrs_170714_002257.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Lead_Foil_PSF_200um_PH_105keV_600s_170715_210115.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Gold_Grid_200um_PH_105keV_600s_170715_212948.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Fragment_200um_PH_105keV_1200s_170715_214648.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Fragment_200um_PH_105keV_12hours_170715_221314.hxt")
#d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\PSF_10um_Lead_Foil_105keV_400um_PH_35mm_170mm_600s_170714_142747.hxt")
d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\ScrollFragment_90deg_200umPH_35mm170mm_105keVMono_10hrs_170716_210507.hxt")



divider = 1

m1 = np.zeros( (80/divider+1,80/divider+1) );
m2 = np.zeros( (80/divider+1,80/divider+1) );
m3 = np.zeros( (80/divider+1,80/divider+1) );

start_bin = 60
end_bin = 87

for j in range(divider, 80, divider):
    for k in range (divider, 80, divider):
        histo1 = np.zeros( d1.shape[2] )
        for i in range(0, d1.shape[2]):
            histo1[i] = 0.001 + np.sum(d1[j:j+divider, k:k+divider,i])#/events_to_scale_signal*10.24
        x_fit = np.concatenate((np.linspace(start_bin, 72, (72-start_bin)*4), np.linspace(77, end_bin, (end_bin-77)*4)))
        y_fit = np.log(np.concatenate((histo1[start_bin*4:288], histo1[308:end_bin*4])))
        fit = np.polyfit(x_fit,y_fit,3)
        x_eval = np.linspace(72,77, 20)
        y_eval = np.exp(np.polynomial.polynomial.polyval(x_eval,np.flip(fit,0)))

        m1[j/divider][k/divider] = np.sum(histo1[288:296])+np.sum(histo1[300:308])#+np.sum(histo1[338:342])#+np.sum(histo1[338:342])#-y_eval)#+np.sum(d1[j:j+divider, k:k+divider, 338:354])#lead
        m2[j/divider][k/divider] = (2*np.sum(d1[j:j+divider, k:k+divider, 276:288])-np.sum(d1[j:j+divider, k:k+divider, 264:276]))
        m3[j/divider][k/divider] = (2*np.sum(d1[j:j+divider, k:k+divider, 304:312])-np.sum(d1[j:j+divider, k:k+divider, 312:320]))
        m1[j/divider][k/divider] -= (m2[j/divider][k/divider]+m3[j/divider][k/divider])*3/5
        #if (m1[j/divider][k/divider] < 0):
        #    m1[j/divider][k/divider] = 0
        
        #m1[j][k] += np.sum(d1[j, k, 266: 270])+np.sum(d1[j, k, 274:278])+np.sum(d1[j, k, 310:314])#gold
        #m1[j][k] += np.sum(d1[j, k, 350:354])#scatter

#to plot figure with colourbar

print (np.sum(m1))
print (np.sum(m2))

background_h = (np.sum(m1[:,70:80], axis=1)+1)/10.
for i in range(0, m1.shape[0]):
    m1[:,i] = np.divide(m1[:,i],background_h)

background_i = (np.sum(m1[:,70:80], axis=1)+1)/10.
print background_h
print background_i

m1 = np.rot90(m1)
m1 = np.flipud(m1)
fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])

im = ax.imshow(m1[1:79,1:79], cmap='bone_r')

fig.colorbar(im, cax=cax, orientation='vertical')

#to add contents to figure, see block comment below
"""
fig2 = plt.figure()
ax2 = fig2.add_subplot(111, aspect='equal')
im = ax2.imshow(m1)
ax2.add_patch(
    patches.Rectangle(
        (20, 40),
        25,
        25,
        fill=False, # remove background
        edgecolor="red"
    )
)

ax2.add_patch(
    patches.Rectangle(
        (5, 5),
        25,
        25,
        fill=False, # remove background
        edgecolor="green"
    )
)
"""
#simple plot of matrix data
#plt.matshow(m1)
plt.savefig(r"C:\XFCT\Alex\Output\signal_fragment.png")  
plt.close()

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])

im = ax.imshow(m2, cmap='bone')

fig.colorbar(im, cax=cax, orientation='vertical')

plt.savefig(r"C:\XFCT\Alex\Output\background_low_fragment.png")  
plt.close()

fig, ax = plt.subplots()
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])

im = ax.imshow(m3, cmap='bone')

fig.colorbar(im, cax=cax, orientation='vertical')

plt.savefig(r"C:\XFCT\Alex\Output\background_high_fragment.png")  
plt.close()

raise SystemExit


#storing name for deconvolution window
win = 'deconvolution'

#generating cv2 compatible image from input
img = (np.float32(m1)-np.float32(np.amin(m1)))/np.float32(np.amax(m1)-np.amin(m1))
#img[70:80,0:80] = 0.
height, width = img.shape[:2]
#showing input data
cv2.namedWindow('input', 0)
cv2.startWindowThread()
cv2.resizeWindow('input', 600,600)
cv2.imshow('input', img)
cv2.namedWindow('psf', 0)
cv2.resizeWindow('psf', 600,600)

IMG = cv2.dft(img, flags=cv2.DFT_COMPLEX_OUTPUT)


def update(_):
    #getting updating parameters from trackbars
    d = (cv2.getTrackbarPos('d', win))
    noise = 10**(-0.1*cv2.getTrackbarPos('SNR (db)', win))

    psf = defocus_kernel(d)
    cv2.imshow('psf', psf)

    psf /= psf.sum()
    psf_pad = np.zeros_like(img)
    kh, kw = psf.shape
    psf_pad[:kh, :kw] = psf
    PSF = cv2.dft(psf_pad, flags=cv2.DFT_COMPLEX_OUTPUT, nonzeroRows = kh)
    PSF2 = (PSF**2).sum(-1)
    iPSF = PSF / (PSF2 + noise)[...,np.newaxis]
    RES = cv2.mulSpectrums(IMG, iPSF, 0)
    res = cv2.idft(RES, flags=cv2.DFT_SCALE | cv2.DFT_REAL_OUTPUT )
    res = np.roll(res, -kh//2, 0)
    res = np.roll(res, -kw//2, 1)
    res = np.float32(res)/np.float32(np.amax(res))
    cv2.imshow(win, res)
#generating output windows with trackbars
cv2.namedWindow(win, cv2.WINDOW_NORMAL)
cv2.resizeWindow(win, 600,600)
cv2.createTrackbar('d', win, int(6), 25, update)
cv2.createTrackbar('SNR (db)', win, int(1), 50, update)
update(None)

#required to keep the program running
while True:
    ch = cv2.waitKey(0) & 0xFF
    if ch == 32 or ch == 27:
        break

cv2.destroyAllWindows()
raise SystemExit
