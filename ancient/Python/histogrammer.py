# Source: http://central.scipy.org/item/22/5/building-a-simple-interactive-2d-data-viewer-with-matplotlib

# License: Creative Commons Zero (almost public domain) http://scpyce.org/cc0

"""
Example usage of matplotlibs widgets: Build a small 2d Data viewer.

Shows 2d-data as a pcolormesh, a click on the image shows the crossection
(x or y, depending on the mouse button) and draws a corresponding line in
the image, showing the location of the crossections. A reset button deletes all
crossections plots.

Works with matplotlib 1.0.1.
"""

from matplotlib.widgets import Cursor, Button
import matplotlib.pyplot as plt
import numpy as np

from read_function import read_file



plt.rcParams['font.size']=8
plt.xkcd()

class viewer_2d(object):
    def __init__(self,d1,x=None, y=None):
        """
        Shows a given array in a 2d-viewer.
        Input: z, an 2d array.
        x,y coordinters are optional.
        """
        divider = 1
        self.data = d1
        self.m1 = np.zeros( (80/divider,80/divider) );
        self.m2 = np.zeros( (80/divider,80/divider) );
        self.m3 = np.zeros( (80/divider,80/divider) );
        
        if x==None:
            self.x=np.arange(self.m1.shape[0])
        else:
            self.x=x
        if y==None:
            self.y=np.arange(self.m1.shape[1])
        else:
            self.y=y

        for j in range(divider, 80, divider):
            for k in range (divider, 80, divider):
                self.m1[j/divider][k/divider] = np.sum(d1[j:j+divider, k:k+divider, 288:298])+np.sum(d1[j:j+divider, k:k+divider, 298:304])#+np.sum(d1[j:j+divider, k:k+divider, 338:354])#lead
                self.m2[j/divider][k/divider] = (2*np.sum(d1[j:j+divider, k:k+divider, 276:288])-np.sum(d1[j:j+divider, k:k+divider, 264:276]))
                self.m3[j/divider][k/divider] = (2*np.sum(d1[j:j+divider, k:k+divider, 304:312])-np.sum(d1[j:j+divider, k:k+divider, 312:320]))
                self.m1[j/divider][k/divider] -= (self.m2[j/divider][k/divider]+self.m3[j/divider][k/divider])*3/5
                #if (m1[j/divider][k/divider] < 0):
                #    m1[j/divider][k/divider] = 0
        
                #m1[j][k] += np.sum(d1[j, k, 266: 270])+np.sum(d1[j, k, 274:278])+np.sum(d1[j, k, 310:314])#gold
                #m1[j][k] += np.sum(d1[j, k, 350:354])#scatter

        self.z=self.m1
        self.fig=plt.figure()
        #Doing some layout with subplots:
        self.fig.subplots_adjust(0.05,0.05,0.98,0.98,0.1)
        self.overview=plt.subplot2grid((8,10),(0,0),rowspan=7,colspan=3)
        self.overview.pcolormesh(self.x,self.y,self.z)
        self.overview.autoscale(1,'both',1)
        self.x_subplot=plt.subplot2grid((8,10),(0,3),rowspan=4,colspan=7)
        self.y_subplot=plt.subplot2grid((8,10),(4,3),rowspan=4,colspan=7)
        
        self.old_xpos = self.m1.shape[0]/2
        self.old_ypos = self.m1.shape[1]/2
        #Adding widgets, to not be gc'ed, they are put in a list:
        
        cursor=Cursor(self.overview, useblit=True, color='black', linewidth=2 )
        but_ax=plt.subplot2grid((8,10),(7,0),colspan=1)
        reset_button=Button(but_ax,'Reset')
        but_ax2=plt.subplot2grid((8,10),(7,1),colspan=1)
        legend_button=Button(but_ax2,'Legend')
        self._widgets=[cursor,reset_button,legend_button]
        #connect events
        reset_button.on_clicked(self.clear_xy_subplots)
        legend_button.on_clicked(self.show_legend)
        self.fig.canvas.mpl_connect('button_press_event',self.click)
    
    def show_legend(self, event):        
        """Shows legend for the plots"""
        for pl in [self.x_subplot,self.y_subplot]:
            if len(pl.lines)>0:
                pl.legend()
        plt.draw()

    def clear_xy_subplots(self,event):
        """Clears the subplots."""
        for j in [self.overview,self.x_subplot,self.y_subplot]:
            j.lines=[]
            j.legend_ = None
        plt.draw()


    def click(self,event):
        """
        What to do, if a click on the figure happens:
            1. Check which axis
            2. Get data coord's.
            3. Plot resulting data.
            4. Update Figure
        """
        if event.inaxes==self.overview:
            #Get nearest data
            xpos=np.argmin(np.abs(event.xdata-self.x))
            ypos=np.argmin(np.abs(event.ydata-self.y))
            
            #Check which mouse button:
            if event.button==1:
                #Plot it
                histo1 = np.zeros( self.data.shape[2] )
                miny = max(0,xpos-2)
                maxy = min(80,xpos+2)
                minx = max(0,ypos-2)
                maxx = min(80,ypos+2)
                for i in range(0, self.data.shape[2]):
                    histo1[i] = np.sum(d1[minx:maxx,miny:maxy,i])#/events_to_scale_signal*10.24

                #c,=self.y_subplot.plot(np.linspace(0, self.data.shape[2]/4, self.data.shape[2]), histo1,label=str(self.x[xpos]))
                c,=self.y_subplot.semilogy(np.linspace(60, self.data.shape[2]/8, (self.data.shape[2]/2-60*4)), histo1[60*4:self.data.shape[2]/2],label=str(self.x[xpos]))
                
                start_bin = 60
                end_bin = 87
                x_fit = np.concatenate((np.linspace(start_bin, 72, (72-start_bin)*4), np.linspace(77, end_bin, (end_bin-77)*4)))
                y_fit = np.log(np.concatenate((histo1[start_bin*4:288], histo1[308:end_bin*4])))
                fit = np.polyfit(x_fit,y_fit,3)
                x_eval = np.linspace(start_bin,end_bin,(end_bin-start_bin)*4)
                y_eval = np.exp(np.polynomial.polynomial.polyval(x_eval,np.flip(fit,0)))
                c,=self.y_subplot.semilogy(x_eval, y_eval)

                self.overview.axvline(self.x[xpos],color=c.get_color(),lw=2)
                self.overview.axhline(self.y[ypos],color=c.get_color(),lw=2)

            elif event.button==3:
                #Plot it                
                histo1 = np.zeros( self.data.shape[2] );
                for i in range(0, self.data.shape[2]):
                    histo1[i] = np.sum(d1[min(self.old_xpos,xpos):max(self.old_xpos,xpos),min(self.old_ypos,ypos):max(self.old_ypos,ypos),i])#/events_to_scale_signal*10.24

                #c,=self.x_subplot.plot(np.linspace(0, self.data.shape[2]/4, self.data.shape[2]), histo1,label=str(self.y[ypos]))
                c,=self.x_subplot.semilogy(np.linspace(60, self.data.shape[2]/8, (self.data.shape[2]/2-60*4)), histo1[60*4:self.data.shape[2]/2],label=str(self.y[ypos]))
                self.overview.axvline(self.x[xpos],color=c.get_color(),lw=2)
                self.overview.axhline(self.y[ypos],color=c.get_color(),lw=2)
            self.old_xpos = xpos
            self.old_ypos = ypos

##        if event.inaxes==self.y_subplot:
##            ypos=np.argmin(np.abs(event.xdata-self.y))
##            c=self.x_subplot.plot(self.x, self.z[ypos,:],label=str(self.y[ypos]))
##            self.overview.axhline(self.y[ypos],color=c.get_color(),lw=2)
##
##        if event.inaxes==self.x_subplot:
##            xpos=np.argmin(np.abs(event.xdata-self.x))
##            c,=self.y_subplot.plot(self.y, self.z[:,xpos],label=str(self.x[xpos]))
##            self.overview.axvline(self.x[xpos],color=c.get_color(),lw=2)
        #Show it
        plt.draw()
        
if __name__=='__main__':
    #Build some strange looking data:
    x=np.linspace(0,80,80)
    y=np.linspace(0,80,80)
    #d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Scroll_105keV_400um_PH_35mm_170mm_8hrs_170714_002257.hxt")
    #d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\PSF_10um_Lead_Foil_105keV_400um_PH_35mm_170mm_600s_170714_142747.hxt")
    #d1, nBins1 = read_file(r"C:\XFCT\Herculaneum2017\Fragment_200um_PH_105keV_1200s_170715_214648.hxt")
    #d1, nBins1 = read_file(r"/Volumes/My Passport/XF-CT/Data/Day 3/Calibration Slide - Squares/Alpha_Square_105keV_400um_PH_35mm_170mm_7_2s_170714_101833.hxt")
    d1, nBins1 = read_file(r"/Volumes/My Passport/XF-CT/Data/Day 1/Test/Gold_Grid_Test_170712_120555.hxt")

    #Put it in the viewer
    #fig_v=viewer_2d(z,x,y)
    fig_v2=viewer_2d(d1)
    #Show it
    plt.show()

raise SystemExit

