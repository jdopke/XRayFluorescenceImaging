This is a C++ class for interacting with HXT files. To use, either build and 
install this library using CMake, or just copy the cpp's and hpp's from 
to your own project. The latter is probably the easiest method. Refer to the 
example programs for basic usage.

This project will continue to be maintained on GitHub at: 
https://github.com/viscenter/hxtio. If you run into any trouble, check the 
GitHub repo for updates. I already know of an intermittent bug in the header 
parsing code that I've yet to track down, for instance. That will hopefully 
be fixed very soon.

- Seth Parker
