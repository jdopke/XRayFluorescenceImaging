#!/bin/bash

./test_configReader ../config/500V_28C_Grads.txt Gradients
./test_configReader ../config/500V_28C_Inters.txt
./test_binReader || echo "Ran test_binReader and got expected failure"
./test_binReader ../test/DummyBinReader.json > /dev/null
./test_sequentialReader || echo "Ran test_sequentialReader and got expected failure"
./test_sequentialReader ../test/DummyBinReader.json
./test_histoReader || echo "Ran test_histoReader without parameters and got expected failure"
./test_histoReader -f || echo "Ran test_histoReader without file parameter and got expected failure"
./test_histoReader ../test/Dummy.hxt -a -q -p -f > /dev/null
./test_histoReader -x40 -y40 ../test/Dummy.hxt -r 2 -s
./test_histoReader -x 40 -y 40 ../test/Dummy.hxt -q -s
./test_histoReader -a ../test/Dummy.hxt -i -r2 -b1 -e65536 > /dev/null
./test_histoReader -a ../test/Dummy.hxt -i -b 65536 -e 1 -o ../test/Dummy.svg > /dev/null
./analysis  || echo "Ran analysis without parameters and got expected failure" 
./analysis -r1
./analysis ../test/Dummy.hxt -r 2 -b1 -e65536 -s0 -i -q -o ../test/DummyAnalysis.svg > /dev/null
./analysis ../test/Dummy.hxt -b 1 -e 65536 -s 1 -o ../test/DummyAnalysis.svg > /dev/null
./getMap || echo "Ran getMap without parameters and got expected failure"
./getMap ../test/Dummy.hxt -r 2 -i -b 1 -e 10 -o ../test/DummyMap.svg
./getMap -r2 -i -b1 -e10 -o ../test/DummyMap.svg
./extractSinglePixelHisto || echo "Ran extractSinglePixelHisto without parameters and got expected failure"
./extractSinglePixelHisto ../test/Dummy.hxt -x 2 -y 2 -r 2
./extractSinglePixelHisto -x2 -y2 -r2
./test_compression ../test/DummyBinReader.json ../test/DummyOutput.json || echo "Compiled without Snappy?"

gcovr -r ../ -e .+json.+ --object-directory=./
gcovr --xml-pretty --exclude-unreachable-branches --print-summary -e .+json.+ -o coverage.xml --root ../
