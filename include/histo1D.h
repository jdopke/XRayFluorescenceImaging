#ifndef HISTO1D_H
#define HISTO1D_H
/*
 *  Histo1D.h
 *
 *  Created by Jens Dopke on 05/08/2013.
 *
 */

#include <stdlib.h>
#include <iostream>

using namespace std;

class Histo1D{
public:
	// Constructors
	Histo1D(string title = " ", int nBins = 10, double min=-1.0, double max=1.0) : title(title), nBins(nBins), min(min), max(max), binWidth((double) (max-min)/((double) nBins)) {
		histoData = new double [nBins + 2];
		histoDataCount = new int [nBins + 2];
		this->clear();
	}
    // Copy Constructors
	Histo1D(const Histo1D& toBeCopied) : title(toBeCopied.title), nBins(toBeCopied.nBins), min(toBeCopied.min), max(toBeCopied.max), binWidth(toBeCopied.binWidth) { }
	// Default Destructor
	~Histo1D() {
		delete histoData ;
        delete histoDataCount ;
	}
	
	void fill(double newData) {
		int sort;
		if (newData < min) sort = 0;
		else if (newData >= max) sort = nBins + 1;
		else sort = ((int) ((newData - min)/ binWidth) + 1);
		if ((sort >=0) && (sort < nBins +2)) {
            histoData[sort] += 1.0;
            histoDataCount[sort]++;
        }
	}
    
	void fill(double newData, double value) {
		int sort;
        std::cout << "THIS SHOULD NOT APPEAR!!!" << std::endl;
		if (newData < min) sort = 0;
		else if (newData >= max) sort = nBins + 1;
		else sort = ((int) ((newData - min)/ binWidth) + 1);
		if ((sort >=0) && (sort < nBins +2)) {
            histoData[sort] += value;
            histoDataCount[sort]++;
        }
	}
    
	void clear() {
		for(int i=0; i < (nBins + 2); i++) {
            histoData[i] = 0.;
            histoDataCount[i] = 0;
        }
	}
	
	void HistoTable (ostream& os = std::cout) {
        os << "\t" << title;
        for(int Bin=0; Bin < (nBins + 2); Bin++) {
            os << "\t" << histoData[Bin];
        }
	}
    
	void HistoTableFull (std::string filename="") {
        if (filename.empty()) {
            filename = title + ".csv";
        }
        std::ofstream tempStream (filename.c_str(), std::ofstream::out);
        tempStream << "Range\tCounts" << std::endl;
        for(int Bin=0; Bin < (nBins + 2); Bin++) {
            tempStream << (min + (((float) Bin-0.5)*binWidth)) << "\t" << histoData[Bin] << std::endl;
        }
        tempStream.close();
	}
	
	void HistoTableNoBorder(std::string filename="") {
        if (filename.empty()) {
            filename = title + ".csv";
        }
        std::ofstream tempStream (filename.c_str(), std::ofstream::out);
        tempStream << "\t" << title;
        for(int Bin=1; Bin < (nBins + 1); Bin++) {
            tempStream << "\t" << histoData[Bin];
        }
        tempStream.close();
	}
    
    int getNBins() {return nBins;}
    int getNBinsTotal() {return nBins+2;}
	
    double *  getXVals() {
        double *xVals = (double *) malloc(sizeof(double) * nBins);
        for (int i=1; i<nBins+1; i++) xVals[i-1] = min + (((float) i) - 0.5) * binWidth;
        return xVals;
    }
    
    double * getYVals() {
        double *yVals = (double *) malloc(sizeof(double) * nBins);
        for (int i=1; i<nBins+1; i++) yVals[i-1] = (double) histoData[i];
        return yVals;
    }
    
	double getBinContent(int Bin) {
		return histoData[Bin];
	}
	
	double getBinMean(int Bin) {
		return (histoData[Bin] / ((double) histoDataCount[Bin]));
	}
	
	int getBinAddress(double location) {
		int sort;
		if (location < min) sort = 0;
		else if (location >= max) sort = nBins + 1;
		else sort = ((int) ((location - min)/ binWidth) + 1);
		
		return sort;
	}
    
    double *getBin(int bin) {
        return &histoData[bin];
    }

    double * getDataArray() {
    	double *temp = new double [nBins];
    	for (int i=1;i<=nBins;i++) {
    		temp[i-1] = histoData[i];
    	}
    	return temp;
    }

    double * getXArray() {
    	double *temp = new double [nBins];
    	for (int i=1;i<=nBins;i++) {
    		temp[i-1] = (min + (((float) i-0.5)*binWidth));
    	}
    	return temp;
    }

    void setBinContent(int Bin, double value) {
        histoData[Bin] = value;
        histoDataCount[Bin] = value;
        return;
    }
    void setBinContent(int Bin, double value, double count) {
        histoData[Bin] = value;
        histoDataCount[Bin] = count;
        return;
    }

    void setTitle(const std::string& titleName) {
	    title = titleName;
        return;
	}

    double integrate(int start, int end) {
        double integral = 0.0;
        if ((start > nBins) || (start > end)) return 0.0;
        if (end > nBins) end = nBins;
        for (int tempBin=start; tempBin<end; tempBin++) {
            integral += histoData[tempBin];
        }
        return integral;
	}
    void plot(std::string plotFilename, bool logplot=false) {
        // Put raw histo data in tmp file
        std::string fileAlone;
        if (plotFilename.rfind("/") == std::string::npos) {
            fileAlone = plotFilename;
        } else {
            fileAlone = plotFilename.substr(plotFilename.rfind("/"), plotFilename.length()-plotFilename.rfind("/"));
        }
        std::string tmp_name = std::string("/tmp/" + fileAlone + ".dat");
        std::cout << "Plotting Hexitec array using temporary data file: " << tmp_name << std::endl;
        this->HistoTableFull(tmp_name);
        this->HistoTableNoBorder(plotFilename+".csv");
        std::string cmd = "gnuplot > " + plotFilename;


        // Open gnuplot as file and pipe commands
        FILE *gnu = popen(cmd.c_str(), "w");

        //fprintf(gnu, "set terminal postscript enhanced color \"Helvetica\" 18 eps\n");
        fprintf(gnu, "set terminal svg size 1280, 1024\n");
        fprintf(gnu, "set palette negative defined ( 0 '#D53E4F', 1 '#F46D43', 2 '#FDAE61', 3 '#FEE08B', 4 '#E6F598', 5 '#ABDDA4', 6 '#66C2A5', 7 '#3288BD')\n");
        //fprintf(gnu, "set pm3d map\n");
        fprintf(gnu, "unset key\n");
        fprintf(gnu, "set title \"Data Array\"\n");
        fprintf(gnu, "set xrange[%f:%f]\n", 0., (float) nBins);
        //fprintf(gnu, "set cbrange[0:120]\n");
        //fprintf(gnu, "splot \"/tmp/tmp_%s.dat\" matrix u (($1)*((%f-%f)/%d)):(($2)*((%f-%f)/%d)):3\n", HistogramBase::name.c_str(), xhigh, xlow, xbins, yhigh, ylow, ybins);
        if (logplot) {
            fprintf(gnu, "set logscale y\n");
        }
        fprintf(gnu, "plot \"%s\" using 1:2 with lines\n", tmp_name.c_str());
        // fprintf(gnu, "plot \"%s\" matrix u (($1)):(($2)):3 with image\n", ("/tmp/" + tmp_name + "_" + name + ".dat").c_str());
        pclose(gnu);

        if (remove(tmp_name.c_str()) != 0) {
            std::cout << "Error when removing temporary data file: " << tmp_name << std::endl;
        } else {
            std::cout << "Successfully removed temporary data file: " << tmp_name << std::endl;
        }
    }

private:
	string title;
	int nBins;
	double min, max;
	
	double binWidth;
	double *histoData;
    int *histoDataCount;
};


#endif