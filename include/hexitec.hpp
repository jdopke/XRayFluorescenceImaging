#pragma once

#ifndef HEXITEC
#define HEXITEC

// Note to self: Yanda.hpp from Seth seems a good way to deal with arrays, just doesn't do the reordering

#include <array>
#include <algorithm>
#include <vector>
#include <deque>
#include <list>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <typeinfo>
#include <typeindex>
#include <stdio.h>
#include <cmath>
#include <iomanip>

//included for clipboard/sequentialReader
#include <atomic>
#include <thread>
#include <memory>
#include <mutex>
#include <condition_variable>
#include <typeinfo>
//above is for clipboard

#include "global.hpp"
#include <nlohmann/json.hpp>

#include "histo1D.h"

// for convenience
using json = nlohmann::json;

namespace libhexitec
{

/** \mainpage X-ray fluorescence Libraries
 *
 * \section intro_sec Introduction
 *
 * These libraries are written to support decoding of data recorded with Hexitec in pursuit of an x-ray fluorescence computed tomography setup.
 * Generation of hxt-type histogram files (no header yet) is supported. Using various methods for generation, comparisons of efficiency and energy resolution can be made
 * for different approaches to handling hits in hexitec.
 *
 * \section install_sec Installation
 *
 * Download the code from https://gitlab.cern.ch/jdopke/Xrayfluorescenceimaging using your favourite method of accessing gitlab. For further information, follow the Readme file in the
 * base directory or directly head to https://xrf-imaging.web.cern.ch/
 *
 * All Software is provided as-is without any warranty for functionality/correctness or completeness, but all available effort has been put into verifying functionality.
 */

// Generates a real array index from a file based array index (i.e. takes an index in an array as stored by hexitec and transfers is into a real linear array index)
inline unsigned int indexHelper(unsigned int fileIndex) {return ((fileIndex%4)*20)+(fileIndex/4)%20+(fileIndex/80)*80;}
inline unsigned int arrayIndexHelper(unsigned int arrayIndex) {return (arrayIndex%20)*4+(arrayIndex/20);}
inline void FourWordSwap(unsigned short* targetBuffer, unsigned short* sourceBuffer) {
	// Note that targetBuffer is written in words (64Bit) 0, 5, 10 and 15, whilst sourcebuffer is used in words 0,1,2,3
	uint64_t * targetTemp, * sourceTemp;
	targetTemp = reinterpret_cast<uint64_t*>(targetBuffer);
	sourceTemp = reinterpret_cast<uint64_t*>(sourceBuffer);
	targetTemp[0] = (sourceTemp[0] & 0xfffful) | ((sourceTemp[1] & 0xfffful) << 16) | ((sourceTemp[2] & 0xfffful) << 32) | ((sourceTemp[3] & 0xfffful) << 48);
	targetTemp[5] = ((sourceTemp[0] & (0xfffful << 16)) >> 16) | ((sourceTemp[1] & (0xfffful << 16))) | ((sourceTemp[2] & (0xfffful << 16)) << 16) | ((sourceTemp[3] & (0xfffful << 16)) << 32);
	targetTemp[10] = ((sourceTemp[0] & (0xfffful << 32)) >> 32) | ((sourceTemp[1] & (0xfffful << 32)) >> 16) | ((sourceTemp[2] & (0xfffful << 32))) | ((sourceTemp[3] & (0xfffful << 32)) << 16);
	targetTemp[15] = ((sourceTemp[0] & (0xfffful << 48)) >> 48) | ((sourceTemp[1] & (0xfffful << 48)) >> 32) | ((sourceTemp[2] & (0xfffful << 48)) >> 16) | ((sourceTemp[3] & (0xfffful << 48)));
}

template <class T>
class HexitecArray
{
public:
	HexitecArray(T fillvalue = 0) {
		m_size = 80;
		memset(&data[0], fillvalue, data.size() * sizeof(T));
		m_isOrdered = false;
		m_isLoaded = true;
	}
	template<typename U>
	HexitecArray<T>& operator+=(const HexitecArray<U>& classObj);
	template<typename U>
	HexitecArray<T>& operator*=(const HexitecArray<U>& classObj);
	std::array<T, 6400> data;
	T& operator()(unsigned int x, unsigned int y) {return data[y*80+x];}
	bool reorder();
	void setOrdered(bool ordered=true) {m_isOrdered = ordered;}
	bool isOrdered() const {return m_isOrdered;}
	unsigned int size() const {return m_size;}
	bool isLoaded() const {return m_isLoaded;}

	bool isConstant() {
		T temp=data[0];
		for (auto &element : data) {
			if (element != temp) return false;
		}
		return true;
	}
	void toFile(std::string filename, bool header=true);
	void plot(std::string plotFilename, bool logplot=false);
	void dump() {
		for(auto &element: this->data) {
			std::cout << element << std::endl;
		}
	}
	T getMaxVal() {
		T max=data[0];
		for (auto &element : data) {
			if (element > max) max=element;
		}
		return max;
	}
	T getMinVal() {
		T min=data[0];
		for (auto &element : data) {
			if (element < min) min=element;
		}
		return min;
	}
	double getAverage() {
	    T sum=0;
	    for (auto &element : data) {
            sum += element;
	    }
	    return sum/data.size();
	}

	double getRms() {
	    T sum=0;
        for (auto &element : data) {
            sum += pow(element,2);
        }
        return sqrt(sum/data.size());
	}

protected:
	unsigned int m_size;
	bool m_isOrdered;
	bool m_isLoaded;
};

template <typename T>
class CalibrationMap : public HexitecArray<T>
{
public:
	CalibrationMap(std::string name, std::string filename=std::string("")) : m_filename(filename) {
		this->m_name = name;
		this->m_size = 80;
		this->m_isOrdered = false;
		this->m_isLoaded = false;
		if (this->m_filename.length()) {
			load(this->m_filename);
		}
	}
	int load(std::string filename);
	bool isLoaded() {return this->m_isLoaded;}

	//Overloaded from HexitecArray
	void toFile(std::string filename, bool header=true);
	void plot(std::string plotFilename);

private:
	std::string m_filename;
	std::string m_name;
};

class HexitecRawFrame : public HexitecArray<unsigned short>
{
public:
	HexitecRawFrame(unsigned int init=0) {
		memset(&(this->data[0]), (unsigned short) init, this->data.size() * sizeof(unsigned short));
		this->m_size = 80;
		this->m_isOrdered = false;
		this->m_isLoaded = false;
	}
	HexitecRawFrame(std::array<unsigned short, 6400> buffer, bool orderMe = false) {
		if (orderMe) {
		    for (unsigned int i=0; i<this->m_size; i++) {
			    for (unsigned int k=0; k<this->m_size/16; k++) {
			        FourWordSwap(&data[k*4+i*80], &buffer[i*80+k*16]);
			    }
		    }
		} else {
			std::copy(std::begin(buffer), std::end(buffer), std::begin(data));
		}
		this->m_size = 80;
		this->m_isOrdered = orderMe;
		this->m_isLoaded = false;
	}
	HexitecRawFrame(const HexitecRawFrame&) = default;
	HexitecRawFrame(HexitecRawFrame&&) = default;
	int load(std::vector<unsigned int> &input) {
		unsigned int index=0;
		for (auto &element : input) {
			data[index++] = element;
		}
		m_isLoaded = true;
		return 0;
	}
	int load(std::ifstream &file) {
// can buffer be directly data?
		unsigned short *buffer = new unsigned short[6400]();
		file.read((char *)buffer, 2*this->data.size());
		if (!file)
			return -1;
		unsigned int index=0;
		for (auto &element : data) {
			element = buffer[index++];
		}
		m_isLoaded = true;
		delete[] buffer;
		return 0;
	}
	unsigned short getMax() {
		unsigned short max = 0;
		for (auto &element : data) {
			if (element > max) { max = element; }
		}
	return max;
	}
	unsigned short getMin() {
		unsigned short min = 65535;
		for (auto &element : data) {
			if (element < min) { min = element; }
		}
		return min;
	}
	void getMinMax(unsigned short &min, unsigned short &max) {
		for (auto &element : data) {
			if (element < min) { min = element; }
			if (element > max) { max = element; }
		}
	}
	HexitecRawFrame& operator= (const HexitecRawFrame& temp) {
		this->data = temp.data;
		this->m_size = temp.size();
		this->m_isOrdered = temp.isOrdered();
		this->m_isLoaded = temp.isLoaded();
		return *this;
	}
};

class HexitecFrame : public HexitecArray<double>
{
public:
	HexitecFrame(double init=0.) {
		memset(&(this->data[0]), init, this->data.size() * sizeof(double));
		this->m_threshold = 0;
		this->m_size = 80;
		this->m_isOrdered = false;
		this->m_isLoaded = false;
	}
	HexitecFrame( const HexitecRawFrame& init) {
		for (unsigned int i = 0; i<6400; i++) {
			this->data[i] = init.data[i];
		}
		this->m_threshold = 0;
		this->m_size = 80;
		this->m_isOrdered = false;
		this->m_isLoaded = true;
	}
	HexitecFrame& operator= (const HexitecRawFrame& temp) {
		for (unsigned int i = 0; i<6400; i++) {
			this->data[i] = temp.data[i];
		}
		this->m_size = temp.size();
		this->m_isOrdered = temp.isOrdered();
		this->m_threshold = 0;
		this->m_isLoaded = temp.isLoaded();
		return *this;
	}
	void setThreshold(CalibrationMap<unsigned short> *thresholdMap) {m_threshold = thresholdMap;} 

private:
	CalibrationMap<unsigned short>* m_threshold;
};

class CalibrationSet{
public:
	CalibrationSet() {
		m_intersect = 0;
		m_slope = 0;
		m_threshold = 0;
	}
	CalibrationSet( const CalibrationSet & CS) {
		this->m_intersect = new CalibrationMap<double>(*(CS.m_intersect));
		this->m_slope = new CalibrationMap<double>(*(CS.m_slope));
		this->m_threshold = new CalibrationMap<unsigned short>(*(CS.m_threshold));
	}
	CalibrationSet(CalibrationMap<double> intersect, CalibrationMap<double> slope, CalibrationMap<unsigned short> threshold) {
		m_intersect = new CalibrationMap<double>(intersect);
		m_slope = new CalibrationMap<double>(slope);
		m_threshold = new CalibrationMap<unsigned short>(threshold);
	}
	CalibrationSet(std::string intersectFilename, std::string slopeFilename, std::string thresholdFilename) {
		m_intersect = new CalibrationMap<double>("Intersect", intersectFilename);
		m_slope = new CalibrationMap<double>("Slope", slopeFilename);
		m_threshold = new CalibrationMap<unsigned short>("Threshold", thresholdFilename);
	}
	~CalibrationSet() {
		if (m_intersect) delete m_intersect;
		if (m_slope) delete m_slope;
		if (m_threshold) delete m_threshold;
	}
	CalibrationSet& operator=(const CalibrationSet& other) {
		this->m_intersect = new CalibrationMap<double>(*(other.m_intersect));
		this->m_slope = new CalibrationMap<double>(*(other.m_slope));
		this->m_threshold = new CalibrationMap<unsigned short>(*(other.m_threshold));
		return *this;
	}

	void plot(std::string preText = "") {m_intersect->plot(preText + "Intersects.svg"); m_slope->plot(preText + "Gradients.svg"); m_threshold->plot(preText + "Thresholds.svg");}
	bool isInitialised() {return (m_intersect && m_slope && m_threshold);}
	bool isOrdered() {return (m_intersect->isOrdered() && m_slope->isOrdered() && m_threshold->isOrdered());}
	void order() {
		if (!m_intersect->isOrdered()) m_intersect->reorder();
		if (!m_slope->isOrdered()) m_slope->reorder();
		if (!m_threshold->isOrdered()) m_threshold->reorder();
	}
	std::shared_ptr<HexitecFrame> calibrate(std::shared_ptr<HexitecRawFrame> input) { std::shared_ptr<HexitecFrame> temp(new HexitecFrame(*input)); *temp *= (*m_slope); *temp += (*m_intersect); temp->setThreshold(m_threshold); return temp;}

protected:
	CalibrationMap<double>* m_intersect;
	CalibrationMap<double>* m_slope;
	CalibrationMap<unsigned short>* m_threshold;
};

//Class stolen from Timon Heim, YARR
template <class T>
class ClipBoard {
    public:

        ClipBoard(unsigned int size=1) : doneFlag(false), m_size(size) {}
        ~ClipBoard() {
            while(!dataQueue.empty()) {
                std::unique_ptr<T> tmp = this->popData();
            }
        }
        void pushData(std::unique_ptr<T> data) {
            queueMutex.lock();
            if (data != NULL) dataQueue.push_back(std::move(data));
            queueMutex.unlock();
            //static unsigned cnt = 0;
            //std::cout << "Pushed " << cnt++ << " " << typeid(T).name() << " objects so far" << std::endl;
            cvNotEmpty.notify_all();
        }
        std::unique_ptr<T> popData() {
            std::unique_ptr<T> tmp;
            queueMutex.lock();
            if(!dataQueue.empty()) {
                tmp = std::move(dataQueue.front());
                dataQueue.pop_front();
            }
            queueMutex.unlock();
			cvNotFull.notify_all();
            return tmp;
        }
        bool full() { return (dataQueue.size() >= m_size);}
        double load() {return ((double) dataQueue.size()/m_size);}
        bool empty() { return dataQueue.empty(); }
        bool isDone() { return doneFlag;}
        void finish() {
        	doneFlag = true;
            cvNotEmpty.notify_all();
        }
        void waitNotEmptyOrDone() {
			std::unique_lock<std::mutex> lk(queueMutex);
			cvNotEmpty.wait(lk, [&] { return doneFlag || !empty(); } );
        }
		void waitNotFull() {
			std::unique_lock<std::mutex> lk(queueMutex);
			cvNotFull.wait(lk, [&] { return !full(); } );
        }
    private:
        std::condition_variable cvNotEmpty, cvNotFull;
        std::mutex queueMutex;
        std::deque<std::unique_ptr<T>> dataQueue;
        std::atomic<bool> doneFlag;
        unsigned int m_size;
};

class HexitecSequentialReader
{
public:
	HexitecSequentialReader(json config=json{}): m_config(config) {
	    if (m_config.empty()) {
	        m_config["SourceFile"] = "./dummy.bin";
	        m_config["BufferLength"] = 10;
	        m_config["CalibrationSet"]["Intersects"] = "Inters.txt";
	        m_config["CalibrationSet"]["Gradients"] = "Grads.txt";
	        m_config["CalibrationSet"]["Thresholds"] = "Thresh.txt";
	        m_config["OrderThreads"] = 1;
	        m_config["TargetHisto"] = "./dummy.hxt";
	        std::ofstream configFile("./DummySequentialReader.json");
	        configFile << std::setw(4) << m_config; 
	        configFile.close();
	    }
	    if (m_config["OrderThreads"] < 1) {
	    	m_config["OrderThreads"] = 1;
	    }
	    m_isInitialised = false;
	    m_readFrames = 0;
	    m_totalFrames = 0;
	    m_orderedFrames = 0;
	    m_buffer = 0;
	}
	~HexitecSequentialReader() {
		for (auto threadElement : m_reading) {
			threadElement->join();
		}
		for (auto threadElement : m_processing) {
			threadElement->join();
		}
		free(m_buffer);
	}
	bool initialise() {
		this->m_calibs = CalibrationSet(m_config["CalibrationSet"]["Intersects"], m_config["CalibrationSet"]["Gradients"], m_config["CalibrationSet"]["Thresholds"]);
    	this->m_calibs.order();
    	this->m_file.open(m_config["SourceFile"], std::ios::in | std::ios::binary);
    	if (!this->m_file.is_open()) {
    		std::cout << "Failed to open source file: " << m_config["SourceFile"] << std::endl;
    	}
    	m_file.seekg (0,m_file.end);
		m_totalFrames = m_file.tellg()/80/80/sizeof(unsigned short);
		m_file.seekg (0);
    	// Should at some point do:
    	this->m_done = false;
		unsigned int readBufferSize = static_cast<unsigned int>(m_config["BufferLength"]) * 6400 * sizeof(unsigned short);
		std::cout << "Allocating Buffer size: " << readBufferSize << " bytes" << std::endl;
		this->m_buffer = static_cast<unsigned short*>(malloc(readBufferSize));
    	this->m_ordered = std::unique_ptr<ClipBoard<HexitecRawFrame> >(new ClipBoard<HexitecRawFrame>(m_config["BufferLength"]));
    	this->m_unordered = std::unique_ptr<ClipBoard<std::array<unsigned short, 6400>> >(new ClipBoard<std::array<unsigned short, 6400>>(m_config["BufferLength"]));
    	this->m_isInitialised = true;
    	return m_isInitialised;
	}
	bool run() {
    	m_reading.push_back(std::move(std::unique_ptr<std::thread>(new std::thread(&HexitecSequentialReader::readerThread, this))));
    	for (unsigned int i =0; i<m_config["OrderThreads"];i++) {
    		m_processing.push_back(std::move(std::unique_ptr<std::thread>(new std::thread(&HexitecSequentialReader::orderThread, this))));
    	}
    	return true;
	}
	
	void readerThread() {
		unsigned int readBufferSize = static_cast<unsigned int>(m_config["BufferLength"]) * 6400 * sizeof(unsigned short);
		while (!m_done) {
    	    {
    	    	std::unique_lock<std::mutex> lk(filemtx);
		        this->m_file.read((char *)m_buffer, readBufferSize);
		        bool stopReading = false;
		        unsigned int pagesRead = m_config["BufferLength"];
		        if (readBufferSize > this->m_file.gcount()) {
		        	pagesRead = this->m_file.gcount()/6400/sizeof(unsigned short);
		        	stopReading = true;
		        }
	    	    for (unsigned int i=0; i<pagesRead; i++) {
	    	    	std::unique_ptr<std::array<unsigned short, 6400>> temp(new std::array<unsigned short, 6400>);
    		    	std::memcpy(&(*temp)[0], &m_buffer[6400*i], temp->size()*sizeof(unsigned short));
					pushMutex.lock();
					m_unordered->waitNotFull();
    		    	m_unordered->pushData(std::move(temp));
    		    	pushMutex.unlock();
    		    	m_readFrames++;
	        	}
    	    	if (stopReading) {
        	    	m_unordered->finish();
        	    	m_done = true;
        	    	break;
        	    }
    	    }
		}
	}
	void orderThread() {
		while(!m_done || !m_unordered->empty()) {
			popMutex.lock();
			m_unordered->waitNotEmptyOrDone();
			std::unique_ptr<std::array<unsigned short, 6400>> tempArray(m_unordered->popData());
			popMutex.unlock();
			if (tempArray) {
				std::unique_ptr<HexitecRawFrame> temp(new HexitecRawFrame(*tempArray, true));
				m_orderedFrames++;
				m_ordered->waitNotFull();
				m_ordered->pushData(std::move(temp));
			} else {
				m_ordered->finish();
			}
			if (m_unordered->isDone() && m_unordered->empty()) {
				m_ordered->finish();
			}
		}
	}
	bool isInitialised() {return m_isInitialised;}
	json const& getConfig() const {return m_config;}
	// Problem here, queue might be in process of finishing last events when we already see done - done needs to propagate
	bool haveData() {return (!m_unordered->isDone() || !m_ordered->isDone() || !m_ordered->empty());}
	bool isDone() {return (m_ordered->isDone() && m_ordered->empty());}
	void waitForData() {m_ordered->waitNotEmptyOrDone();}
	double getReadPercentage() {if (m_isInitialised) {return (double) 100.0*m_readFrames/m_totalFrames;} else {return (double) 0;}}
	double getOrderedPercentage() {if (m_isInitialised) {return (double) 100.0*m_orderedFrames/m_totalFrames;} else {return (double) 0;}}
	double getReadQueueLoad() {return m_unordered->load();}
	double getOrderQueueLoad() {return m_ordered->load();}
	unsigned long getTotalFrameCount() {if (m_isInitialised) {return m_totalFrames;} else {return (unsigned long) 0;}}
	std::unique_ptr<HexitecRawFrame> getNextRawFrame() { return m_ordered->popData(); }


private:
	friend std::ostream & operator<<(std::ostream &os, const HexitecSequentialReader& h);
	std::mutex filemtx;
	std::mutex pushMutex, popMutex;
	unsigned short * m_buffer;
	unsigned int m_readFrames, m_totalFrames, m_orderedFrames;
	CalibrationSet m_calibs;
	std::vector<std::shared_ptr<std::thread>> m_reading, m_processing;
	std::unique_ptr<ClipBoard<HexitecRawFrame> > m_ordered;
	std::unique_ptr<ClipBoard<std::array<unsigned short, 6400>> >m_unordered;
	bool m_isInitialised;
	std::atomic<bool> m_done;
	json m_config;
	std::ifstream m_file;
};

/**
 * \brief Reader class for binary data files (*.bin) stored from a hexitec detector
 * 
 * An object that allows to read a hexitec binary file through use of readFrame() and get[Next][Raw]Frame() methods.
 * The object is configured through use of a JSON configuration file, but can also be set up with a new Source binary file and histogram target (only provided in name by the class)
 */

	class HexitecBinaryReader
	{
	public:
/**
 * Constructs the HexitecBinaryReader class based on a json configuration object. If omitted, it is initialised with an empty json object, and providing standard sets of configuration from the source
 *
 * @param config A json configuration object - if no json object is handed over, the class is initialised with an emtpy configuration set that can later be written to file
 */

		HexitecBinaryReader(json config=json{});
/**
 * Standard destructor
 */
		~HexitecBinaryReader() {
	//		if (m_calibs) delete m_calibs;
		}
/**
 * Sets up the reader class to be ready for readFrame(). This will open the configured sourcefile, seek the end once to estimate the total number of frames and then reset to the beginning to be ready for reading.
 */
		bool initialise();
		
/**
 * Retrieves a HexitecFrame from the local frame buffer 
 *
 * @param index Used to indicate which frame in the local framebuffer is requested. 0 is the oldest frame in the buffer, getBufferLength()-1 is the latest. Where an index beyond Bufferlength is given,
 * an empty HexitecFrame is returned.
 * @return Returns a HexitecFrame from a Rawframe in the object's local buffer by applying the configuration configured for this binary reader.
 */

		std::shared_ptr<HexitecFrame> getFrame(unsigned int index);

/**
 * Retrieves the latest HexitecFrame from the local frame buffer 
 */

		std::shared_ptr<HexitecFrame> getNextFrame();

/**
 * Retrieves a HexitecRawFrame from the local frame buffer 
 *
 * @param index Used to indicate which frame in the local framebuffer is requested. 0 is the oldest frame in the buffer, getBufferLength()-1 is the latest. Where an index beyond Bufferlength is given,
 * an empty HexitecFrame is returned.
 * @return Returns a HexitecRawFrame from the object's local buffer.
 */

		std::shared_ptr<HexitecRawFrame> getRawFrame(unsigned int index);

/**
 * Retrieves the latest HexitecRawFrame from the local frame buffer 
 */

		std::shared_ptr<HexitecRawFrame> getNextRawFrame();
		
/**
 * Read's a new HexitecRawFrame from Sourcefile into the local frame buffer. If the local frame buffer is already at maximum length, the earliest read frame (index 0) is deleted 
 * and the new frame is pushed_back to the end of the local buffer.
 *
 * @param nFrames Number of Frames to be read from the file. Frames will all be read and filled into the local buffer, deletion happening as required when hitting the maximum local buffer size.
 * @return Returns a HexitecRawFrame from the object's local buffer.
 */

		bool readFrame(unsigned int nFrames=1);

		void setIntersects(std::string filename) {m_config["CalibrationSet"]["Intersects"] = filename; m_isInitialised=false; this->initialise();}
		void setGradients(std::string filename) {m_config["CalibrationSet"]["Gradients"] = filename; m_isInitialised=false; this->initialise();}
		void setThresholds(std::string filename) {m_config["CalibrationSet"]["Thresholds"] = filename; m_isInitialised=false; this->initialise();}

		json const& getConfig() const {return m_config;}
		void plotCalibration(std::string preText = "") {m_calibs.plot(preText);}
/**
 * Returns the current location of the filepointer in Sourcefile
 */
		int getFilePos() {return this->m_file.tellg();}

/**
 * Returns the current length of the local framebuffer
 */

	    unsigned int getBufferLength() {return this->frames.size();}

/**
 * Sets the source filename of the file to be read by this reader.
 *
 * @param sourceFile The new filename of the binary file to be read by this reader.
 */

	    void setSourceFile(std::string sourceFile) {m_config["SourceFile"] = sourceFile;}

/**
 * Sets the target histogram filename of this binary reader.
 *
 * @param targetHisto The new filename to be stored for the target histogram file
 */

	    void setTargetHisto(std::string targetHisto) {m_config["TargetHisto"] = targetHisto;}

/**
 * Returns total Framecount in source file
 */

	    unsigned int getTotalFrameCount() {return ((unsigned int) m_totalFrames);}

/**
 * Returns total Framecount in source file
 */

	    unsigned int getFrameCount() {return ((unsigned int) m_readFrames);}

/**
 * Returns the percentage of Frames already read out of the total frame in Sourcefile
 */

	    double getReadPercentage() {return ((double) m_readFrames*100.0/m_totalFrames);}
	private:

/**
 * Dumps the configuration to the given ostream
 */

		friend std::ostream & operator<<(std::ostream &os, const HexitecBinaryReader& h);
		unsigned int m_readFrames, m_totalFrames;
		CalibrationSet m_calibs;
		std::deque<std::shared_ptr<HexitecRawFrame>> frames;
        std::array<unsigned short, 6400> buffer;
		bool m_isInitialised;
		json m_config;
		std::ifstream m_file;
	};

/*
 *  Storage for Hit Data with surrounding area defineable
 */
    class HitMatrix {
    public:
        /*
        HitMatrix(unsigned int size_x, unsigned int size_y, HexitecRawFrame sourceVals,\
        unsigned int centreX, unsigned int centreY) : m_sizeX(size_x), m_sizeY(size_y) {
            hits = std::vector<unsigned short>(m_sizeX*m_sizeY,0);
            unsigned int halfsizeX = m_sizeX/2;
            unsigned int halfsizeY = m_sizeY/2;
            unsigned int minX = centreX;
            unsigned int maxX = centreX + m_sizeX - halfsizeX;
            unsigned int minY = centreY;
            unsigned int maxY = centreY + m_sizeY - halfsizeY;
            if (centreX >= (m_sizeX/2)) {
                minX -= m_sizeX/2;
            } else {
                minX = 0;
            }
            if (maxX > 80) {
                maxX=80;
            }
            if (centreY >= (m_sizeY/2)) {
                minY -= m_sizeY/2;
            } else {
                minY = 0;
            }
            if (maxY > 79) {
                maxY=79;
            }
            for (unsigned int x = 0; x<=(maxX-minX); x++) {
                for (unsigned int y = 0; y<=(maxY-minY); y++) {
                    hits[x+y*m_sizeX] = sourceVals(minX+x, minY+y);
                }
            }
        }*/

        HitMatrix(int size_x, int size_y, int xPos=0, int yPos=0) :\
        m_sizeX(size_x), m_sizeY(size_y),xCoord(xPos), yCoord(yPos) {
            hits = std::vector<unsigned short>(m_sizeX*m_sizeY,0);
            isolated = false;
        }


/**
 * Stores values in a vector.
 *
 * @param x location in x of the value to be set.
 * @param y location in y of the value to be set.
 * @param val value to be set at location x/y.
 * @return boolean whether the value has been set according to parameters.
 */

        bool setVal(int x, int y, unsigned short val) {
            if (x >= m_sizeX||y >= m_sizeY || x<0 || y<0) return false;
            hits[x+y*m_sizeX] = val;
            return true;
        }

/**
 * Fetches values in a vector.
 *
 * @param x location in x of the value to be fetched.
 * @param y location in y of the value to be fetched.
 * @return (unsigned int) the value has been set according to parameters.
 */

        unsigned short getVal(int x, int y) {
            if (x >= m_sizeX|| y >= m_sizeY || x<0 || y<0) return 0;
            return hits[x+y*m_sizeX];
        }

/**
 * Checks if the hit is isolated (condition to be defined in the cxx file; currently, no neighbors above threshold)
 *
 * @return boolean whether the the hit is isolated.
 */

        bool isIsolated () {
            if (aboveCounts==1) {isolated=true;}
            return isolated;
        }

        /*counts the number of pixels within this m_sizeX*m_sizeY region that satisfies a condition
        * in the test code, the condition being above the threshold
        */
        void addCounts() {
            aboveCounts += 1;
        }

/**
 * Finds the coordinates of the hit in the raw frame.
 *
 * @return x, y coordinates of the hit in the raw frame.
 */
        // get coordinates of a significant hit from the raw frame
        unsigned int getXcoord() {
            return xCoord;
        }
        unsigned int getYcoord() {
            return yCoord;
        }

/**
 * Finds the maximum pixel value of the region (of size sizeX*sizeY) surrounding the hit.
 *
 * @return the maximum pixel value of the region.
 */

        // get the maximum value within the region
        unsigned int maxVal() {
            unsigned int maxV = 0;
            for (int i = 0; i<m_sizeX*m_sizeY; i++) {
                if (hits[i] > maxV) {
                    //only do this method on isolated single hits maps so there cannot be 2 positions having the same max value
                    maxV = hits[i];
                }
            }
            return maxV;
        }

/**
 * Calculates the total pixel value of the region (of size sizeX*sizeY) surrounding the hit.
 *
 * @return the total pixel value of the region.
 */

        // get the sum of values in the vector
        unsigned int getTot() {
            unsigned int tot = 0;
            for (int i = 0; i<m_sizeX*m_sizeY; i++) {
                tot += hits[i];
            }
            sum = tot;
            return sum;
        }

        void setTot() {
            unsigned int tot = 0;
            for (int i = 0; i<m_sizeX*m_sizeY; i++) {
                tot += hits[i];
            }
            sum = tot;
        }


/**
 * Calculates the centre-of-mass coordinates of the region (of size sizeX*sizeY) surrounding the hit,\
 * taking the hit to be the origin with coordinates (0,0).
 *
 * @return the x and y coordinates of the centre of mass.
 */
        // find centre of mass coordinates (offset relative to (0,0))
        double coMx() {
            double weightedSum = 0;
            //double coMxCoord;
            for (int i = 0; i<m_sizeX; i++) {
                for (int j = 0; j<m_sizeY; j++) {
                    if (xCoord-1+i>=0 && yCoord-1+j>=0 && xCoord-1+i<80 && yCoord-1+j<80) {
                        weightedSum += (i-1)*hits[i+m_sizeX*j];
                    }
                }
            }
            return weightedSum/sum;
        }
        double coMy() {
            double weightedSum = 0;
            for (int i = 0; i<m_sizeX; i++) {
                for (int j = 0; j<m_sizeY; j++) {
                    if (xCoord-1+i>=0 && yCoord-1+j>=0 && xCoord-1+i<80 && yCoord-1+j<80) {
                        weightedSum += (j-1)*hits[i+m_sizeX*j];//index wrong?
                    }
                }
            }
            return weightedSum/sum;
        }


    private:
        std::vector<unsigned short> hits;
        int m_sizeX, m_sizeY, xCoord, yCoord;
        unsigned int sum=0, aboveCounts=0;
        //xCoord, yCoord are coordinates of the significant hit in the raw frame
        bool isolated;
    };

// The following currently has a massive memory footprint, but will hopefully do the trick for now
// Dynamic memory allocation needs looking at, check out Seths dark ways of doing multi-dimensional arrays
class HexitecRawHistogram{
public:
	HexitecRawHistogram(unsigned int nBins=8192) {
		m_nBins = nBins;
		this->data = new unsigned int[nBins*80*80];
	}
	HexitecRawHistogram(std::string filename) {
		m_nBins = 1;
		this->data = new unsigned int[1*80*80];
		this->loadHistogram(filename);
	}
	~HexitecRawHistogram() {
		delete [] this->data;
		this->data=0;
	}
	unsigned int binIndex(unsigned int x, unsigned int y, unsigned int binNumber=0) {return x*m_nBins*80+y*m_nBins+binNumber;}
	void histogramRawFrame(HexitecRawFrame const &frame) {
		for(int i=0; i<6400; i++) {
			if (frame.data[i] < m_nBins) {
				unsigned int index = i*m_nBins+frame.data[i];
				this->data[index]++;
			}
		}
	}
	void histogramRawFrame(std::shared_ptr<HexitecRawFrame> frame) {
		for(int i=0; i<6400; i++) {
			if (frame->data[i] < m_nBins) {
				unsigned int index = i*m_nBins+frame->data[i];
				this->data[index]++;
			}
		}
	}
	void histogramHitMatrix (HitMatrix hit) {
	    if (hit.maxVal() < m_nBins) {
	        unsigned int index = (hit.getXcoord()+hit.getYcoord()*80)*m_nBins+hit.maxVal();
	        this->data[index]++;
	    }
	}
	//iterates through every HitMatrix in the vector array
	void histogramHitMatrixVector (std::vector<HitMatrix> hits) {
        for (auto hit:hits) {
            histogramHitMatrix(hit);
        }
    }

    //extracts a single pixel histogram
    Histo1D *extractPixelHisto(unsigned int x, unsigned int y, unsigned int threshold=0) {
        Histo1D *tempHisto = new Histo1D("TemporaryHistogram", m_nBins, threshold, m_nBins-1);
        for(unsigned int i=threshold; i<m_nBins; i++) {
            tempHisto->setBinContent(i+1-threshold, this->data[((x+y*80)*m_nBins)+i]);
        }
        return tempHisto;
    }


    void reInitialise(unsigned int nBins) {
		delete [] this->data;
		this->data =0;
		m_nBins = nBins;
		this->data = new unsigned int[nBins*80*80];
	}
	unsigned int * reduce(unsigned int reductionFactor=2) {
		if (!reductionFactor) {
			return (unsigned int *) 0;
		}
		unsigned int newBins = m_nBins/reductionFactor + ((m_nBins%reductionFactor) > 0 ? 1 : 0);
		unsigned int *temp = new unsigned int[newBins*80*80];
		for(int i=0; i<80; i++) {
			for (int k=0; k<80; k++) {
				for (unsigned int binNumber = 0; binNumber<m_nBins; binNumber++) {
					unsigned int index = i*80*m_nBins+k*m_nBins+binNumber;
					temp[i*80*newBins+k*newBins+(binNumber/reductionFactor)] += this->data[index];
				}
			}
		}
		return temp;
	}
	bool dumpHistogram(std::string filename, unsigned int reductionFactor = 1) {
		if (!reductionFactor) {
			return false;
		}
		std::ofstream histoFile(filename, std::ios::out | std::ios::binary);
		if (reductionFactor == 1) {
			histoFile.write((char *) this->data, m_nBins*80*80*sizeof(unsigned int));
		} else { //reduce data first (into a new set, temporary, then save that data
			unsigned int * temp = this->reduce(reductionFactor);
			unsigned int newBins = m_nBins/reductionFactor + ((m_nBins%reductionFactor) > 0 ? 1 : 0);
			histoFile.write((char *) temp, newBins*80*80*sizeof(unsigned int));
		}
		histoFile.close();
		return true;
	}
	bool loadHistogram(std::string filename) {
		std::ifstream histoFile(filename, std::ios::in | std::ios::binary);
		histoFile.seekg (0,histoFile.end);
		unsigned int nBins = histoFile.tellg()/80/80/sizeof(unsigned int);
		histoFile.seekg (0);
		this->reInitialise(nBins);
		histoFile.read((char *) this->data, m_nBins*80*80*sizeof(unsigned int));
		histoFile.close();
		return true;
	}
	void printPixel(unsigned int x, unsigned int y, bool printEmpty=true) {
		for(unsigned int i=0; i<m_nBins; i++) {
			if (printEmpty || this->data[x*m_nBins*80+y*m_nBins+i]) {
				std::cout << i << "\t" << this->data[x*m_nBins*80+y*m_nBins+i] << std::endl;
			}
		}
	}
	unsigned int integral(unsigned int x, unsigned int y, unsigned int maxBin) {
		unsigned int tempIntegral=0;
		for (unsigned int tempBin=0; tempBin<maxBin; tempBin++) {
			tempIntegral += this->data[binIndex(x, y, tempBin)];
		}
		return tempIntegral;
	}
	unsigned int countNZBins(unsigned int x, unsigned int y) { // Function to count non-zero bins for a single pixel histogram
		unsigned int binCount=0;
		for (unsigned int tempBin=0; tempBin<m_nBins; tempBin++) {
			binCount += (unsigned int) (this->data[binIndex(x, y, tempBin)] > 0);
		}
		return binCount;
	}
	unsigned int integrate(unsigned int x, unsigned int y, unsigned int start=0, unsigned int end=65536) { // Function to count non-zero bins for a single pixel histogram
		unsigned int integral=0;
		if ((start > m_nBins) || (start > end)) return 0;
		if (end > m_nBins) end = m_nBins;
		for (unsigned int tempBin=start; tempBin<end; tempBin++) {
			integral += this->data[binIndex(x, y, tempBin)];
		}
		return integral;
	}
	unsigned int getNBins() {return m_nBins;}
	unsigned int getVal(unsigned int x, unsigned int y, unsigned int bin) {return this->data[binIndex(x,y,bin)];}
	std::vector<unsigned int> findPeaks(unsigned int x, unsigned int y) {
		std::cout << "Peakfinder, called with coords " << x << " " << y << std::endl;
		std::cout << "Number of bins is " << m_nBins << std::endl;
		std::cout << "Maximum index is " << binIndex(79,79,m_nBins) << std::endl;
		std::cout << "Maximum index in following loop will be " << binIndex(x,y,m_nBins-1) << std::endl;
		std::vector<unsigned int> peaks;
		for (unsigned int tempBin=1; tempBin<m_nBins-1; tempBin++) {
			if ((this->data[binIndex(x,y,tempBin)] > this->data[binIndex(x,y,tempBin)-1]) && (this->data[binIndex(x,y,tempBin)] > this->data[binIndex(x,y,tempBin)+1])) {
				peaks.push_back(tempBin);
			}
		}
		return peaks;
	}
private:
	unsigned int *data;
	unsigned int m_nBins; //number of bins (in the histogram for energy counts) per pixel
	HexitecArray<double> peakFinder;
};

}

#endif
