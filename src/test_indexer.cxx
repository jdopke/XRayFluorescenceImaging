#include "hexitec.hpp"

#ifdef USE_SNAPPY_COMPRESSION
#include <snappy.h>
#endif

#include <nlohmann/json.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <chrono> 
using namespace std::chrono;


using namespace libhexitec;

int main(int argc, char* argv[]) {
	if (argc > 1) {
		std::cerr << argv[0] << " does not require or process any parameters" << std::endl;
	}
	std::vector<unsigned int> alreadyTouched;
	for (unsigned int i=0; i<80; i++) {
		if (((i%20) == 0) && i>0) {
			std::cout << std::endl;
		}
		std::cout << i << "\t" << arrayIndexHelper(i);
//		if (std::find(alreadyTouched.begin(), alreadyTouched.end(), i) != alreadyTouched.end()) {
//			std::cout << "\tDone already";
//		}
		alreadyTouched.push_back(indexHelper(i));
		std::cout << std::endl;
	}
	return 0;
}