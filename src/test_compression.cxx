// Test_readBinary
// Should read a single frame from a binary hexitec file and display it
// Could also read calibration maps and then spit out a calibrated file

#include "hexitec.hpp"

#ifdef USE_SNAPPY_COMPRESSION
#include <snappy.h>
#endif

#include <nlohmann/json.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <chrono>
using namespace std::chrono;

using namespace libhexitec;


static std::string base64_encode(const std::string &in) {

    std::string out;

    int val=0, valb=-6;
    for (unsigned char c : in) {
        val = (val<<8) + c;
        valb += 8;
        while (valb>=0) {
            out.push_back("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[(val>>valb)&0x3F]);
            valb-=6;
        }
    }
    if (valb>-6) out.push_back("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[((val<<8)>>(valb+8))&0x3F]);
    while (out.size()%4) out.push_back('=');
    return out;
}

static std::string base64_decode(const std::string &in) {

    std::string out;

    std::vector<int> T(256,-1);
    for (int i=0; i<64; i++) T["ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"[i]] = i; 

    int val=0, valb=-8;
    for (unsigned char c : in) {
        if (T[c] == -1) break;
        val = (val<<6) + T[c];
        valb += 6;
        if (valb>=0) {
            out.push_back(char((val>>valb)&0xFF));
            valb-=8;
        }
    }
    return out;
}

class CompressionTest{
public:
	void push_back(unsigned int sizeUncompressed, std::string frame) {uncompressed.push_back(sizeUncompressed); std::string temp = base64_encode(frame); frames.push_back(temp); storage.push_back(temp.size()); compressed.push_back(frame.size());}
	double compressedAvg() {
		double sum=0.0;
		for(auto &element : compressed) {
			sum +=  (double) element;
		}
		return (double) sum/(double) compressed.size();
	}
	double storageAvg() {
		double sum=0.0;
		for(auto &element : storage) {
			sum +=  (double) element;
		}
		return (double) sum/(double) storage.size();
	}
	double uncompressedAvg() {
		double sum=0.0;
		for(auto &element : uncompressed) {
			sum +=  (double) element;
		}
		return (double) sum/(double) uncompressed.size();
	}
	unsigned int size() {return compressed.size();}
	std::vector<std::string> getFrames() {return frames;}
private:
	std::vector<unsigned int> compressed;
	std::vector<unsigned int> storage;
	std::vector<unsigned int> uncompressed;
	std::vector<std::string> frames;
};

int main(int argc, char* argv[]) {
#ifdef USE_SNAPPY_COMPRESSION

	if (argc < 2) {
		std::cout << "Usage:      " << argv[0] << " <JSON config filename> <JSON output filename>" << std::endl;
		std::cout << "  <JSON config filename>: A JSON config filename to run this program with." << std::endl;
		std::cout << "  <JSON output filename>: A JSON based output file for storing the compressed raw frames" << std::endl;
		std::cout << "No parameters given, creating an empty reader, writing out its config and exiting" << std::endl;
		HexitecBinaryReader temp;
		return -1;
	}

	std::ifstream jsonFile(argv[1]);
	HexitecBinaryReader reader(json::parse(jsonFile));
	reader.initialise();
	reader.plotCalibration("Dummy_");

 	auto start = high_resolution_clock::now();

 	CompressionTest cTest;
// 	HexitecRawFrame* oldFrame = 0;
 	while(reader.readFrame(1)) {
		std::shared_ptr<HexitecRawFrame> tempRawFrame = reader.getRawFrame(0);
		if (tempRawFrame->isConstant()) {
			std::cout << " Yet another constant Frame at index: " << cTest.size()  << " Value: " << tempRawFrame->data[0] << " @ File Position: " << reader.getFilePos() << std::endl;
		}
/*		if (oldFrame.data != tempRawFrame.data) {
			std::cout << "Found two consecutive Frames that were the same at frame index: " << cTest.size() << std::endl;
			for (auto &element : oldFrame.data) {
				std::cout << element << "\t";
			}
			std::cout << std::endl;
		}
*/
		std::string output;
		unsigned int data_size =  tempRawFrame->data.size()*sizeof(tempRawFrame->data[0]);
		snappy::Compress((char *) (&tempRawFrame->data[0]), data_size, &output);
		cTest.push_back(data_size, output);
		base64_decode(base64_encode(output)).size();
//		oldFrame = tempRawFrame;
	}
 	auto stop = high_resolution_clock::now();
 	auto duration = duration_cast<microseconds>(stop - start); 
 	std::cout << "Time taken to load and compress " << cTest.size() << " frames: " << duration.count() << "us (" << duration.count()/(double) cTest.size() << "us/frame, " << (double) cTest.size()*12800.0/duration.count()*1.e6/1024./1024. << "MB/s)" << std::endl; 
 	std::cout << "Frame size is (on average):            " << cTest.uncompressedAvg() << std::endl;
 	std::cout << "Compressed frame size is (on average): " << cTest.compressedAvg() << "\t";
 	std::cout << "Compression ratio: " << cTest.compressedAvg()/cTest.uncompressedAvg()*100.0 << "%" << std::endl;
 	std::cout << "Storage frame size is (on average):    " << cTest.storageAvg() << "\t";
 	std::cout << "Compression ratio: " << cTest.storageAvg()/cTest.uncompressedAvg()*100.0 << "%" << std::endl;



 	if (argc>2) {
 		std::ofstream jsonOutFile(argv[2]);
 		json outputObject;
 		outputObject["Header"] = std::string("This is Hexitec Binary file, stored with per-frame snappy compression (from unsigned short values)");
 		outputObject["Frames"] = cTest.getFrames();
 		jsonOutFile << std::setw(4) << outputObject << std::endl;
 		jsonOutFile.close();
 	}
	return 0;

#else // without snappy
	std::cout << "You are running the test compression without having found snappy during compile time - exiting gracefully..." << std::endl;
	return -1;
#endif
}