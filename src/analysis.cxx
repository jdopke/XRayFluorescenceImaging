// Test_readBinary
// Should read a single frame from a binary hexitec file and display it
// Could also read calibration maps and then spit out a calibrated file

#include "hexitec.hpp"

#include <nlohmann/json.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <chrono> 
using namespace std::chrono;


using namespace libhexitec;


int main(int argc, char* argv[]) {
    std::string fileName="", outFileName="temp.svg", option;
    bool haveFile=false, quiet=false, reduceHisto=false, integrateHisto=false;
    unsigned int reductionFactor=1;
    unsigned int startIntegral = 0;
    unsigned int stopIntegral = 65536;
    unsigned int setRatio=0;
    if (argc > 1) {
        for (int i=1; i<argc; i++) {
            option = argv[i];
            if (option[0] != '-') {
                fileName = option;
                haveFile = true;
            }
            switch (option[1]) {
                case 'q': {
                    quiet = true;
                    break;
                }
                case 's': {
                    if (option.length() == 2) {
                        setRatio = atoi(argv[++i]);
                    } else {
                        setRatio = atoi(option.substr(2).c_str());
                    }
                    break;
                }
                case 'r': {
                    reduceHisto = true;
                    if (option.length() == 2) {
                        reductionFactor = atoi(argv[++i]);
                    } else {
                        reductionFactor = atoi(option.substr(2).c_str());
                    }
                    break;
                }
                case 'i': {
                    integrateHisto = true;
                    break;
                }
                case 'b': {
                    if (option.length() == 2) {
                        startIntegral = atoi(argv[++i]);
                    } else {
                        startIntegral = atoi(option.substr(2).c_str());
                    }
                    break;
                }
                case 'e': {
                    if (option.length() == 2) {
                        stopIntegral = atoi(argv[++i]);
                    } else {
                        stopIntegral = atoi(option.substr(2).c_str());
                    }
                    break;
                }
                case 'o': {
                    outFileName = argv[++i];
                    break;
                }
                default: {
                    break;
                }
            }
        }
    } else {
        std::cout << "Usage:      " << argv[0] << " <Histo filename (*.hxt)> -x <x> -y <y> -q -b <begin> -e <end> -o <outFileName>" << std::endl;
        std::cout << "  <Histo filename>: A histogram file as created by (e.g.) test_binReader." << std::endl;
        std::cout << "  -r <reduction>  : set reduction factor" << std::endl;
        std::cout << "  -s <ratio>      : beginning of integral for 2D plot" << std::endl;
        std::cout << "  -q              : flag to shut up (quiet)" << std::endl;
        std::cout << "  -b <begin>      : beginning of integral for 2D plot" << std::endl;
        std::cout << "  -e <end>        : end of integral for 2D plot" << std::endl;
        std::cout << "  -o <outFileName>: output filename for 2D plot" << std::endl;
        return -1;
    }

    if (haveFile) {
        HexitecRawHistogram histo(fileName);
        if (reduceHisto) {
            histo.dumpHistogram("/tmp/dumpHisto.csv", reductionFactor);
            histo.loadHistogram("/tmp/dumpHisto.csv");
        }
        HexitecArray<unsigned int> nBinsMap;
        unsigned int x, y;
        for (x=0; x<80; x++) {
            for (y=0; y<80; y++) {
                if (integrateHisto) {
                    nBinsMap(x,y) = histo.integrate(x,y,startIntegral,stopIntegral);
                } else {
                    nBinsMap(x,y) = histo.countNZBins(x, y);
                }
            }
        }
        nBinsMap.plot(outFileName);
        nBinsMap.toFile("temp.csv", true);
        if (!quiet) {
            std::cout << "Found a maximum value of: " << nBinsMap.getMaxVal() << std::endl;
            if (setRatio && (nBinsMap.getMinVal() > setRatio)) std::cout << "Found a maximum value of: " << nBinsMap.getMinVal() << std::endl;
        }
    } else {
        std::cout << "You screwed up, no filename given" << std::endl;
    }

    return 0;
}