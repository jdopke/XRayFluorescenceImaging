// Test_readBinary
// Should read a single frame from a binary hexitec file and display it
// Could also read calibration maps and then spit out a calibrated file

#include "hexitec.hpp"

#ifdef USE_SNAPPY_COMPRESSION
#include <snappy.h>
#endif

#include <nlohmann/json.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <chrono> 
using namespace std::chrono;


using namespace libhexitec;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		std::cout << "Usage:      " << argv[0] << " <JSON config filename>" << std::endl;
		std::cout << "  <JSON config filename>: A JSON config filename to run this program with." << std::endl;
		std::cout << "No parameters given, creating an empty reader, writing out its config and exiting" << std::endl;
		HexitecBinaryReader temp;
		return -1;
	}

	std::ifstream jsonFile(argv[1]);
	HexitecBinaryReader reader(json::parse(jsonFile));
	std::cout << "Created binary reader with buffer length: " << reader.getConfig()["BufferLength"] << std::endl;
	if (!reader.readFrame()) {
		reader.initialise();
	}
	if (reader.readFrame(0)) {
		std::cout << "Checked reader is working correctly, dumping configuration:" << std::endl;
		std::cout << reader << std::endl;
	}
	reader.plotCalibration("Dummy_");

	HexitecRawHistogram histo(8192);
	unsigned short maxVal=0;
	unsigned short minVal=65535;
	unsigned long frameCount = 0;
	std::shared_ptr<HexitecRawFrame> temp;
	std::cout << "Processing input file for " << reader.getTotalFrameCount() << " frames" << std::endl;

	auto start = high_resolution_clock::now();

	while(reader.readFrame()) { // this will end after all frames have been read from file, not, when all frames are processed
		temp = reader.getRawFrame(0);
		frameCount++;
		temp->getMinMax(minVal, maxVal);
		histo.histogramRawFrame(*temp);
		auto now = high_resolution_clock::now();
		auto framerate = (double) 1000000 * (double) frameCount / duration_cast<microseconds>(now - start).count();
		std::cout << "[" << framerate << "fps]" << " read " << std::fixed << std::setprecision(2) << std::setw(6) << reader.getReadPercentage() << "%%    \r";		
	}
	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start); 
	auto framerate = (double) 1000000 * (double) frameCount / duration_cast<microseconds>(stop - start).count();
	std::cout << "[" << framerate << "fps]" << " read " << std::fixed << std::setprecision(2) << std::setw(6) << reader.getReadPercentage() << "%%    \n";		
	std::cout << "Microseconds for running over this file: " << duration.count() << std::endl; 
//	reader.getRawFrame(0).dump();
	std::string histoFileName = std::string(reader.getConfig()["TargetHisto"]);
	histo.dumpHistogram(histoFileName);
	histoFileName.replace(histoFileName.find(".hxt"), 4, "_small.hxt");
	histo.dumpHistogram(histoFileName, 0);
	histo.reduce(0);
	histo.dumpHistogram(histoFileName, 16);
	std::shared_ptr<HexitecRawFrame> tempRawFrame = reader.getRawFrame(0);
	tempRawFrame->plot("Dummy_RawFrame.svg");
//	reader.getFrame(0).dump();
	std::shared_ptr<HexitecFrame> tempFrame = reader.getFrame(0);
	tempFrame->plot(std::string("Dummy_Frame.svg"));
	tempRawFrame->getMinMax(minVal, maxVal);
	std::cout << "Minimal Value in Run was: " << minVal << "   Maximum was: " << maxVal << std::endl;

	HexitecFrame testFrame;
	if (testFrame.isConstant()) {
		testFrame.data[0] = 1.0;
	}
	if (!testFrame.isConstant()) {
		testFrame.data[0] = 0.0;
		testFrame.dump();
	}

#ifdef USE_SNAPPY_COMPRESSION
	std::string output;
	unsigned int data_size =  tempRawFrame->data.size()*sizeof(tempRawFrame->data[0]);
	std::cout << "Data size to be reduced: " << data_size << std::endl;
	snappy::Compress((char *) (&tempRawFrame->data[0]), data_size, &output);
	std::string output_uncom;
	snappy::Uncompress(output.data(), output.size(), &output_uncom);
	std::cout << "input size:" <<  data_size << " output size:" << output.size() << " uncompressed: " << output_uncom.size() << std::endl;
#endif

	return 0;
}