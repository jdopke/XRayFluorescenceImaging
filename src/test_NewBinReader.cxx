// Test_readBinary
// Should read a single frame from a binary hexitec file and display it
// Could also read calibration maps and then spit out a calibrated file

#include "hexitec.hpp"
#include "histo1D.h"


#include <nlohmann/json.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <chrono> 
using namespace std::chrono;


using namespace libhexitec;

int main(int argc, char* argv[]) {
	if (argc < 2) {
		std::cout << "Usage:      " << argv[0] << " <JSON config filename>" << std::endl;
		std::cout << "  <JSON config filename>: A JSON config filename to run this program with." << std::endl;
		std::cout << "No parameters given, creating an empty reader, writing out its config and exiting" << std::endl;
		HexitecBinaryReader temp;
		return -1;
	}

	std::ifstream jsonFile(argv[1]);
	HexitecBinaryReader reader(json::parse(jsonFile));
	std::cout << "Created binary reader with buffer length: " << reader.getConfig()["BufferLength"] << std::endl;
	if (!reader.readFrame()) {
		reader.initialise();
	}
	if (reader.readFrame(0)) {
		std::cout << "Checked reader is working correctly, dumping configuration:" << std::endl;
		std::cout << reader << std::endl;
	}
	reader.plotCalibration("Dummy_");

	HexitecRawHistogram histo(8192);

	auto start = high_resolution_clock::now();
	//reader.readFrame(); // this will end after all frames have been read from file, not, when all frames are processed

    int sizeX = 3; //need to be odd to be symmetrical around centre
    int sizeY = sizeX;

    std::vector<HitMatrix> hits;
    std::vector<HitMatrix> isolatedHits;
	while (reader.readFrame()) {
        std::shared_ptr<HexitecRawFrame> temp = reader.getRawFrame(reader.getBufferLength()-1);
        //HexitecArray<int> brightArray;
        //HexitecRawFrame temp = reader.getRawFrame(1);

        //double threshold = temp.getAverage() + 3 * temp.getRms();
        //unsigned int thresholdFloor = floor(threshold);
        unsigned int thresholdFloor = 25;

        for (int i = 0; i < 80; i++) {
            for (int j = 0; j < 80; j++) {
                if ((*temp)(i, j) > thresholdFloor) {
                    HitMatrix hit(sizeX, sizeY, i, j);
                    for (int y = 0; y < sizeX; y++) {
                        for (int z = 0; z < sizeY; z++) {
                            if ((i - 1 + y) >= 0 && (j - 1 + z) >= 0 && (i - 1 + y) < 80 && (j - 1 + z) < 80) {
                                hit.setVal(y, z, (*temp)(i - 1 + y, j - 1 + z));
                                if (hit.getVal(y, z) > thresholdFloor) { hit.addCounts(); }
                            }
                        }
                    }// these nested for loop iterates through the 2d matrix map of size sizeX*sizeY and stores pixel values
                    if (hit.isIsolated()) {
                        hit.setTot();
                        isolatedHits.push_back(hit);
                    }
                }
            }
        }

       	histo.histogramHitMatrixVector(isolatedHits);
       	isolatedHits.clear();
    }

	auto stop = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(stop - start); 
	std::cout << "Microseconds for running over this file: " << duration.count() << std::endl; 
	std::string histoFileName = std::string(reader.getConfig()["TargetHisto"]);
	histo.dumpHistogram(histoFileName);

	return 0;
}