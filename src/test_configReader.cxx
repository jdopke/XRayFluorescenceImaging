#include "hexitec.hpp"

int main(int argc, char* argv[]) {
	std::cout << "Version: " << LIBHEXITEC_VERSION_MAJOR << "." << LIBHEXITEC_VERSION_MINOR << std::endl;

	if (argc > 2) {
		libhexitec::CalibrationMap<double> *calibration = new libhexitec::CalibrationMap<double>(argv[2], argv[1]);
		std::string fileName = std::string("./") + std::string(argv[2]);
		if (calibration->isLoaded()) {
			calibration->plot(fileName);
		}
		delete calibration;
	} else if (argc > 1) {
		libhexitec::CalibrationMap<double> *calibration = new libhexitec::CalibrationMap<double>("CalibrationMap", argv[1]);
		calibration->plot("./CalibrationMap.svg");
		calibration->toFile("./Calibration.csv", true);
		delete calibration;
	}
	return 0;
}