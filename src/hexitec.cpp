#include "hexitec.hpp"

namespace libhexitec
{
template <class T>
int CalibrationMap<T>::load(std::string filename) {
    std::ifstream file(filename);
    int i = 0;
    while(file.good()) {
        std::string                line;
        std::getline(file,line);

        std::stringstream          lineStream(line);
        std::string                cell;

        while(std::getline(lineStream,cell, '\t'))
        {
            this->data[i++] = atof(cell.c_str());
        }
    }
    this->m_isLoaded = true;
    return i;
}

template <class T>
bool HexitecArray<T>::reorder() {
    std::array<T, 6400> temp;
    T* temp_pointer = &temp[0];
    T* data_pointer = &data[0];
    for (unsigned int i=0; i<6400; i++) {
        *(temp_pointer+indexHelper(i)) = *(data_pointer+i);
    }
    data = temp;
    return true;
}

template <class T>
void HexitecArray<T>::toFile(std::string filename, bool header) {
    std::fstream file(filename, std::fstream::out | std::fstream::trunc);
    // Header
    if (header) {
        file << "Hexitec Array " <<  std::endl;
        file << m_size << std::endl;
        file << m_size << std::endl;
    }
    // Data
    for (unsigned int i=0; i<m_size; i++) {
        for (unsigned int j=0; j<m_size; j++) {
            file << data[i+(j*m_size)] << " ";
        }
        file << std::endl;
    }
    file.close();
}

template <class T>
void HexitecArray<T>::plot(std::string plotFilename, bool logplot) {
	// Put raw histo data in tmp file
    std::string fileAlone;
    if (plotFilename.rfind("/") == std::string::npos) {
        fileAlone = plotFilename;
    } else {
        fileAlone = plotFilename.substr(plotFilename.rfind("/"), plotFilename.length()-plotFilename.rfind("/"));
    }
	std::string tmp_name = std::string("/tmp/" + fileAlone + ".dat");
	std::cout << "Plotting Hexitec array using temporary data file: " << tmp_name << std::endl;
	this->toFile(tmp_name, false);
	std::string cmd = "gnuplot > " + plotFilename;


	// Open gnuplot as file and pipe commands
	FILE *gnu = popen(cmd.c_str(), "w");

	//fprintf(gnu, "set terminal postscript enhanced color \"Helvetica\" 18 eps\n");
	fprintf(gnu, "set terminal svg size 1280, 1024\n");
	fprintf(gnu, "set palette negative defined ( 0 '#D53E4F', 1 '#F46D43', 2 '#FDAE61', 3 '#FEE08B', 4 '#E6F598', 5 '#ABDDA4', 6 '#66C2A5', 7 '#3288BD')\n");
	//fprintf(gnu, "set pm3d map\n");
	fprintf(gnu, "unset key\n");
	fprintf(gnu, "set title \"Data Array\"\n");
	fprintf(gnu, "set xrange[%f:%f]\n", 0., (float) m_size);
	fprintf(gnu, "set yrange[%f:%f]\n", 0., (float) m_size);
	//fprintf(gnu, "set cbrange[0:120]\n");
	//fprintf(gnu, "splot \"/tmp/tmp_%s.dat\" matrix u (($1)*((%f-%f)/%d)):(($2)*((%f-%f)/%d)):3\n", HistogramBase::name.c_str(), xhigh, xlow, xbins, yhigh, ylow, ybins);
    if (logplot) {
        fprintf(gnu, "set logscale cb\n");
    }
    fprintf(gnu, "plot \"%s\" matrix u (($1)*((%f-%f)/%d.0)+%f):(($2)*((%f-%f)/%d.0)+%f):($3) with image\n", tmp_name.c_str(), (float) m_size, 0., m_size, 0.5, (float) m_size, 0., m_size, 0.5);
	// fprintf(gnu, "plot \"%s\" matrix u (($1)):(($2)):3 with image\n", ("/tmp/" + tmp_name + "_" + name + ".dat").c_str());
	pclose(gnu);

	if (remove(tmp_name.c_str()) != 0) {
		std::cout << "Error when removing temporary data file: " << tmp_name << std::endl;
	} else {
		std::cout << "Successfully removed temporary data file: " << tmp_name << std::endl;
	}
}

template <class T>
void CalibrationMap<T>::toFile(std::string filename, bool header) {
    std::fstream file(filename, std::fstream::out | std::fstream::trunc);
    // Header
    if (header) {
        file << "Hexitec Array " <<  std::endl;
        file << this->m_name << std::endl;
        file << this->m_size << std::endl;
        file << this->m_size << std::endl;
    }
    // Data
    for (unsigned int i=0; i<this->m_size; i++) {
        for (unsigned int j=0; j<this->m_size; j++) {
            file << this->data[i+(j*this->m_size)] << " ";
        }
        file << std::endl;
    }
    file.close();
}

template <class T>
void CalibrationMap<T>::plot(std::string plotFilename) {
    // Put raw histo data in tmp file
    std::string fileAlone;
    if (plotFilename.rfind("/") == std::string::npos) {
        fileAlone = plotFilename;
    } else {
        fileAlone = plotFilename.substr(plotFilename.rfind("/"), plotFilename.length()-plotFilename.rfind("/"));
    }
    std::string tmp_name = std::string("/tmp/" + fileAlone + ".dat");
    std::cout << "Plotting " << this->m_name << " CalibrationMap using temporary data file: " << tmp_name << std::endl;
    this->toFile(tmp_name, false);
    std::string cmd = "gnuplot > " + plotFilename;


    // Open gnuplot as file and pipe commands
    FILE *gnu = popen(cmd.c_str(), "w");

    //fprintf(gnu, "set terminal postscript enhanced color \"Helvetica\" 18 eps\n");
    fprintf(gnu, "set terminal svg size 1280, 1024\n");
    fprintf(gnu, "set palette negative defined ( 0 '#D53E4F', 1 '#F46D43', 2 '#FDAE61', 3 '#FEE08B', 4 '#E6F598', 5 '#ABDDA4', 6 '#66C2A5', 7 '#3288BD')\n");
    //fprintf(gnu, "set pm3d map\n");
    fprintf(gnu, "unset key\n");
    fprintf(gnu, "set title \"%s\"\n" , this->m_name.c_str());
    fprintf(gnu, "set xrange[%f:%f]\n", 0., (float) this->m_size);
    fprintf(gnu, "set yrange[%f:%f]\n", 0., (float) this->m_size);
    //fprintf(gnu, "set cbrange[0:120]\n");
    //fprintf(gnu, "splot \"/tmp/tmp_%s.dat\" matrix u (($1)*((%f-%f)/%d)):(($2)*((%f-%f)/%d)):3\n", HistogramBase::name.c_str(), xhigh, xlow, xbins, yhigh, ylow, ybins);
    fprintf(gnu, "plot \"%s\" matrix u (($1)*((%f-%f)/%d.0)+%f):(($2)*((%f-%f)/%d.0)+%f):3 with image\n", tmp_name.c_str(), (float) this->m_size, 0., this->m_size, 0.5, (float) this->m_size, 0., this->m_size, 0.5);
    // fprintf(gnu, "plot \"%s\" matrix u (($1)):(($2)):3 with image\n", ("/tmp/" + tmp_name + "_" + name + ".dat").c_str());
    pclose(gnu);

    if (remove(tmp_name.c_str()) != 0) {
        std::cout << "Error when removing temporary data file: " << tmp_name << std::endl;
    } else {
        std::cout << "Successfully removed temporary data file: " << tmp_name << std::endl;
    }
}

template <class T> template<typename U>
HexitecArray<T>& HexitecArray<T>::operator+=(const HexitecArray<U>& rhs) {
	unsigned int i=0;
	for(auto &element : data) {
		element += (T) rhs.data[i++];
	}
	return *this;
}

template <class T> template<typename U>
HexitecArray<T>& HexitecArray<T>::operator*=(const HexitecArray<U>& rhs) {
	unsigned int i=0;
	for(auto &element : data) {
		element *= (T) rhs.data[i++];
	}
	return *this;
}

/*
template <class T> template<typename U>
HexitecArray<T>& HexitecArray<T>::operator+=(const HexitecArray<U>& rhs) {
    unsigned int i=0;
    for(auto &element : data) {
        element += rhs.data[i++];
    }
    return *this;
}

template <class T> template<typename U>
HexitecArray<T>& HexitecArray<T>::operator*=(const HexitecArray<U>& rhs) {
    unsigned int i=0;
    for(auto &element : data) {
        element *= rhs.data[i++];
    }
    return *this;
}
*/

HexitecBinaryReader::HexitecBinaryReader(json config) : m_config(config) {
    if (m_config.empty()) {
        m_config["SourceFile"] = "./dummy.bin";
        m_config["BufferLength"] = 10;
        m_config["CalibrationSet"]["Intersects"] = "Inters.txt";
        m_config["CalibrationSet"]["Gradients"] = "Grads.txt";
        m_config["CalibrationSet"]["Thresholds"] = "Thresh.txt";
        m_config["TargetHisto"] = "./dummy.hxt";
        std::ofstream configFile("./DummyBinReader.json");
        configFile << std::setw(4) << m_config; 
        configFile.close();
    }
    m_isInitialised = false;
}

bool HexitecBinaryReader::initialise() {
    this->m_calibs = CalibrationSet(m_config["CalibrationSet"]["Intersects"], m_config["CalibrationSet"]["Gradients"], m_config["CalibrationSet"]["Thresholds"]);
    this->m_calibs.order();
    this->m_file.open(m_config["SourceFile"], std::ios::in | std::ios::binary);
	m_file.seekg (0,m_file.end);
	m_totalFrames = m_file.tellg()/80/80/sizeof(unsigned short);
	m_file.seekg (0);
    // Should at some point do:
    this->m_isInitialised = true;
    this->m_readFrames = 0;
    return m_isInitialised;
}

std::shared_ptr<HexitecFrame> HexitecBinaryReader::getFrame(unsigned int index) {if (index > frames.size()) {return std::shared_ptr<HexitecFrame>(new HexitecFrame());} return m_calibs.calibrate(frames[index]);}

std::shared_ptr<HexitecFrame> HexitecBinaryReader::getNextFrame() {this->readFrame(); return m_calibs.calibrate(frames[frames.size()-1]);}

std::shared_ptr<HexitecRawFrame> HexitecBinaryReader::getRawFrame(unsigned int index) {if (index > frames.size()) {return std::shared_ptr<HexitecRawFrame>(new HexitecRawFrame());} return frames[index];}

std::shared_ptr<HexitecRawFrame> HexitecBinaryReader::getNextRawFrame() {this->readFrame(); return frames[frames.size()-1];}

bool HexitecBinaryReader::readFrame(unsigned int nFrames) {
    if (!m_isInitialised) {
        std::cerr << "HexitecBinaryReader was not initialised - call initialise() first!" << std::endl;
        return false;
    }

    if (!nFrames) {
        std::cerr << "readFrame: requested to read zero frames, done..." << std::endl;
        return true;
    }

    for (unsigned int count=0; count < nFrames; count ++)
    {
        if (this->frames.size() >= m_config["BufferLength"]) {
            this->frames.erase(this->frames.begin());
        }

        this->m_file.read((char *)&buffer, buffer.size() * sizeof(unsigned short));
        if (buffer.size() * sizeof(unsigned short) > (unsigned int) this->m_file.gcount()) {
            return false;
        }

        std::shared_ptr<HexitecRawFrame> temp(new HexitecRawFrame(buffer, true));
        m_readFrames++;
        temp->reorder();
        temp->setOrdered();
        this->frames.push_back(temp);
        if (nFrames > 1) std::cout << "Read " << (count+1) << "/" << nFrames << " frames" << std::endl; 
    }
    return true;
}

std::ostream & operator<<(std::ostream &os, const libhexitec::HexitecBinaryReader& h) {
    return os << std::setw(4) << h.getConfig();
}

std::ostream & operator<<(std::ostream &os, const libhexitec::HexitecSequentialReader& h) {
    return os << std::setw(4) << h.getConfig();
}

}


template class libhexitec::HexitecArray<double>;
template class libhexitec::HexitecArray<float>;
template class libhexitec::HexitecArray<int>;
template class libhexitec::HexitecArray<short>;
template class libhexitec::HexitecArray<unsigned short>;
template class libhexitec::HexitecArray<unsigned int>;

template class libhexitec::CalibrationMap<double>;
template class libhexitec::CalibrationMap<float>;
template class libhexitec::CalibrationMap<int>;
template class libhexitec::CalibrationMap<short>;
template class libhexitec::CalibrationMap<unsigned short>;
template class libhexitec::CalibrationMap<unsigned int>;


template class libhexitec::ClipBoard<libhexitec::HexitecRawFrame>;
