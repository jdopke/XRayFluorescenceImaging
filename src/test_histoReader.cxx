// Test_readBinary
// Should read a single frame from a binary hexitec file and display it
// Could also read calibration maps and then spit out a calibrated file

#include "hexitec.hpp"

#include <nlohmann/json.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <chrono> 
using namespace std::chrono;


using namespace libhexitec;

int main(int argc, char* argv[]) {
	unsigned int x=0, y=0;
	std::string fileName="", outFileName="temp.svg", option;
	bool printHisto=false, haveFile=false, quiet=false, runAllBins=false, fullDump=false, scanData=false, reduceHisto=false, integrateHisto=false;
	unsigned int reductionFactor=1;
	unsigned int startIntegral = 0;
	unsigned int stopIntegral = 65536;
	if (argc > 1) {
		for (int i=1; i<argc; i++) {
			option = argv[i];
			if (option[0] != '-') {
				fileName = option;
				haveFile = true;
			}
			switch (option[1]) {
				case 'x': {
					if (option.length() == 2) {
						x = atoi(argv[++i]);
					} else {
						x = atoi(option.substr(2).c_str());
					}
					break;
				}
				case 'y': {
					if (option.length() == 2) {
						y = atoi(argv[++i]);
					} else {
						y = atoi(option.substr(2).c_str());
					}
					break;
				}
				case 'p': {
					printHisto = true;
					break;
				}
				case 's': {
					scanData = true;
					break;
				}
				case 'q': {
					quiet = true;
					break;
				}
				case 'a': {
					runAllBins = true;
					scanData = true;
					break;
				}
				case 'f': {
					fullDump = true;
					break;
				}
				case 'r': {
					reduceHisto = true;
					if (option.length() == 2) {
						reductionFactor = atoi(argv[++i]);
					} else {
						reductionFactor = atoi(option.substr(2).c_str());
					}
					break;
				}
				case 'i': {
					integrateHisto = true;
					break;
				}
				case 'b': {
					if (option.length() == 2) {
						startIntegral = atoi(argv[++i]);
					} else {
						startIntegral = atoi(option.substr(2).c_str());
					}
					break;
				}
				case 'e': {
					if (option.length() == 2) {
						stopIntegral = atoi(argv[++i]);
					} else {
						stopIntegral = atoi(option.substr(2).c_str());
					}
					break;
				}
				case 'o': {
					outFileName = argv[++i];
					break;
				}
				default: {
					break;
				}
			}
		}
	} else {
		std::cout << "Usage:      " << argv[0] << " <Histo filename (*.hxt)> -x <x> -y <y> -p -q -a -b <begin> -e <end> -o <outFileName>" << std::endl;
		std::cout << "  <Histo filename>: A histogram file as created by (e.g.) test_binReader." << std::endl;
		std::cout << "  <x> <y>         : Coordinates to dump out - if not given, defaults to 0, 0" << std::endl;
		std::cout << "  -p              : flag to print out the full histogram" << std::endl;
		std::cout << "  -q              : flag to shut up (quiet)" << std::endl;
		std::cout << "  -a              : flag to generate a map of all pixels with number of histogram bins being filled" << std::endl;
		std::cout << "  -b <begin>      : beginning of integral for 2D plot" << std::endl;
		std::cout << "  -e <end>        : end of integral for 2D plot" << std::endl;
		std::cout << "  -o <outFileName>: output filename for 2D plot" << std::endl;
		return -1;
	}

	if (haveFile) {
		HexitecRawHistogram histo(fileName);
		if (reduceHisto) {
			histo.dumpHistogram("/tmp/dumpHisto.csv", reductionFactor);
			histo.loadHistogram("/tmp/dumpHisto.csv");
		}
		if (!quiet) std::cout << "Showing you data for pixel: " << x << ", " << y << std::endl;
		if (printHisto || fullDump) histo.printPixel(x, y, fullDump);
		if (scanData) {
			if (runAllBins) {
				HexitecArray<unsigned int> nBinsMap;
				for (x=0; x<80; x++) {
					for (y=0; y<80; y++) {
						if (integrateHisto) {
							nBinsMap(x,y) = histo.integrate(x,y,startIntegral,stopIntegral);
						} else {
							nBinsMap(x,y) = histo.countNZBins(x, y);
						}
						if (!quiet) std::cout << x << "\t" << y << "\t" << nBinsMap(x,y) << std::endl;
					}
				}
				nBinsMap.plot(outFileName);
				nBinsMap.toFile("temp.csv", true);
			} else {
				if (quiet){
					std::cout << x << "\t" << y << "\t" << histo.countNZBins(x, y) << std::endl;
				}else {
					std::cout << "Histogram has " << histo.countNZBins(x, y) << " non-empty bins out of a total of " << histo.getNBins() << std::endl;
					std::cout << "Simple local maxima search gives local maxima at:" << std::endl;
					for (auto val : histo.findPeaks(x,y)) {
						std::cout << val << "\t" << histo.getVal(x,y,val) << std::endl;
					}
				}
			}
		}
	} else {
		std::cout << "You screwed up, no filename given" << std::endl;
	}

	return 0;
}