// Test_readBinary
// Should read a single frame from a binary hexitec file and display it
// Could also read calibration maps and then spit out a calibrated file

#include "hexitec.hpp"

#include <nlohmann/json.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <chrono> 
using namespace std::chrono;


using namespace libhexitec;


int main(int argc, char* argv[]) {
    std::string fileName="", outFileName="map2d.svg", option;
    bool haveFile=false, reduceHisto=false, integrateHisto=false;
    unsigned int reductionFactor=1;
    int startIntegral = 0;
    int stopIntegral = 8205;
    if (argc > 1) {
        for (int i=1; i<argc; i++) {
            option = argv[i];
            if (option[0] != '-') {
                fileName = option;
                haveFile = true;
            }
            switch (option[1]) {
                case 'r': {
                    reduceHisto = true;
                    if (option.length() == 2) {
                        reductionFactor = atoi(argv[++i]);
                    } else {
                        reductionFactor = atoi(option.substr(2).c_str());
                    }
                    break;
                }
                case 'i': {
                    integrateHisto = true;
                    break;
                }
                case 'b': {
                    if (option.length() == 2) {
                        startIntegral = atoi(argv[++i]);
                    } else {
                        startIntegral = atoi(option.substr(2).c_str());
                    }
                    break;
                }
                case 'e': {
                    if (option.length() == 2) {
                        stopIntegral = atoi(argv[++i]);
                    } else {
                        stopIntegral = atoi(option.substr(2).c_str());
                    }
                    break;
                }
                case 'o': {
                    outFileName = argv[++i];
                    break;
                }
                default: {
                    break;
                }
            }
        }
    } else {
        std::cout << "Usage:      " << argv[0] << " <Histo filename (*.hxt)> -b <begin> -e <end> -o <outFileName>" << std::endl;
        std::cout << "  <Histo filename>: A histogram file as created by (e.g.) test_binReader." << std::endl;
        std::cout << "  -r <reduction>  : set reduction factor" << std::endl;
        std::cout << "  -b <begin>      : beginning of integral for 2D plot" << std::endl;
        std::cout << "  -e <end>        : end of integral for 2D plot" << std::endl;
        std::cout << "  -o <outFileName>: output filename for 2D plot" << std::endl;
        return -1;
    }

    if (haveFile) {
        HexitecRawHistogram histo(fileName);
        if (reduceHisto) {
            histo.dumpHistogram("/tmp/dumpHisto.csv", reductionFactor);
            histo.loadHistogram("/tmp/dumpHisto.csv");
        }
        HexitecArray<unsigned int> nBinsMap;
        if (nBinsMap.isConstant() && nBinsMap.isLoaded()) {
            nBinsMap.size();
        }
        unsigned int x, y;
        Histo1D* tempHisto1d;
        for (x=0; x<80; x++) {
            for (y=0; y<80; y++) {
                if (integrateHisto) {
                    tempHisto1d = histo.extractPixelHisto(x,y);
                    nBinsMap(x,y) = tempHisto1d->integrate(startIntegral,stopIntegral);
                } else {
                    std::cout << "beginning and end of the integral required!\n";
                }
            }
        }
        std::cout << "Average Map value: " << nBinsMap.getAverage() << std::endl;
        std::cout << "Average Map value: " << nBinsMap.getRms() << std::endl;
        nBinsMap.plot(outFileName, true);
        nBinsMap.toFile("map2d.csv", true);
    } else {
        std::cout << "You screwed up, no filename given" << std::endl;
    }

    return 0;
}