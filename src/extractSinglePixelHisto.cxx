// Test_readBinary
// Should read a single frame from a binary hexitec file and display it
// Could also read calibration maps and then spit out a calibrated file

#include "hexitec.hpp"

#include <nlohmann/json.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <chrono>
using namespace std::chrono;


using namespace libhexitec;

int main(int argc, char* argv[]) {
	unsigned int x=0, y=0;
	std::string fileName="", option, input;
	bool haveFile=false, reduceHisto=false, logScale = false;
    unsigned int reductionFactor=1, thresholdValue=0;
    //std::cout << "Enter command: ";
    //std::cin.getline (argv,256);
    //argc = argv.length()
    //std::getline(std::cin,input);
    //argc = input.length();
    //argv = input.c_str();

	if (argc > 1) {
		for (int i=1; i<argc; i++) {
			option = argv[i];
			if (option[0] != '-') {
				fileName = option;
				haveFile = true;
			}
			switch (option[1]) {
				case 'x': {
					if (option.length() == 2) {
						x = atoi(argv[++i]);
					} else {
						x = atoi(option.substr(2).c_str());
					}
					break;
				}
                case 'l': {
                    logScale = true;
                    break;
                }
				case 'y': {
					if (option.length() == 2) {
						y = atoi(argv[++i]);
					} else {
						y = atoi(option.substr(2).c_str());
					}
					break;
				}
                case 'r': {
                    reduceHisto = true;
                    if (option.length() == 2) {
                        reductionFactor = atoi(argv[++i]);
                    } else {
                        reductionFactor = atoi(option.substr(2).c_str());
                    }
                    break;
                }
                case 't': {
                    if (option.length() == 2) {
                        thresholdValue = atoi(argv[++i]);
                    } else {
                        thresholdValue = atoi(option.substr(2).c_str());
                    }
                    break;
                }
				default: {
					break;
				}
			}
		}
	} else {
		std::cout << "Usage:      " << argv[0] << " <Histo filename (*.hxt)> -x <x> -y <y> " << std::endl;
		std::cout << "  <Histo filename>: A histogram file as created by (e.g.) test_binReader." << std::endl;
		std::cout << "  <x> <y>         : Coordinates to dump out - if not given, defaults to 0, 0" << std::endl;
		return -1;
	}

	if (haveFile) {
		HexitecRawHistogram histo(fileName);
        if (reduceHisto) {
            histo.dumpHistogram("/tmp/dumpHisto.csv", reductionFactor);
            histo.loadHistogram("/tmp/dumpHisto.csv");
        }
        //extract the single-pixel histogram
        Histo1D* tempHisto1d = histo.extractPixelHisto(x,y,thresholdValue);
        tempHisto1d->setTitle("Histogram_x_"+std::to_string(x)+"_y_"+std::to_string(y));
        tempHisto1d->HistoTableFull();
        tempHisto1d->plot("Histogram_x_"+std::to_string(x)+"_y_"+std::to_string(y)+".svg", logScale);
	} else {
		std::cout << "You screwed up, no filename given" << std::endl;
	}

	return 0;
}