# X-Ray Fluorescence related software repo

This repository contains random collected code around reading Hexitec Data (for the purpose of fluorescence imaging), either in binary format or hxt format, as well as converting from the former to the latter. Most of the code was created on a project trying to make old scrolls readable, e.g. this:

![PHerc Photo](doc/img/Fragment_small.gif)

Documentation of the code and the project as a whole will be automatically generated and uploaded to [this website](https://cern.ch/xrf-imaging/).

Related Code (probably more up-to-date) in Python will be tracked [here](https://gitlab.cern.ch/jdopke/xrfimagingpython), as I haven't figured out Python submodules quite yet.

The C++ code herein relies on [JSON for Modern C++](https://github.com/nlohmann/json), written by Niels Lohmann, for reading/writing configuration and potentially even data files... (though I might look for something more compressed)

