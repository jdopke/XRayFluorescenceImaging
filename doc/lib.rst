Library Description
===================


Hexitec ouput
-------------

The library functionality mostly relates to representation of hexitec recorded data, as well as processing thereof, be it into histograms, images, extracted subsets of the like.

The basic unit of everything that is being handled is a templated HexitecArray, with abstractions into CalibrationMaps, Hexitec(Raw)Frame and so on. Two additional more complex classes are provided as well:

* a Binary reader class, HexitecBinaryReader, which allows one to read a binary file and extract individual frames or to process it into a histogram
* a Histogrammer class, HexitecRawHistogram, allowing one to feed in HexitecRawFrames and generate a histogram of recorded energies for each pixel

Processing times vary, but largely depend on the amount of hard-disk available.

Reader Class
------------

The reader class can be easily controller by providing a JSON configuration file, that defines calibration files, as well as a buffer length for a number of recently read frames to be held in memory.

.. code-block:: json
   
   {
     "BufferLength": 10,
     "CalibrationSet": {
        "Gradients": "../config/500V_28C_Grads.txt",
        "Intersects": "../config/500V_28C_Inters.txt",
        "Thresholds": "../config/500V_28C_Thresh.txt"
     },
     "SourceFile": "../test/Dummy.bin",
     "TargetHisto": "../test/Dummy.hxt"
   }

The class becomes quite inefficient if the buffer length is set too large, as removal of the first vector element storing the frames and addition of a new tail have a massive overhead on memory allocation.

.. code-block:: cpp
   :linenos:

   class HexitecBinaryReader
   {
   public:
     HexitecBinaryReader(json config=json{});
     ~HexitecBinaryReader();
     bool initialise();
     HexitecFrame getFrame(unsigned int index);
     HexitecFrame getNextFrame();
     HexitecRawFrame getRawFrame(unsigned int index);
     HexitecRawFrame getNextRawFrame();
     bool readFrame(unsigned int nFrames=1);
     void setIntersects(std::string filename);
     void setGradients(std::string filename);
     void setThresholds(std::string filename);
     json const& getConfig() const ;
     void plotCalibration(std::string preText = "") ;
     int getFilePos();
   private:
     friend std::ostream & operator<<(std::ostream &os, const HexitecBinaryReader& h);
     CalibrationSet m_calibs;
     std::vector<HexitecRawFrame> frames;
     bool m_isInitialised;
     json m_config;
     std::ifstream m_file;
   };

The binary reader can either be created with a json config object being handed over, or get a (default) empty json object. In the latter case. a standard configuration is created, which can then be modified by retrieving the config object (getConfig) and then modifying entries.

The standard way though should be to load a json config file as is shown following:

.. code-block:: cpp

   std::ifstream jsonFile(filename); // filename needs to be a good json file
   HexitecBinaryReader reader(json::parse(jsonFile));


A simple program doing lots of things with this reader class is test_binReader and should be read by the so-inclined.


Histogrammer Class
------------------

The histogrammer class can be fed HexitecRawFrames, histogramming all possible values up to a maximum bin-Value, specified on instantiation.

.. code-block:: cpp
   :linenos:

   class HexitecRawHistogram{
   public:
     HexitecRawHistogram(unsigned int nBins=8192);
     HexitecRawHistogram(std::string filename);
     ~HexitecRawHistogram();
     unsigned int binIndex(unsigned int x, unsigned int y, unsigned int binNumber=0);
     void histogramRawFrame(HexitecRawFrame frame);
     void reInitialise(unsigned int nBins);
     unsigned int * reduce(unsigned int reductionFactor=2);
     bool dumpHistogram(std::string filename, unsigned int reductionFactor = 1);
     bool loadHistogram(std::string filename);
     void printPixel(unsigned int x, unsigned int y, bool printEmpty=true);
     unsigned int integral(unsigned int x, unsigned int y, unsigned int maxBin);
     unsigned int countNZBins(unsigned int x, unsigned int y);
     unsigned int getNBins();
     unsigned int getVal(unsigned int x, unsigned int y, unsigned int bin);
     std::vector<unsigned int> findPeaks(unsigned int x, unsigned int y);
   private:
     unsigned int *data;
     unsigned int m_nBins;
     HexitecArray<double> peakFinder;
   };

One major feat is that it does initially histogram full bin entries, i.e. up to 65536 bins along the energy axes (unsigned short value). This keeps the full resolution of all hits until such a time, when fitting is required and therefore coarser binning gives a better stability. Reduction of the histogram size can be done through writing out with a reductionfactor. Re-reading in the same file, one can easily replace the histogram with a coarser resoliution version. Fitting to be implemented. Reduction on the spot to be implemented. The histogrammer is implemented as inlined header code, rather than compiled library code.



