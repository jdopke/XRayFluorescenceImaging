.. DECAL DAQ Software documentation master file, created by
   sphinx-quickstart on Sat Jan 12 22:47:51 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :hidden:

   Introduction <intro>
   Library description <lib>
   Test tools <test>
   Snappy <snappy>


XRF Imaging Software for use with Hexitec
=========================================

This `Software <https://gitlab.cern.ch/jdopke/XRayFluorescenceImaging/>`__ is written to allow conversion and inspection of scan files recorded with the hexitec detector.

The repository contains all code (that I am aware of) written so far in the ancient directory, with subdirectories for Python and C++ based code. These are just for reference and should not be considered anywhere near production quality - in fact even for reference reading, that one is pretty bad. Please have a a look at CONTRIBUTING.md within the repo to see what needs working out.

I am trying to keep it up to date by pushing regularly, but please ask jdopke_at_cern.ch, in case you feel like there is something amiss.

