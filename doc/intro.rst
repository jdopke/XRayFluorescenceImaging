XRF Imaging Software for use with Hexitec
=========================================

This software project came up with relation to measurements based on the Hexitec Detector, performed at Diamond in Summers of 2016 and 2017. Data recorded with Hexitec is, in its final form, provided as a 3D array over 2 lateral coordinates and and energy range. Some initial analysis based on python scripts (see `Python subdirectory <https://gitlab.cern.ch/jdopke/XRayFluorescenceImaging/tree/master/ancient/Python>`__) showed that a deeper understanding of the production of these 3D datasets may be required to perform better in certain analyses. This software is to allow reconditioning of the raw data into 3D arrays whilst applying certain constraints. An initial attempt was made in Python, which works, but is hardly readable/user-friendly.

Hexitec ouput
-------------

Hexitec records data on a frame basis, i.e. every pixel in the detector (single front-end delivers 80x80 pixels) records a value in each frame. Detector calibration allows to detect whether a pixel is above its threshold and can then be used to turn a 16 bit integer value into a floating point value that is an energy in keV. Storage of the raw data is into bin-files, which just contain 16 bit values for every pixel, frame by frame. The pixel order is slightly odd, with the sensor being split into 4 regions and each delivering a pixel at a time:

Order within the file:

(1,1), (1,21), (1,41), (1, 61), (1, 2), (1, 22), ... (79, 80), (80, 20), (80, 40), (80, 60), (80, 80)

A comment on data size
~~~~~~~~~~~~~~~~~~~~~~

Depending on the frame rate chosen (up to about 10k frames/s), file size can get very large and starts being the major bottleneck in processing (read speed from disk). This software has initial hooks to be able to compile against `Snappy <https://github.com/google/snappy>`__, a fast compression algorithm freely available. Hope is that we can come up with a block format for raw files, so as to process them from bin into compressed bin, where re-processing is still possible but faster due to shorter read times.

Compiling the software
----------------------

To compile the software from this repository, we are currently relying on `CMake <https://cmake.org/>`__:

After checkout, create a subdirectory build and run CMake in there:

.. code-block:: bash

  mkdir build
  cd build/
  cmake ../
  make

After the build is complete, you should be able to run `./test_reader ../config/500V_28C_Thresh.txt Thresholds`, which should produce a file called "Threshold.svg" in the current directory - viewable with any webbrowser.
