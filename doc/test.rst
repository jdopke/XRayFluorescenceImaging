Test Tools
==========

.. role:: bash(code)
   :language: bash

A set of test tools are written, mostly to support quick testing of library functions, but also to run Code Coverage tests. However, these tools currently provide all the basic functionality based on libHexitec.

test_binReader
--------------

This program uses the HexitecBinaryReader Class to read in a binary Hexitec file. It'll process the file into a (properly ordered) histogram and output that to a ".hxt" file.

It requires an input JSON file as given in e.g. DummyBinReader.json:

.. code-block:: json
   
   {
     "BufferLength": 10,
     "CalibrationSet": {
        "Gradients": "../config/500V_28C_Grads.txt",
        "Intersects": "../config/500V_28C_Inters.txt",
        "Thresholds": "../config/500V_28C_Thresh.txt"
     },
     "SourceFile": "../test/Dummy.bin",
     "TargetHisto": "../test/Dummy.hxt"
   }

**Bufferlength** refers to an internal buffer utilised inside HexitecBinaryReader.

**CalibrationSet** describes a set of filenames, relative to the run directory of test_binReader.

**SourceFile** and **TargetHisto** are input and output files for the program, where SourceFile refers to the a Hexitec Binary file from a run. The output HXT file is generated at full resolution of the bin file fed in.

test_configReader
-----------------

A simple test program, producing an svg plot of the calibration file handed over as the first parameter. A second parameter allows defining the plot file name.

.. figure:: img/test/Intersects.svg
   :scale: 50 %
   :alt: Intersects Map

   An example of the output intersects map from the config folder.


test_histoReader
----------------

Main test program for reading and processing histograms - can produce 1D histograms for single pixels or a map of total counts at the moment. Histograms can be down-sampled by a reduction factor.

.. figure:: img/test/Alpha.svg
   :scale: 50 %
   :alt: Alpha Sample Count

   An example of test_histoReader from an Alpha intensity square sample.


test_histoReader has various parameters influencing the output:

.. code-block:: bash

   test_histoReader <Histo filename (*.hxt)> -x <x> -y <y> -p -q -a -b <begin> -e <end> -o <outFileName>

.. table:: Parameters for test_histoReader
   :align: left
   :widths: auto

   ================  ===============================================================================
   Parameter         Description
   ================  ===============================================================================
   <Histo filename>  A histogram file as created by (e.g.) test_binReader.
   <x> <y>           Coordinates to dump out - if not given, defaults to 0, 0
   -p                flag to print out the full histogram
   -q                flag to shut up (quiet)
   -a                flag to generate a map of all pixels with number of histogram bins being filled
   -b <begin>        beginning of integral for 2D plot
   -e <end>          end of integral for 2D plot
   -o <outFileName>  output filename for 2D plot
   ================  ===============================================================================



test_compression
----------------

A program applying compression to all frames within a binary file, reporting on total compression ratios and timing thereof. This code does not get tested or reported in coverage, as it is currently not baseline for later processing.
