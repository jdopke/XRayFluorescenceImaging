Snappy
======

.. role:: bash(code)
   :language: bash

`Snappy <https://github.com/google/snappy>`__ is a free, open-source, fast compression library, allowing to compress arrays of `char`.

A test application is set up: :bash:`test_compression`. Given a parameter <filename> which should point to a hexitec calibration file, it will load the calibration map and try to compress and decompress it in memory, just to show how things work.


