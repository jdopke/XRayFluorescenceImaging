# Contributing

In general the process would be forking of the original repository, creating a branch and then submitting. For those based on a different gitlab server for submit rights, feel free to clone, then push with new branches to your own server and let me fork from there. I can then transfer your changes and merge on this gitlab installation.

## A rough idea about what needs doing here:

Mostly listing things that are necessary to process files from 2017

* Reading of the actual binary files is done but:
	* HexitecBinaryReader is inefficient, probably overheads in juggling frames around - more shared_ptr or handover by reference?
* Frame handling:
	* Hits
	* Clustering
	* Occupancy measures
	* Frame writing
		* Original storage with slightly disordered binary data
		* Compressed storage with ordered frame, most likely massive gains through compression
			* Prefer to have a standard container construct that allows typed storage of different containers, could allow to store 
	* Histogram writing (and reading) needs a header for the histogram file, describing what is in the binary part
* Need to introduce exceptions
	* e.g. exception for wrong filenames, non-existing files, trying to apply objects that are not ready (`isInitialised()`) and so on
* Threading (Though currently not useful):
    * Single frames for clusters
    * Compression of frames in parallel?

## Other thoughts

Setup should be simulated in [Geant 4](https://geant4.web.cern.ch/). Given a proper simulation, we may be able to establish, whether a lab-based setup is a viable route?
